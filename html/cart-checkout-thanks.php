<? include('header.php'); ?>

	<div class="container">

		<div class="row">
			<div class="col-md-5 center-block thanks-block pt50 pb50">
				<i class="fa fa-check"></i>
				<h1>
					Все прошло успешно!
				</h1>
				<p>номер заказа: 654sdf4</p>
				<h5>
					Спасибо за покупку, мы надеемся, что вам поравилось.
				</h5>
			</div>
		</div>

		<div class="row">
			<div class="col-md-5 center-block error-block pt50 pb50">
				<i class="fa fa-close"></i>
				<h1>
					Ошибка!
				</h1>
				<p>номер заказа: 654sdf4</p>
				<h5>
					Чтото пошло не так, попробуйте позже или обратитесь за <a href="#">поддержкой</a>.
				</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 center-block wait-block pt50 pb50">
				<i class="fa fa-clock-o"></i>
				<h1>
					Ждем!
				</h1>
				<p>номер заказа: 654sdf4</p>
				<h5>
					Ожидаем поступления средств на наш счет.
				</h5>
			</div>
		</div>
	</div>



	

<? include('footer.php'); ?>