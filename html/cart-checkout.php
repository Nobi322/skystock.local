<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb12">
				<form class="col-md-5 search-col">
					<input type="text" class="form-control">
					<button type="submit" class="btn btn-primary"><i class="i-magni"></i> Поиск</button>
				</form>
				<div class="col-md-7 return-link">
					<a href="#"><i class="fa fa-angle-left"></i>Вернуться назад</a>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h2 class="my-title">
					Оформление заказа
				</h2>
			</div>
			<div class="col-md-12 line-divider"></div>


			<div class="col-md-12">
				<table class="cart-table-finish">
					<tr class="ct-total-row">
						<td>Сводка заказа: </td>
						<td class="ct-qty-total"><span class="qty-total">2</span> клипа</td>
						<td></td>
						<td class="ct-total">
							<span class="total">158</span> грн <b>
								<br>
								( <span class="each">79</span> грн за каждый )</b>
						</td>

					</tr>
				</table>
			</div>
		</div>
		<form class="row pt40">
			<div class="col-md-12">
				<h3 class="my-title text-center">
					Способ оплаты
				</h3>
			</div>
			<div class="col-md-8 col-md-offset-2 pay-selector">
				<label class="pay-item">
					<input type="radio" name="p-method" class="dnt-style"/>
					<span>
						<img src="img/privat24.png">
					</span>
				</label>
				<label class="pay-item">
					<input type="radio" name="p-method" class="dnt-style"/>
					<span>
						Банковский перевод
					</span>
				</label>
			</div>
		</form>
		<div class="row">
			<form class="col-md-8 col-md-offset-2 pt30 pb80">
				<div class="row">
					<div class="col-md-3 form-text">
						Имя
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Фамилия
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Почтовый адрес 1
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Почтовый адрес 2
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Город
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Штат/Область
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Почтовый индекс
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Номер телефона
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Номер факса
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Страна
					</div>
					<div class="col-md-6">
						<select class="sel-24">
							<option>США</option>
							<option>Украина</option>
							<option>Россия</option>
							<option>Гондурас</option>
							<option>Крыжополь</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5 col-md-offset-4">
						<button type="submit" class="btn btn-primary  mt20">Оформить заказ</button>
					</div>
				</div>
			</form>
		</div>
	</div>



	

<? include('footer.php'); ?>