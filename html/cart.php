<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb12">
				<form class="col-md-5 search-col">
					<input type="text" class="form-control">
					<button type="submit" class="btn btn-primary"><i class="i-magni"></i> Поиск</button>
				</form>
				<div class="col-md-7 return-link">
					<a href="#"><i class="fa fa-angle-left"></i>Вернуться назад</a>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h2 class="my-title">
					Моя корзина <span class="counter">(0)</span>
				</h2>
			</div>
			<div class="col-md-12 line-divider"></div>
			<div class="col-md-12 mb100">
				<table class="cart-table">
					<tbody>
						<tr>
							<td class="ct-img">
								<a href="#"><img src="img/img-tmp-1.jpg" alt=""/></a>
							</td>
							<td>
								HD <span class="ct-type">Клип:</span> 2901469
							</td>
							<td>
								79 грн
							</td>
							<td>
								<a href="#" class="ct-delete">Удалить</a>
							</td>
						</tr>
						<tr>
							<td class="ct-img">
								<a href="#"><img src="img/img-tmp-2.jpg" alt=""/></a>
							</td>
							<td>
								HD <span class="ct-type">Клип:</span> 2901469
							</td>
							<td>
								79 грн
							</td>
							<td>
								<a href="#" class="ct-delete">Удалить</a>
							</td>
						</tr>
						<tr>
							<td class="ct-img">
								<a href="#"><img src="img/img-tmp-3.jpg" alt=""/></a>
							</td>
							<td>
								HD <span class="ct-type">Клип:</span> 2901469
							</td>
							<td>
								162 грн
							</td>
							<td>
								<a href="#" class="ct-delete">Удалить</a>
							</td>
						</tr>
						<tr class="ct-total-row">
							<td>Сводка заказа: </td>
							<td class="ct-qty-total"><span class="qty-total">2</span> клипа</td>
							<td class="ct-total"><span class="total">158</span> грн <b>( <span class="each">79</span> грн за каждый )</b></td>
							<td><a href="#" class="ct-delete-total">Удалить</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>



	

<? include('footer.php'); ?>