<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17">
				<form class="col-md-5 search-col">
					<input type="text" class="form-control">
					<button type="submit" class="btn btn-primary"><i class="i-magni"></i> Поиск</button>
				</form>
				<div class="col-md-7 category-selected">
					Кадры <span class="sel">белый слон</span> (8,253)
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 line-divider"></div>
			</div>
			<div class="row">
				<div class="col-md-5 pb23">
					<select class="big-select" data-placeholder="Фильтровать по категориям">
						<option></option>
						<option>Слоники (8,545)</option>
						<option>Мышки (18,545)</option>
						<option>Кошки (1,345)</option>
						<option>Коптеры (145)</option>
						<option>Леса (2,945)</option>
						<option>Поля</option>
						<option>Океаны</option>
						<option>Чертики</option>
						<option>Фестивали</option>
						<option>Персонажи</option>
						<option>придумай</option>
						<option>чтото</option>
						<option>еще</option>
						<option>мне лень</option>
					</select>
				</div>
				<div class="col-md-7 resolution-filter">
					Разрешение:
					<form action="">
						<ul>
							<li><label><input type="checkbox" name="resolution" class="dnt-style"/><span>4K</span></label> (854)</li>
							<li><label><input type="checkbox" name="resolution" class="dnt-style"/><span>HD</span></label> (9,54)</li>
							<li><label><input type="checkbox" name="resolution" class="dnt-style"/><span>SD</span></label> (154)</li>
							<li><label><input type="checkbox" name="resolution" class="dnt-style"/><span>Web</span></label> (854)</li>
						</ul>
					</form>
				</div>
			</div>
		</div>
	</div>
	<section class="container-fluid catalog">
		<div class="container">
			<div class="row pt20">
				<div class="col-md-7 additional-tags">
					Похожее:
					<a href="#">слоны,</a>
					<a href="#">белый тигр,</a>
					<a href="#">слоны в африке,</a>
					<a href="#">группа слонов</a>
				</div>
				<div class="col-md-5 ctl-controls">
					<div class="number-settings">
						<a href="#" class="nbs-controll"><i class="i-friction"></i></a>
						<div class="nbs-popup">
							<a href="#" class="i-arr-down"></a>
							Показать:
							<ul class="nbs-list">
								<li class="active">
									<a href="#">20</a>
								</li>
								<li>
									<a href="#">60</a>
								</li>
								<li>
									<a href="#">100</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="pagi">
						<input type="text" class="form-control" value="2">
						из 124
						<a href="#" class="pagi-prev"></a>
						<a href="#" class="pagi-next"></a>
					</div>
				</div>
			</div>
			<div class="row items-grid">
				<?
					$i=0;

					while($i++<16):
						$cur = rand(1,3);
				?>
				<div class="col-md-3 item-pill">
					<div class="item-cnt" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
						<a href="#" class="img-cnt">
							<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
						</a>
						<div class="ip-title">
							<span class="quality">HD</span> 00:<?  echo rand(10, 59); ?>
						</div>
						<div class="ip-buttons">
							<a href="#" class="ip-favorite <? echo (rand(0,1)) ?  'active': ''; ?>" title="Добавить в избранное" data-item-no="54fdfsfb"></a>
							<span class="divider"></span>
							<a href="#" class="ip-cart"></a>
						</div>
					</div>
				</div>
				<?  endwhile; ?>
			</div>
			<div class="row mb60">
				<div class="col-md-5 ctl-controls pull-right">
					<div class="number-settings">
						<a href="#" class="nbs-controll"><i class="i-friction"></i></a>
						<div class="nbs-popup">
							<a href="#" class="i-arr-down"></a>
							Показать:
							<ul class="nbs-list">
								<li class="active">
									<a href="#">20</a>
								</li>
								<li>
									<a href="#">60</a>
								</li>
								<li>
									<a href="#">100</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="pagi">
						<input type="text" class="form-control" value="2">
						из 124
						<a href="#" class="pagi-prev"></a>
						<a href="#" class="pagi-next"></a>
					</div>
				</div>
			</div>
		</div>
	</section>



	

<? include('footer.php'); ?>