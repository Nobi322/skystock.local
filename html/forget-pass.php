<? include('header.php'); ?>


<div class="container pt50 pb100">


	<div class="row mb40">
		<div class="col-md-7 center-block text-center">
			<form  class="form-blue">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 white mb20">
						<h2>Забыли пароль?</h2>
						<p>
							Введите свой адрес электронной
							почты ниже, и мы отправим вам
							ссылку на сброс пароля.
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<input type="email" class="form-control" placeholder="Адрес электронной почты"/>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-md-6 col-md-offset-3">
						<button type="submit" class="btn btn-default">
							Вышлите мне ссылку  на смену пароля
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row mb40">
		<div class="col-md-7 center-block">
			<form class="form-blue">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 white mb20">
						<h2>Забыли пароль?</h2>
						<div class="form-error">
							Мы не смогли найти введенное
							вами имя пользователя
							в нашей системе.
						</div>
						<p>
							Введите свой адрес электронной
							почты ниже, и мы отправим вам
							ссылку на сброс пароля.
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<input type="email" class="form-control" placeholder="Адрес электронной почты"/>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-md-6 col-md-offset-3">
						<button type="submit" class="btn btn-default">
							Вышлите мне ссылку  на смену пароля
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row ">
		<div class="col-md-7 center-block ">
			<form class="form-blue">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 white mb20">
						<h2>Войти</h2>
						<div class="form-info">
							Проверьте свою почту
							и воспользуйтесь ссылкой для
							восстановления пароля.
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<input type="text" class="form-control" placeholder="Ваше имя или e-mail"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<input type="password" class="form-control" placeholder="Введите пароль"/>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-md-6 col-md-offset-3">
						<button type="submit" class="btn btn-default">
							Войти
						</button>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3 pt22">
						<a href="#">Забыли пароль?</a>
					</div>
						
				</div>
			</form>
		</div>
	</div>
</div>



	

<? include('footer.php'); ?>