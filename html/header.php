<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1000">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>SkyStock</title>

	<!-- Bootstrap -->
	<link href="css/jquery.formstyler.css" rel="stylesheet">
	<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="css/video-js.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/owl.theme.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<header class="container-fluid">
	<div class="row top-grey">
		<div class="container">
			<div class="row">
				<div class="col-md-8 pull-right top-menu">
					<ul class="lang-menu">
						<li>
							<a href="#">En</a>
						</li>
						<li class="active">
							<a href="#">Ru</a>
						</li>
					</ul>
					<div class="profile hidden login">
						<a href="#login-pp" class="open-log">
							Войти
						</a>
						<div id="login-pp" class="login-pp">
							<form>
								<div class="row">
									<div class="col-md-12">
										<input class="form-control" type="text" required placeholder="Имя пользователя или e-mail">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input class="form-control" type="password" required placeholder="Пароль">
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<button type="submit" class="btn btn-default">Войти</button>
									</div>
									<div class="col-md-6">
										<a href="#" class="grey-link">
											Забыли пароль?
										</a>
									</div>
								</div>
							</form>
							<div class="row">
								<div class="col-md-12 line-divider"></div>
							</div>
							<div class="row">
								<div class="col-md-12 text-cell">
									Еще не зарегистрированы?<br>
									<a href="#">
										Создать бесплатный аккаунт
										для просмотра
									</a>

								</div>
							</div>
						</div>
					</div>
					<div class="profile signed-in">
						<a href="#sigin-pp" class="open-sig">
							Здравствуйте, iren <i class="fa fa-angle-down"></i>
						</a>
						<div id="sigin-pp" class="sigin-pp">
							<ul class="sig-list">
								<li><a href="#" class="si-favorite"><i class="icon"></i> Избранное</a></li>
								<li><a href="#" class="si-profile"><i class="icon"></i> Сведения об аккаунте</a></li>
								<li><a href="#" class="si-downloads"><i class="icon"></i> История загрузок</a></li>
								<li><a href="#" class="si-logout"><i class="icon"></i> Выйти</a></li>
							</ul>
						</div>
					</div>
					<a href="#" class="cart-link">В корзине 2 товара</a>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row h-second">
			<a href="/" class="col-md-3 logo">
				<img src="img/logo.png" alt=""/>
			</a>
			<div class="col-md-3 pull-right info-h">
				<p class="phones">
					<span>+38 (050)</span> 336 05 00
				</p>
				<p class="phones">
					<span>+38 (048)</span> 704 74 33
				</p>
				<a href="mailto:sales@skycap.ua" class="email"><i class="i-email"></i> sales@skycap.ua </a>
			</div>
		</div>
	</div>
</header>
<div class="wrapper">



