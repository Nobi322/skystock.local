<? include('header.php'); ?>


<div class="container pt40">
	<div class="row">
		<div class="col-md-12">
			<ol>
				<li>
					<a href="main.php" target="_blank">Главная</a>
				</li>
				<li>
					<a href="catalog.php" target="_blank">Каталог</a>
				</li>
				<li>
					<a href="item-page.php" target="_blank">Странница товара</a>
				</li>
				<li>
					<a href="plan.php" target="_blank">Обзор плана</a>
				</li>
				<li>
					<a href="plan-plus.php" target="_blank">Обзор плана c банером</a>
				</li>
				<li>
					<a href="plan-info.php" target="_blank">Обзор плана c историей</a>
				</li>
				<li>
					<a href="library.php" target="_blank">Библиотека - пустая</a>
				</li>
				<li>
					<a href="library-full.php" target="_blank">Библиотека - полная</a>
				</li>
				<li>
					<a href="library-inside.php" target="_blank">Библиотека - внутри</a>
				</li>
				<li>
					<a href="forget-pass.php" target="_blank">Забытый пароль</a>
				</li>
				<li>
					<a href="registration-n-login.php" target="_blank">Регистрация и вход</a>
				</li>
				<li>
					<a href="my-account-plan.php" target="_blank">Профиль - План</a>
				</li>
				<li>
					<a href="my-account-profile.php" target="_blank">Профиль - Профиль</a>
				</li>
				<li>
					<a href="info-all-plan.php" target="_blank">Выбор плана</a>
				</li>
				<li>
					<a href="cart.php" target="_blank">Корзина</a>
				</li>
				<li>
					<a href="text-page.php" target="_blank">Страница стилей</a>
				</li>
				<li>
					<a href="404.php" target="_blank">404</a>
				</li>
				<li>
					<a href="my-account-change.php" target="_blank">Изменить - всё</a>
				</li>
				<li>
					<a href="my-account-options.php" target="_blank">Изменить - настройки</a>
				</li>
				<li>
					<a href="my-account-plans-full.php" target="_blank">Изменить - планы - полный</a>
				</li>
				<li>
					<a href="cart-checkout.php" target="_blank">Корзина - оформление</a>
				</li>
				<li>
					<a href="cart-checkout-thanks.php" target="_blank">Корзина - оформление - спасибо</a>
				</li>
				<li>
					<a href="my-account-payment-history.php" target="_blank">История платежей</a>
				</li>
				<li>
					<a href="my-account-payment-history-details.php" target="_blank">История платежей - Подробности</a>
				</li>
				<li>
					<a href="my-account-download-history.php" target="_blank">История загрузок</a>
				</li>
			</ol>
		</div>
	</div>
</div>

<? include('footer.php'); ?>
