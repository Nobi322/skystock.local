<? include('header.php'); ?>



<section class="plan-selector container-fluid" style="background-image: url('img/plan-2-bg.jpg')">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 ps-block">
				<h1 class="title">
					Выберите подходящий пакет
				</h1>
				<div class="row">
					<div class="col-md-3">
						<p class="label-h3">
							Пакеты видео
						</p>
					</div>
					<div class="col-md-9">
						<ul class="nav nav-tabs ps-tabs-list" role="tablist">
							<li role="presentation" class="active">
								<a href="#WEB" aria-controls="home" role="tab" data-toggle="tab">Web</a>
							</li>
							<li role="presentation">
								<a href="#SD" aria-controls="home" role="tab" data-toggle="tab">SD</a>
							</li>
							<li role="presentation">
								<a href="#HD" aria-controls="home" role="tab" data-toggle="tab">HD</a>
							</li>
							<li role="presentation">
								<a href="#K4" aria-controls="home" role="tab" data-toggle="tab">4K</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane ps-tab active" id="WEB">
								<div class="row">
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов Web: 5
										</h3>
										<p class="ps-price">
											79 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 5 видео</a>
									</div>
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов Web: 10
										</h3>
										<p class="ps-price">
											149 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 10 видео</a>
									</div>
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов Web: 25
										</h3>
										<p class="ps-price">
											349 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 25 видео</a>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane ps-tab " id="SD">
								<div class="row">
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов SD: 5
										</h3>
										<p class="ps-price">
											79 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 5 видео</a>
									</div>
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов SD: 10
										</h3>
										<p class="ps-price">
											149 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 10 видео</a>
									</div>
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов SD: 25
										</h3>
										<p class="ps-price">
											349 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 25 видео</a>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane ps-tab " id="HD">
								<div class="row">
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов HD: 5
										</h3>
										<p class="ps-price">
											79 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 5 видео</a>
									</div>
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов HD: 10
										</h3>
										<p class="ps-price">
											149 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 10 видео</a>
									</div>
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов HD: 25
										</h3>
										<p class="ps-price">
											349 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 25 видео</a>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane ps-tab " id="K4">
								<div class="row">
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов 4K: 5
										</h3>
										<p class="ps-price">
											79 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 5 видео</a>
									</div>
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов 4K: 10
										</h3>
										<p class="ps-price">
											149 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 10 видео</a>
									</div>
									<div class="col-md-4 ps-pill">
										<h3>
											Клипов 4K: 25
										</h3>
										<p class="ps-price">
											349 грн
										</p>
										<p class="ps-eco">
											Сэкономьте 16 грн
										</p>

										<a href="#" class="btn btn-plan">Получить 25 видео</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ps-mini-info">
						Видео должны быть загружены в течение 365 дней после покупки. Они регулируются условиями стандартной лицензии.
						Нужен план для небольшой группы или предприятия? См. решения для бизнеса.
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="faq container">
	<div class="row">
		<div class="col-md-12 pt60">
			<h2 class="bold">
				Вопросы и ответы
			</h2>
		</div>
		<div class="col-md-12 line-divider"></div>
	</div>
	<div class="row pt30 pb100">
		<div class="col-md-6 pr40">
			<ul class="faq-list">
				<li>
					<dt>Когда я могу использовать мой пакет видео?</dt>
					<dd>
						Начинайте скачивать видео сразу после покупки пакета. Скачивать можно в любое время в течение 365 дней.
					</dd>
				</li>

				<li>
					<dt>Какие по размеру видео я могу скачивать с моим пакетом?</dt>
					<dd>
						Доступны пакеты с разрешениями HD, SD и Web. Для скачивания из этого пакета будут доступны видео с таким же или более низким разрешением. Например, из пакета HD можно скачать видео с разрешениями HD, SD или Web.
					</dd>
				</li>
				<li>
					<dt>
						Могу ли я покупать видеоклипы без пакета?
					</dt>
					<dd>
						Да. Добавьте ваши клипы в корзину и оплатите их кредитной картой. Клип с разрешением 4K стоит 199 $, HD — 79 $, SD — 49 $, а с разрешением Web — 19 $.
					</dd>
				</li>
				<li>
					<dt>
						Может ли у меня быть несколько активных пакетов одновременно?
					</dt>
					<dd>
						Да. Ограничений на количество или тип пакетов, которыми вы можете пользоваться одновременно, не установлено. Например, у вас одновременно может быть 3 активных пакета HD.
					</dd>
				</li>
			</ul>
		</div>
		<div class="col-md-6">
			<ul class="faq-list">
				<li>
					<dt>
						Если у меня несколько пакетов, как можно выбрать, из какого из них скачивать видео?
					</dt>
					<dd>
						Вы сможете выбирать, какой пакет использовать в корзине. Вам будут доступны все подходящие пакеты футажей
					</dd>
				</li>
				<li>
					<dt>
						Когда истекает срок действия моего пакета?
					</dt>
					<dd>
						Ваш пакет перестает действовать после скачивания установленного количества изображений либо по истечении 365 дней. После этого вы не сможете скачивать клипы при помощи этого пакета, но в течение года у вас сохраняется доступ к клипам из истории скачиваний.
					</dd>
				</li>
				<li>
					<dt>
						Можно ли включить автоматическое продление моих пакетов?
					</dt>
					<dd>
						Да. В пакетах есть параметр "автоматическое продление". Мы настоятельно рекомендуем включать автоматическое продление для непрерывного доступа к видеоклипам. Вы можете включить автоматическое продление на странице ”Сводка заказа" при оформлении заказа или на вашей странице "Сведения об аккаунте" при покупке пакета. Если автоматическое продление включено, ваш пакет будет автоматически пополняться, как только вы скачаете все видео из пакета или по истечению 365 дней, смотря что произойдет раньше.
					</dd>
				</li>
			</ul>
		</div>
	</div>
</section>

	

<? include('footer.php'); ?>