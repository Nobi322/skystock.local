<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb12">
				<form class="col-md-5 search-col">
					<input type="text" class="form-control">
					<button type="submit" class="btn btn-primary"><i class="i-magni"></i> Поиск</button>
				</form>
				<div class="col-md-7 return-link">
					<a href="#"><i class="fa fa-angle-left"></i>Вернуться назад</a>
				</div>
			</div>

		</div>
	</div>
	<section class="container-fluid item-info">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<video id="test" class="video-js vjs-default-skin"
						   controls preload="auto" width="576" height="380"
						   poster="http://video-js.zencoder.com/oceans-clip.png"
						   data-setup='{"example_option":true}'>
						<source src="http://video-js.zencoder.com/oceans-clip.mp4" type='video/mp4' />
						<source src="http://video-js.zencoder.com/oceans-clip.webm" type='video/webm' />
						<source src="http://video-js.zencoder.com/oceans-clip.ogv" type='video/ogg' />
						<p class="vjs-no-js">Для просмотра этого видео пожалуйста включите JavaScript, или узнайте какой браузер поддерживает  <a href="http://videojs.com/html5-video-support/" target="_blank">воспроизведение HTML5 видео</a></p>
					</video>
					<div class="row item-buttons">
						<div class="col-md-6">
							<a href="#" class="ip-favorite" title="Сохранить в избранное" data-item-no="54fdfsfb"><i class="fa fa-star"></i> Сохранить в избранное</a>
						</div>
						<div class="col-md-6">
							<a href="#" class="pp-unlogined"><i class="fa fa-download"></i> Скачать образец</a>
						</div>
					</div>
				</div>
				<div class="col-md-5 video-description">
					<p>Стоковое видео:</p>
					<h5>
						Слоны заблудились в зеленой траве - 4K
					</h5>
					<div class="row">
						<div class="col-md-12 line-divider"></div>
						<div class="col-md-12 ">
							<ul class="vd-info-list">
								<li><span class="title">Длина клипа:</span> 00:24 </li>
								<li><span class="title">Соотношение ширины и высоты:</span> 16:9 </li>
								<li><span class="title">Номер клипа:</span> 4523001</li>
								<li><span class="title">Авторское право:</span> <a href="">Pathos Media</a> </li>
							</ul>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12 vd-text">
							С разрешением моделей.<br>
							Подписанное разрешение, хранящееся в архиве SkyCap-stock, Inc.
						</div>
					</div>
					<form action="">
					<div class="row">
						<div class="col-md-12">

								<table class="quality-table">
									<tr>
										<td><input type="radio" name="quality" value="hd" required></td>
										<td>HD</td>
										<td>79 <span>грн</span></td>
										<td>1920 X 1080  @ 25 fps MJPEG</td>
										<td>218.6 MB</td>
									</tr>
									<tr>
										<td><input type="radio" name="quality" value="sd" required></td>
										<td>SD</td>
										<td>119 <span>грн</span></td>
										<td>852 X 480 @ 25 fps MOV</td>
										<td>6.8 MB</td>
									</tr>
									<tr>
										<td><input type="radio" name="quality" value="web" required></td>
										<td>Web</td>
										<td>119 <span>грн</span></td>
										<td>852 X 480 @ 25 fps MOV</td>
										<td>6.8 MB</td>
									</tr>
									<tr>
										<td><input type="radio" name="quality" value="4k" required></td>
										<td>4K</td>
										<td>119 <span>грн</span></td>
										<td>852 X 480 @ 25 fps MOV</td>
										<td>6.8 MB</td>
									</tr>
								</table>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12 mt20">
							<button type="submit" class="btn btn-default lg">Скачать</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid same-videos">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mt30">
					<h3>Похожие клипы</h3>
				</div>
			</div>
			<div class="row items-grid small-grid">
				<?
					$i=0;

					while($i++<10):
						$cur = rand(1,3);
				?>
				<div class="col-md-3 item-pill">
					<div class="item-cnt" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
						<a href="#" class="img-cnt">
							<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
						</a>
						<div class="ip-title">
							<span class="quality">HD</span> 00:<?  echo rand(10, 59); ?>
						</div>
						<div class="ip-buttons">
							<a href="#" class="ip-favorite <? echo (rand(0,1)) ?  'active': ''; ?>" title="Добавить в избранное"></a>
							<span class="divider"></span>
							<a href="#" class="ip-cart"></a>
						</div>
					</div>
				</div>
				<?  endwhile; ?>
			</div>
		</div>
	</section>
	<section class="container-fluid same-model">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="mt0">Та же модель</h3>
				</div>
			</div>
			<div class="row items-grid small-grid">
				<?
					$i=0;

					while($i++<10):
						$cur = rand(1,3);
				?>
				<div class="col-md-3 item-pill">
					<div class="item-cnt" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
						<a href="#" class="img-cnt">
							<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
						</a>
						<div class="ip-title">
							<span class="quality">HD</span> 00:<?  echo rand(10, 59); ?>
						</div>
						<div class="ip-buttons">
							<a href="#" class="ip-favorite <? echo (rand(0,1)) ?  'active': ''; ?>" title="Добавить в избранное"></a>
							<span class="divider"></span>
							<a href="#" class="ip-cart"></a>
						</div>
					</div>
				</div>
				<?  endwhile; ?>
			</div>
		</div>
	</section>
	<section class="container">
		<div class="row mb100">
			<div class="col-md-12 tag-block">
				<h4>
					Ключевые слова
				</h4>
				<a href="#">авант́юра</a>,
				<a href="#">авто</a>,
				<a href="#">автомашина</a>,
				<a href="#">автомобиль</a>,
				<a href="#">автомобиля</a>,
				<a href="#">балка</a>,
				<a href="#">беззаботный</a>,
				<a href="#">беспечально</a>,
				<a href="#">бриз</a>,
				<a href="#">бросить</a>,
				<a href="#">брус</a>,
				<a href="#">быстро</a>,
				<a href="#">быстрый</a>,
				<a href="#">вагон</a>,
				<a href="#">веселье</a>,
				<a href="#">весёлый</a>,
				<a href="#">ветер</a>,
				<a href="#">ветерок</a>,
				<a href="#">ветрило</a>,
				<a href="#">ветры</a>,
				<a href="#">волнующий</a>,
				<a href="#">волос</a>,
				<a href="#">волосина</a>,
				<a href="#">волосинка</a>,
				<a href="#">волосы</a>,
				<a href="#">вольность</a>,
				<a href="#">воля</a>,
				<a href="#">выражение лица</a>,
				<a href="#">гора</a>,
				<a href="#">горный</a>,
				<a href="#">грань</a>,
				<a href="#">гримаса</a>,
				<a href="#">движущийся</a>,
				<a href="#">девица</a>,
				<a href="#">девочка</a>,
				<a href="#">девушка</a>,
				<a href="#">девчонка</a>,
				<a href="#">дезертировать</a>,
				<a href="#">делать перерыв</a>,
				<a href="#">дорога</a>,
				<a href="#">дороге</a>,
				<a href="#">женщина</a>,
				<a href="#">житель испаноязычной страны</a>,
				<a href="#">забава</a>,
				<a href="#">забавный</a>,
				<a href="#">забросить</a>,
				<a href="#">заводить</a>,
				<a href="#">заднее сиденье</a>,
				<a href="#">закат</a>,
				<a href="#">запинаться</a>,
				<a href="#">запнуться</a>,
				<a href="#">заход солнца</a>,
				<a href="#">избегать</a>,
				<a href="#">избежание</a>,
				<a href="#">избежать</a>,
				<a href="#">имидж</a>,
				<a href="#">испаноязычной</a>,
				<a href="#">испортить</a>,
				<a href="#">кабина</a>,
				<a href="#">кабриолет</a>,
				<a href="#">каникулы</a>,
				<a href="#">конвертируемый</a>,
				<a href="#">крепкий</a>,
				<a href="#">крепко</a>,
				<a href="#">куча</a>,
				<a href="#">латинец</a>,
				<a href="#">латинка</a>,
				<a href="#">латиноамериканец</a>,
				<a href="#">латиноамериканка</a>,
				<a href="#">латиноамериканский</a>,
				<a href="#">латинский американец</a>,
				<a href="#">лето</a>,
				<a href="#">лик</a>,
				<a href="#">лицо</a>,
				<a href="#">ломать</a>,
				<a href="#">ломаться</a>,
				<a href="#">люд</a>,
				<a href="#">люди</a>,
				<a href="#">машина</a>,
				<a href="#">машинное</a>,
				<a href="#">мина</a>,
				<a href="#">молодёжь</a>,
				<a href="#">молодой человек</a>,
				<a href="#">молодость</a>,
				<a href="#">надцать</a>,
				<a href="#">народ</a>,
				<a href="#">нарушать</a>,
				<a href="#">нарушить</a>,
				<a href="#">населять</a>,
				<a href="#">нация</a>,
				<a href="#">обгоняющий</a>,
				<a href="#">облик</a>,
				<a href="#">обратимо</a>,
				<a href="#">освобождение</a>,
				<a href="#">оставить</a>,
				<a href="#">оставление</a>,
				<a href="#">отгул</a>,
				<a href="#">отмена</a>,
				<a href="#">отпуск</a>,
				<a href="#">пассажир</a>,
				<a href="#">пассажирский</a>,
				<a href="#">передвигаться</a>,
				<a href="#">перекладина</a>,
				<a href="#">перемещаться</a>,
				<a href="#">перерыв</a>,
				<a href="#">поверхность surface</a>,
				<a href="#">подростково</a>,
				<a href="#">подростковый</a>,
				<a href="#">подросток</a>,
				<a href="#">поездка</a>,
				<a href="#">покинуть</a>,
				<a href="#">попоститься</a>,
				<a href="#">портить</a>,
				<a href="#">поститься</a>,
				<a href="#">потеха</a>,
				<a href="#">предстать</a>,
				<a href="#">преждевременно</a>,
				<a href="#">приключ́ение</a>,
				<a href="#">приключение</a>,
				<a href="#">приход</a>,
				<a href="#">проводить лето</a>,
				<a href="#">проходить</a>,
				<a href="#">прочно</a>,
				<a href="#">прочный</a>,
				<a href="#">пустыня</a>,
				<a href="#">пустяк</a>,
				<a href="#">пустяки</a>,
				<a href="#">путешествие</a>,
				<a href="#">путешествовать</a>,
				<a href="#">путь</a>,
				<a href="#">радость</a>,
				<a href="#">развлечение</a>,
				<a href="#">разлом</a>,
				<a href="#">резвит автомобиль</a>,
				<a href="#">рискнуть</a>,
				<a href="#">рисковать</a>,
				<a href="#">родня</a>,
				<a href="#">рожа</a>,
				<a href="#">рукоятки подняли</a>,
				<a href="#">рязори́ть</a>,
				<a href="#">рязоря́ть</a>,
				<a href="#">свобода</a>,
				<a href="#">скоростной</a>,
				<a href="#">скорый</a>,
				<a href="#">сломать</a>,
				<a href="#">сломаться</a>,
				<a href="#">служанка</a>,
				<a href="#">смело встречать</a>,
				<a href="#">смешной</a>,
				<a href="#">солнечность</a>,
				<a href="#">солнечный свет</a>,
				<a href="#">солнце</a>,
				<a href="#">спасаться</a>,
				<a href="#">спастись</a>,
				<a href="#">спешащий</a>,
				<a href="#">споткнуться</a>,
				<a href="#">сталкиваться</a>,
				<a href="#">тачка</a>,
				<a href="#">транспортное средство</a>,
				<a href="#">трогательный</a>,
				<a href="#">улыбаться</a>,
				<a href="#">улыбка</a>,
				<a href="#">улыбнуться</a>,
				<a href="#">усмехаться</a>,
				<a href="#">усмехнуться</a>,
				<a href="#">усмешка</a>,
				<a href="#">утеха</a>,
				<a href="#">фасад front</a>,
				<a href="#">физиономия</a>,
				<a href="#">циферблат</a>,
				<a href="#">этнический</a>,
				<a href="#">этническо</a>,
				<a href="#">юность</a>,
				<a href="#">юноша</a>,
				<a href="#">юношеский</a>,
				<a href="#">языческий</a>
			</div>

		</div>
	</section>



	

<? include('footer.php'); ?>