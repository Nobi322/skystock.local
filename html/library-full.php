<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb12">
				<form class="col-md-5 search-col">
					<input type="text" class="form-control">
					<button type="submit" class="btn btn-primary"><i class="i-magni"></i> Поиск</button>
				</form>
				<div class="col-md-7 return-link">
					<a href="#"><i class="fa fa-angle-left"></i>Вернуться назад</a>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-8">
				<h2 class="my-libs">
					Мои видеобиблиотеки <span class="counter">(0)</span>
				</h2>
			</div>
			<div class="col-md-4 text-right pt30">
				Сортировать по:
				<form class="sort-form">
					<select class="sel-24">
						<option>Самые новые</option>
						<option>Самые крутые</option>
					</select>
				</form>
			</div>
			<div class="col-md-12 line-divider"></div>

		</div>
	</div>
	<section class="container full-libs">
		<div class="row pt30">
			<?
				$i=0;

				while($i++<16):
					$cur = rand(1,3);
			?>
			<div class="sp-5-col">
				<div class="lib-item">
					<a href="#" class="img-cnt">
						<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
					</a>
					<div class="lib-title">
						<a href="#" class="lib-name">
							Роботы <span>(20)</span>
						</a>
						<p class="lib-redact">
							Отредактировано: 01.08.2015
						</p>
					</div>
				</div>
			</div>
			<?  endwhile; ?>
		</div>
	</section>



	

<? include('footer.php'); ?>