<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb12">
				<form class="col-md-5 search-col">
					<input type="text" class="form-control">
					<button type="submit" class="btn btn-primary"><i class="i-magni"></i> Поиск</button>
				</form>
				<div class="col-md-7 return-link">
					<a href="#"><i class="fa fa-angle-left"></i>Вернуться назад</a>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-6">
				<ul class="lib-crumps">
					<li>
						<a href="#">Мои видеобиблиотеки </a>
					</li>
					<li>
						Маугли <span>(4 клипа)</span>
					</li>
				</ul>
			</div>
			<div class="col-md-6 text-right pt3">
				<a href="#" class="bt-buy">Добавить все в корзину</a>
				<a href="#" class="bt-share">Поделиться</a>
				<a href="#" class="bt-delete">Удалить</a>
			</div>
			<div class="col-md-12 line-divider"></div>

		</div>
	</div>
	<section class="container inside-libs">
		<div class="row items-grid small-grid">
			<?
			$i=0;

			while($i++<4):
				$cur = rand(1,3);
				?>
				<div class="col-md-3 item-pill">
					<div class="item-cnt" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
						<a href="#" class="img-cnt">
							<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
						</a>
						<div class="ip-title">
							<span class="quality">HD</span> 00:<?  echo rand(10, 59); ?>
						</div>
						<div class="ip-buttons">
							<a href="#" class="ip-delete" title="Добавить в избранное" data-item-no="54fdfsfb"></a>
							<span class="divider"></span>
							<a href="#" class="ip-cart"></a>
						</div>
					</div>
				</div>
			<?  endwhile; ?>
		</div>
	</section>

	<section class="container recommend mb100">
		<div class="row">
			<div class="col-md-12">
				<h5>
					Рекомендованно для Вас
				</h5>
			</div>
		</div>
		<div class="row items-grid small-grid">
			<?
			$i=0;

			while($i++<10):
				$cur = rand(1,3);
				?>
				<div class="col-md-3 item-pill">
					<div class="item-cnt" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
						<a href="#" class="img-cnt">
							<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
						</a>
						<div class="ip-title">
							<span class="quality">HD</span> 00:<?  echo rand(10, 59); ?>
						</div>
						<div class="ip-buttons">
							<a href="#" class="ip-favorite <? echo (rand(0,1)) ?  'active': ''; ?>" title="Добавить в избранное" data-item-no="54fdfsfb"></a>

						</div>
					</div>
				</div>
			<?  endwhile; ?>
		</div>
	</section>



	

<? include('footer.php'); ?>