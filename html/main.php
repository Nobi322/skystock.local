<? include('header.php'); ?>

<section class="search-block container-fluid"  data-vide-bg="http://vjs.zencdn.net/v/oceans.mp4" data-vide-options="loop: true, muted: true, position: 0% 0%">

	<div class="container">
		<div class="row">
			<div class="col-md-8 center-block search-big">
				<form>
					<input type="text" placeholder="Поиск видео">
					<button type="submit" class="btn btn-search"><i class="i-magni"></i> Поиск</button>
				</form>
			</div>
		</div>
	</div>
</section>
<section class="container">
	<div class="row">
		<div class="col-md-12 text-center pt40">
			<h1>Категории</h1>
			<div class="categories-block">
				<div class="row">
					<?
					$i=0;
					$t = array(
						"Технологии",
						"Люди",
						"Фоны/текстуры",
						"Природа",
						"Спорт/отдых",
						"Искуство"
					);
					while($i++<6):
					?>
						<div class="col-md-4 ">
							<a href="#" class="cat-item">
								<div class="img-cnt">
									<img src="img/content/cat-<? echo $i; ?>.jpg">
								</div>
								<div class="cat-text">
									<? echo $t[$i-1]; ?>
								</div>
							</a>
						</div>
					<? endwhile; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul class="speech-block">
				<?
				$y=0;
				$ti = array(
					"Анимация",
					"Аэрофотосъемка",
					"Бизнес/финансы",
					"Еда и напитки",
					"Животные/дикая природа",
					"Замедленная съемка",
					"Здания/ Достопримечательности ",
					"Здоровье",
					"Знаки/Символы",
					"Знаменитости",
					"Искусство",
					"Лицензия Editorial",
					"Люди",
					"Мода ",
					"Наука",
					"Образование",
					"Праздники",
					"Предметы",
					"Природа",
					"Промышленность",
					"Путешествия",
					"Рапид-съёмка",
					"Религия",
					"Спорт/Отдых",
					"Технологии",
					"Транспорт",
					"Фоны/текстуры",
					"Хромакей"
				);

				while($y++<count($ti)):
					?>
				<li>
					<a href="#">
						<? echo $ti[$y-1]; ?>
					</a>
				</li>
				<? endwhile; ?>
			</ul>
		</div>
	</div>
</section>
<section class="tarif">
	<div class="container">
		<div class="row">
			<div class="col-md-12 line-divider"></div>
			<div class="col-md-12 tarif-single">
				Скачайте один видеоклип: &nbsp;&nbsp;
				<span class="price">19 грн </span><span class="type">Web</span><span class="divider"></span>
				<span class="price">49 грн </span><span class="type">HD</span><span class="divider"></span>
				<span class="price">79 грн </span><span class="type">SD</span><span class="divider"></span>
				<span class="price">199 грн </span><span class="type">4K</span>
			</div>
			<div class="col-md-12 line-divider"></div>
			<div class="col-md-4 tarif-single">
				Экономьте на видео:
			</div>
			<div class="col-md-7">

				<a href="#" class="chose-packet">
					Выберите подходящий пакет подписки
					<i class="i-arrow-round"></i>
				</a>
			</div>
		</div>
	</div>
</section>
<section class="register-slide container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-7 center-block">
				<form class="register-form">
					<h5>Создать бесплатный аккаунт</h5>
					<div class="row">
						<div class="col-md-12">
							<input type="email" class="form-control" placeholder="Ваш e-mail"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ваше имя"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="password" class="form-control" placeholder="Введите пароль"/>
						</div>
					</div>
					<div class="row mb15 mt20">
						<div class="col-md-10 col-md-offset-1">
							<input id="subscribe" type="checkbox">
							<label for="subscribe" class="text">Да, присылайте мне обновления и специальные предложения от SkyCap-stock по электронной почте.</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<input id="accept" type="checkbox" required>
							<label for="accept" class="text">Я принимаю Условия использования сайта и Условия лицензирования SkyCap-stock.</label>
						</div>
					</div>
					<div class="row mt40">
						<div class="col-md-10 col-md-offset-1">
							<button class="btn btn-default">Зарегистрироваться</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

	

<? include('footer.php'); ?>