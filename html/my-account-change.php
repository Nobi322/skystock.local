<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb25">
				<div class="col-md-12">
					<h5 class="bold">
						Ваш аккаунт
					</h5>
					<ul class="nav nav-pills account-pills mt20">
						<li role="presentation" class="active"><a href="#">Планы </a></li>
						<li role="presentation"><a href="#">Профиль</a></li>
						<li role="presentation"><a href="#">Выставление счетов</a></li>
						<li role="presentation"><a href="#">История покупок</a></li>
						<li role="presentation"><a href="#">Настройки</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h2 class="my-libs">
					Изменить почтовый адрес
				</h2>
			</div>
			<div class="col-md-12 line-divider"></div>
			<form class="col-md-8 pt30 pb80">
				<div class="row">
					<div class="col-md-3 form-text">
						Имя
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Фамилия
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Почтовый адрес 1
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Почтовый адрес 2
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Город
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Штат/Область
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Почтовый индекс
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Номер телефона
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Номер факса
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 form-text">
						Страна
					</div>
					<div class="col-md-6">
						<select class="sel-24">
							<option>США</option>
							<option>Украина</option>
							<option>Россия</option>
							<option>Гондурас</option>
							<option>Крыжополь</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5 col-md-offset-4">
						<button type="submit" class="btn btn-default mt20">Сохранить изменения</button>
					</div>
				</div>
			</form>
		</div>
	</div>



	

<? include('footer.php'); ?>