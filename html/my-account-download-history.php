<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb25">
				<div class="col-md-12">
					<h5 class="bold">
						Ваш аккаунт
					</h5>
					<ul class="nav nav-pills account-pills mt20">
						<li role="presentation"><a href="#">Планы </a></li>
						<li role="presentation"><a href="#">Профиль</a></li>
						<li role="presentation"><a href="#">Выставление счетов</a></li>
						<li role="presentation" class="active"><a href="#">История покупок</a></li>
						<li role="presentation"><a href="#">Настройки</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h2 class="my-libs">
					История покупок
				</h2>
			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="down-history-table">
					<tbody>
					<tr>
						<td class="ct-img">
							<a href="#"><img src="img/img-tmp-1.jpg" alt=""/></a>
						</td>
						<td>
							HD <span class="ct-type">Клип:</span> 2901469
						</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown" >
									Скачать
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Скачать HD</a></li>
									<li><a href="#">Скачать SD</a></li>
									<li><a href="#">Скачать Web</a></li>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td class="ct-img">
							<a href="#"><img src="img/img-tmp-2.jpg" alt=""/></a>
						</td>
						<td>
							HD <span class="ct-type">Клип:</span> 2901469
						</td>
						<td>
							<a href="#" class="btn btn-success">Скачать</a>
						</td>
					</tr>
					<tr>
						<td class="ct-img">
							<a href="#"><img src="img/img-tmp-3.jpg" alt=""/></a>
						</td>
						<td>
							HD <span class="ct-type">Клип:</span> 2901469
						</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown" >
									Скачать
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Скачать HD</a></li>
									<li><a href="#">Скачать SD</a></li>
									<li><a href="#">Скачать Web</a></li>
								</ul>
							</div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<nav>
					<ul class="pagination ">
						<li>
							<a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
							<a href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>



	

<? include('footer.php'); ?>