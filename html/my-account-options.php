<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb25">
				<div class="col-md-12">
					<h5 class="bold">
						Ваш аккаунт
					</h5>
					<ul class="nav nav-pills account-pills mt20">
						<li role="presentation" class="active"><a href="#">Планы </a></li>
						<li role="presentation"><a href="#">Профиль</a></li>
						<li role="presentation"><a href="#">Выставление счетов</a></li>
						<li role="presentation"><a href="#">История покупок</a></li>
						<li role="presentation"><a href="#">Настройки</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<form id="account_preferences_form" method="POST" class="pad-top-small mb100">
			<input name="_CSRF_TOKEN" value="ec7b4f4a6b00f7f4c33f1bd4cef42e972e0177cdc4ecb049" type="hidden">

			<fieldset class="pad-bottom mb25">
				<div class="row">
					<div class="col-md-12 pt40">
						<h2 class="my-libs">
							Настройки электронной почты
						</h2>
					</div>
					<div class="col-md-12 line-divider"></div>
				</div>
				<p>
					Какие сообщения вы бы хотели получать от Shutterstock?
				</p>
				<label class="checkbox" for="shutterstock_news">
					<input id="shutterstock_news" name="email_updates" checked="checked" class="ch-border" type="checkbox">Вдохновение для творчества, обучающие материалы и контент, отобранный профессионалами.
				</label>
				<label class="checkbox" for="partner_offers">
					<input id="partner_offers" name="email_partner" checked="checked" class="ch-border" type="checkbox">Специальные предложения и выгодные акции.
				</label>
				<label class="checkbox" for="monthly_newsletter">
					<input id="monthly_newsletter" name="email_newsletter" checked="checked" class="ch-border" type="checkbox">Информация о продукте и обновления.
				</label>
			</fieldset>
			<fieldset class="pad-bottom mb25">
				<h3>Язык сообщений</h3>
				<div class="row">
					<div class="col-md-12 line-divider"></div>
				</div>
				<p>
					Выберите язык для электронных сообщений от Shutterstock. При этом вы можете просматривать наш сайт на любом языке - выбранный вами язык сообщений останется прежним.
				</p>
				<div class="row ">
					<div class="col-md-5">
						<select name="language" >
							<option value="">-- выберите ниже --
							</option><option value="zh">Китайский</option>
							<option value="cs">Czech</option>
							<option value="da">Danish</option>
							<option value="nl">Голландский</option>
							<option value="en">Английский</option>
							<option value="fi">Finnish</option>
							<option value="fr">Французский</option>
							<option value="de">Немецкий</option>
							<option value="hu">Hungarian</option>
							<option value="it">Итальянский</option>
							<option value="ja">Японский</option>
							<option value="ko">Korean</option>
							<option value="nb">Norwegian</option>
							<option value="pl">Polish</option>
							<option value="pt">Португальский</option>
							<option value="ru" selected="">Russian</option>
							<option value="es">Испанский</option>
							<option value="sv">Swedish</option>
							<option value="th">Thai</option>
							<option value="tr">Turkish</option>
						</select>
					</div>
				</div>



			</fieldset>
			<fieldset class="pad-bottom mb20">

				<h3>Показать настройки</h3>
				<div class="row">
					<div class="col-md-12 line-divider"></div>
				</div>
				<p>
					Выберите единицы измерения для размеров изображения
				</p>
				<label class="radio" for="display_units_inches">
					<input name="display_units" id="display_units_inches" value="1" type="radio">Дюймы
				</label>
				<label class="radio" for="display_units_centimeters">
					<input name="display_units" id="display_units_centimeters" value="0" checked="checked" type="radio">Сантиметры
				</label>
			</fieldset>
			<input name="action" value="update_preferences" type="hidden">
			<button class="btn btn-primary" value="save" name="save_btn" id="save-changes" type="submit">Сохранить изменения</button>

		</form>
	</div>



	

<? include('footer.php'); ?>