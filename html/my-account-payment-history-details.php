<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb25">
				<div class="col-md-12">
					<h5 class="bold">
						Ваш аккаунт
					</h5>
					<ul class="nav nav-pills account-pills mt20">
						<li role="presentation"><a href="#">Планы </a></li>
						<li role="presentation"><a href="#">Профиль</a></li>
						<li role="presentation"><a href="#">Выставление счетов</a></li>
						<li role="presentation" class="active"><a href="#">История покупок</a></li>
						<li role="presentation"><a href="#">Настройки</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h2 class="my-libs">
					История покупок
				</h2>
			</div>

		</div>
		<div class="row">
			<div class="col-md-6">
				<h3>
					Подробности заказа
					<a href="#">2321455454sdd</a>
				</h3>
				<table class="profile-table">
					<tbody><tr>
						<td>Дата покупки</td>
						<td>31 Янв. 2015</td>
					</tr>
					<tr>
						<td>
							Способ оплаты
						</td>
						<td>
							Privat24
						</td>
					</tr>
					<tr>
						<td>Статус</td>
						<td>
							<label class="label label-success">Доставлено</label><br><br>
							<label class="label label-primary">В ожидании</label><br><br>
							<label class="label label-warning">Отменено</label><br><br>
							<label class="label label-danger">Ошибка</label>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-5 pull-right">
				<h3>
					Состав покупки
				</h3>
				<table class="profile-table items">
					<tbody>
					<tr>
						<td>Видео "летающие панды" HD</td>
						<td>25 uah</td>
					</tr>
					<tr>
						<td>Видео "летающие панды" HD</td>
						<td>25 uah</td>
					</tr>
					<tr>
						<td>
							<b>Итого</b>
						</td>
						<td class="text-right">
							50 uah
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>



	

<? include('footer.php'); ?>