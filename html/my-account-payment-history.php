<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb25">
				<div class="col-md-12">
					<h5 class="bold">
						Ваш аккаунт
					</h5>
					<ul class="nav nav-pills account-pills mt20">
						<li role="presentation"><a href="#">Планы </a></li>
						<li role="presentation"><a href="#">Профиль</a></li>
						<li role="presentation"><a href="#">Выставление счетов</a></li>
						<li role="presentation" class="active"><a href="#">История покупок</a></li>
						<li role="presentation"><a href="#">Настройки</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h2 class="my-libs">
					История покупок
				</h2>
			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<table>
					<thead>
					<tr>
						<th>Дата</th>
						<th>Номер заказа</th>
						<th>Товар</th>
						<th>Способ оплаты</th>
						<th>Сумма</th>
						<th>Статус</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>31 Янв. 2015</td>
						<td>
							<a href="#">874d5sadas23</a>
						</td>
						<td>
							<a href="#">Видео "Летающие панды" HD</a>
						</td>
						<td>
							Privat24
						</td>
						<td>30.00 uah</td>
						<td>
							Принят
						</td>
					</tr>
					<tr>
						<td>31 Янв. 2015</td>
						<td>
							<a href="#">874d5sadas23</a>
						</td>
						<td>
							<a href="#">Видео "Летающие панды" HD</a>
						</td>
						<td>
							Privat24
						</td>
						<td>30.00 uah</td>
						<td>
							Принят
						</td>
					</tr>
					<tr>
						<td>31 Янв. 2015</td>
						<td>
							<a href="#">874d5sadas23</a>
						</td>
						<td>
							<a href="#">Видео "Летающие панды" HD</a>
						</td>
						<td>
							Privat24
						</td>
						<td>30.00 uah</td>
						<td>
							Принят
						</td>
					</tr>
					<tr>
						<td>31 Янв. 2015</td>
						<td>
							<a href="#">874d5sadas23</a>
						</td>
						<td>
							<a href="#">Видео "Летающие панды" HD</a>
						</td>
						<td>
							Privat24
						</td>
						<td>30.00 uah</td>
						<td>
							Принят
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<nav>
					<ul class="pagination ">
						<li>
							<a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
							<a href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>



	

<? include('footer.php'); ?>