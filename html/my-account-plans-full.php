<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb25">
				<div class="col-md-12">
					<h5 class="bold">
						Ваш аккаунт
					</h5>
					<ul class="nav nav-pills account-pills mt20">
						<li role="presentation" class="active"><a href="#">Планы </a></li>
						<li role="presentation"><a href="#">Профиль</a></li>
						<li role="presentation"><a href="#">Выставление счетов</a></li>
						<li role="presentation"><a href="#">История покупок</a></li>
						<li role="presentation"><a href="#">Настройки</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h2 class="my-libs">
					Подписки
				</h2>
			</div>
			<div class="col-md-12 line-divider"></div>

		</div>
		<div class="row">
			<div class="col-md-12">

			</div>
		</div>
		<div class="row">

			<div class="col-md-12">
				<h3 class="plan-title">
					Изображения
				</h3>

				<div class="row plan-pill">
					<div class="col-md-12">
						<p class="h4">Картинки по запросу</p>
					</div>
					<div class="col-md-6">
						<p>5 Загрузок, Все JPEG/Vector разрешения</p>
						<p>
							Стандартная лицензия
						</p>
						<form class="renewal">

							<label class="ch-renewal on">
								Автовозобновление
								<input type="checkbox" name="img-renev" class="ch-border"/>
							</label>
						</form>
					</div>
					<div class="col-md-6 text-right">
						<p>
							Подписка истекает
						</p>
						<span class="label label-success">365</span>
						<!--  есть еще label-warning - желтый, и label-danger - красный если мало времени осталось если будет желание реализовать -->
						<p class="mt15">
							Загрузок осталось
						</p>
						<span class="label label-success">5/5</span>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<h3 class="plan-title">
					Видео
				</h3>
				<div class="row plan-pill">
					<div class="col-md-12">
						<p class="h4">Видео по запросу</p>
					</div>
					<div class="col-md-6">
						<p>5 Загрузок, Все AVI/RAV разрешения</p>
						<p>
							Стандартная лицензия
						</p>
						<form class="renewal">

							<label class="ch-renewal on">
								Автовозобновление
								<input type="checkbox" name="img-renev" class="ch-border"/>
							</label>
						</form>
					</div>
					<div class="col-md-6 text-right">
						<p>
							Подписка истекает
						</p>
						<span class="label label-success">365</span>
						<!--  есть еще label-warning - желтый, и label-danger - красный если мало времени осталось если будет желание реализовать -->
						<p class="mt15">
							Загрузок осталось
						</p>
						<span class="label label-danger">1/5</span>
					</div>
				</div>
			</div>
		</div>
	</div>



	

<? include('footer.php'); ?>