<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb25">
				<div class="col-md-12">
					<h5 class="bold">
						Ваш аккаунт
					</h5>
					<ul class="nav nav-pills account-pills mt20">
						<li role="presentation"><a href="#">Планы </a></li>
						<li role="presentation" class="active"><a href="#">Профиль</a></li>
						<li role="presentation"><a href="#">Выставление счетов</a></li>
						<li role="presentation"><a href="#">История покупок</a></li>
						<li role="presentation"><a href="#">Настройки</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h2 class="my-libs">
					Профиль
				</h2>
			</div>
			<div class="col-md-12 line-divider"></div>
			<div class="col-md-12">
				<table class="profile-table">
					<tr>
						<td>Имя пользователя</td>
						<td>iren9879874</td>
					</tr>
					<tr>
						<td>Пароль</td>
						<td>
							<a href="#">Изменить пароль</a></td>
					</tr>
					<tr>
						<td>Адрес электронной почты</td>
						<td>
							irem.mamay@gmail.com<br>

							<a href="#">Изменить адрес электронной почты</a>
						</td>
					</tr>
					<tr>
						<td>Контактная информация</td>
						<td>
							<a href="#">Изменить почтовый адрес</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>



	

<? include('footer.php'); ?>