<? include('header.php'); ?>

<section class="search-block small container-fluid" style="background: url('img/plan-bg.jpg')">
	<div class="container">
		<div class="row">
			<div class="col-md-10 center-block search-big">
				<form>
					<input type="text" placeholder="Поиск видео">
					<button type="submit" class="btn btn-search"><i class="i-magni"></i> Поиск</button>
				</form>
			</div>
		</div>
	</div>
</section>
	<section class="tarif mb40">
		<div class="container">
			<div class="row">
				<div class="col-md-12 plan-info">
					<h2>
						Обзор плана
					</h2>
					<p>
						На данный момент вы пользуетесь бесплатным аккаунтом для просмотра.
					</p>
				</div>
				<div class="col-md-12 line-divider"></div>
				<div class="col-md-12 tarif-single small">
					Скачайте один видеоклип: &nbsp;&nbsp;
					<span class="price">19 грн </span><span class="type">Web</span><span class="divider"></span>
					<span class="price">49 грн </span><span class="type">HD</span><span class="divider"></span>
					<span class="price">79 грн </span><span class="type">SD</span><span class="divider"></span>
					<span class="price">199 грн </span><span class="type">4K</span>
				</div>

			</div>
			<div class="row">
				<div class="col-md-7">

					<a href="#" class="chose-packet small">
						Выберите подходящий пакет подписки
						<i class="i-arrow-round"></i>
					</a>
				</div>
			</div>
		</div>
	</section>
<section class="container">
	<div class="row">
		<div class="col-md-12 my-history">
			<div class="mh-cnt">
				<ul class="nav nav-tabs mh-tab-list" role="tablist">
					<li role="presentation" class="active">
						<a href="#new-watch" aria-controls="home" role="tab" data-toggle="tab">Недавний Просмотр</a>
					</li>
					<li role="presentation">
						<a href="#new-search" aria-controls="home" role="tab" data-toggle="tab">Недавний Поиск</a>
					</li>
					<li role="presentation">
						<a href="#favorite" aria-controls="home" role="tab" data-toggle="tab">Избранное</a>
					</li>
				</ul>
				<div class="mh-tab tab-content">
					<div id="new-watch" role="tabpanel" class="tab-pane  active">
						<div class="mh-owl-carousel">
							<?
							$i=0;

							while($i++<7):
								$cur = rand(1,3);
								?>
								<a href="#" class="mh-item" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
									<div class="img-cnt">
										<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
									</div>
									<div class="mh-item-title">
										Text
									</div>
								</a>
							<?  endwhile; ?>
						</div>

					</div>
					<div id="new-search" role="tabpanel" class="tab-pane">
						<div class="mh-owl-carousel">
							<?
							$i=0;

							while($i++<7):
								$cur = rand(1,3);
								?>
								<a href="#" class="mh-item" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
									<div class="img-cnt">
										<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
									</div>
									<div class="mh-item-title">
										Text
									</div>
								</a>
							<?  endwhile; ?>
						</div>
					</div>
					<div id="favorite" role="tabpanel" class="tab-pane">
						<div class="mh-owl-carousel">
							<?
							$i=0;

							while($i++<7):
								$cur = rand(1,3);
								?>
								<a href="#" class="mh-item" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
									<div class="img-cnt">
										<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
									</div>
									<div class="mh-item-title">
										Text
									</div>
								</a>
							<?  endwhile; ?>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<section class="container">
	<div class="row">
		<div class="col-md-12 text-center pt60">
			<h1>Категории</h1>
			<div class="categories-block">
				<div class="row">
					<?
					$i=0;
					$t = array(
						"Технологии",
						"Люди",
						"Фоны/текстуры",
						"Природа",
						"Спорт/отдых",
						"Искуство"
					);
					while($i++<6):
					?>
						<div class="col-md-4 ">
							<a href="#" class="cat-item">
								<div class="img-cnt">
									<img src="img/content/cat-<? echo $i; ?>.jpg">
								</div>
								<div class="cat-text">
									<? echo $t[$i-1]; ?>
								</div>
							</a>
						</div>
					<? endwhile; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul class="speech-block">
				<?
				$y=0;
				$ti = array(
					"Анимация",
					"Аэрофотосъемка",
					"Бизнес/финансы",
					"Еда и напитки",
					"Животные/дикая природа",
					"Замедленная съемка",
					"Здания/ Достопримечательности ",
					"Здоровье",
					"Знаки/Символы",
					"Знаменитости",
					"Искусство",
					"Лицензия Editorial",
					"Люди",
					"Мода ",
					"Наука",
					"Образование",
					"Праздники",
					"Предметы",
					"Природа",
					"Промышленность",
					"Путешествия",
					"Рапид-съёмка",
					"Религия",
					"Спорт/Отдых",
					"Технологии",
					"Транспорт",
					"Фоны/текстуры",
					"Хромакей"
				);

				while($y++<count($ti)):
					?>
				<li>
					<a href="#">
						<? echo $ti[$y-1]; ?>
					</a>
				</li>
				<? endwhile; ?>
			</ul>
		</div>
	</div>
</section>
	<section class="container-fluid best-redaction">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mt30">
					<h3>Лучшее от редакции</h3>
				</div>
			</div>
			<div class="row items-grid small-grid">
				<?
				$i=0;

				while($i++<10):
					$cur = rand(1,3);
					?>
					<div class="col-md-3 item-pill">
						<div class="item-cnt" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
							<a href="#" class="img-cnt">
								<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
							</a>
							<div class="ip-title">
								<span class="quality">HD</span> 00:<?  echo rand(10, 59); ?>
							</div>
							<div class="ip-buttons">
								<a href="#" class="ip-favorite <? echo (rand(0,1)) ?  'active': ''; ?>" title="Добавить в избранное"></a>
								<span class="divider"></span>
								<a href="#" class="ip-cart"></a>
							</div>
						</div>
					</div>
				<?  endwhile; ?>
			</div>
		</div>
	</section>
	<section class="container-fluid new-video mb70">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="mt0">Новые</h3>
				</div>
			</div>
			<div class="row items-grid small-grid">
				<?
				$i=0;

				while($i++<10):
					$cur = rand(1,3);
					?>
					<div class="col-md-3 item-pill">
						<div class="item-cnt" data-video-src="video/video-tmp-<?  echo $cur; ?>.mp4" data-video-title="video title test" data-item-no="54fdfsfb">
							<a href="#" class="img-cnt">
								<img src="img/img-tmp-<?  echo $cur; ?>.jpg">
							</a>
							<div class="ip-title">
								<span class="quality">HD</span> 00:<?  echo rand(10, 59); ?>
							</div>
							<div class="ip-buttons">
								<a href="#" class="ip-favorite <? echo (rand(0,1)) ?  'active': ''; ?>" title="Добавить в избранное"></a>
								<span class="divider"></span>
								<a href="#" class="ip-cart"></a>
							</div>
						</div>
					</div>
				<?  endwhile; ?>
			</div>
		</div>
	</section>


	

<? include('footer.php'); ?>