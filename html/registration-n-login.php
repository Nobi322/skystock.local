<? include('header.php'); ?>



<section class="register-slide container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 form-blue mt40">
				<div class="row ">
					<div class="col-md-6 col-md-offset-1 p-divider">
						<form class="white">
							<h3>Создать аккаунт</h3>
							<div class="row pt20">
								<div class="col-md-12">
									<input type="email" class="form-control" placeholder="Ваш e-mail"/>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="text" class="form-control" placeholder="Ваше имя"/>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="password" class="form-control" placeholder="Введите пароль"/>
								</div>
							</div>
							<div class="row mb15 mt20">
								<div class="col-md-10 col-md-offset-1">
									<input id="subscribe" type="checkbox">
									<label for="subscribe" class="text">Да, присылайте мне обновления и специальные предложения от SkyCap-stock по электронной почте.</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10 col-md-offset-1">
									<input id="accept" type="checkbox" required>
									<label for="accept" class="text">Я принимаю Условия использования сайта и Условия лицензирования SkyCap-stock.</label>
								</div>
							</div>
							<div class="row mt40">
								<div class="col-md-10 col-md-offset-1">
									<button class="btn btn-default">Зарегистрироваться</button>
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-5 ">
						<form class="">
							<div class="row">
								<div class="col-md-9 col-md-offset-2 white mb20">
									<h3>Уже есть акаунт?</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" placeholder="Ваше имя или e-mail"/>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<input type="password" class="form-control" placeholder="Введите пароль"/>
								</div>
							</div>
							<div class="row mt20">
								<div class="col-md-8 col-md-offset-2">
									<button type="submit" class="btn btn-default">
										Войти
									</button>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 col-md-offset-2 pt22">
									<a href="#">Забыли пароль?</a>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

	

<? include('footer.php'); ?>