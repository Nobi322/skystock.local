<? include('header.php'); ?>
	<div class="container-fluid manipulation">
		<div class="container">
			<div class="row pt17 pb12">
				<form class="col-md-5 search-col">
					<input type="text" class="form-control">
					<button type="submit" class="btn btn-primary"><i class="i-magni"></i> Поиск</button>
				</form>
				<div class="col-md-7 return-link">
					<a href="#"><i class="fa fa-angle-left"></i>Вернуться назад</a>
				</div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="row pt40">
			<div class="col-md-12">
				<h1>h1. Bootstrap heading</h1>
				<h2>h2. Bootstrap heading</h2>
				<h3>h3. Bootstrap heading</h3>
				<h4>h4. Bootstrap heading</h4>
				<h5>h5. Bootstrap heading</h5>
				<h6>h6. Bootstrap heading</h6>

				<ul>
					<li>Lorem ipsum dolor sit amet</li>
					<li>Consectetur adipiscing elit</li>
					<li>Integer molestie lorem at massa</li>
					<li>Facilisis in pretium nisl aliquet</li>
					<li>Nulla volutpat aliquam velit
						<ul>
							<li>Phasellus iaculis neque</li>
							<li>Purus sodales ultricies</li>
							<li>Vestibulum laoreet porttitor sem</li>
							<li>Ac tristique libero volutpat at</li>
						</ul>
					</li>
					<li>Faucibus porta lacus fringilla vel</li>
					<li>Aenean sit amet erat nunc</li>
					<li>Eget porttitor lorem</li>
				</ul>

				<ol>
					<li>Lorem ipsum dolor sit amet</li>
					<li>Consectetur adipiscing elit</li>
					<li>Integer molestie lorem at massa</li>
					<li>Facilisis in pretium nisl aliquet</li>
					<li>Nulla volutpat aliquam velit</li>
					<li>Faucibus porta lacus fringilla vel</li>
					<li>Aenean sit amet erat nunc</li>
					<li>Eget porttitor lorem</li>
				</ol>

				<h3>Таблица №3</h3>
				<table>
					<thead>
					<tr>
						<th>№</th>
						<th>Название</th>
						<th>Характеристики</th>
						<th>Свойства</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<th scope="row">1</th>
						<td>Тыква</td>
						<td>Если Вы пожелаете перестать получать от нас информацию, Вы сможете отказаться от рассылки в любое время, пройдя по ссылке, приведенной в конце каждого письма.</td>
						<td>Вы сможете отказаться от рассылки в любое время, пройдя по ссылке, приведенной в конце каждого письма.</td>
					</tr>
					<tr>
						<th scope="row">2</th>
						<td>Тыква</td>
						<td>Если Вы пожелаете перестать получать от нас информацию, Вы сможете отказаться от рассылки в любое время, пройдя по ссылке, приведенной в конце каждого письма.</td>
						<td>Вы сможете отказаться от рассылки в любое время, пройдя по ссылке, приведенной в конце каждого письма.</td>
					</tr>
					<tr>
						<th scope="row">3</th>
						<td>Тыква</td>
						<td>Если Вы пожелаете перестать получать от нас информацию, Вы сможете отказаться от рассылки в любое время, пройдя по ссылке, приведенной в конце каждого письма.</td>
						<td>Вы сможете отказаться от рассылки в любое время, пройдя по ссылке, приведенной в конце каждого письма.</td>
					</tr>
					</tbody>
				</table>
				<h3>Форма</h3>
				<div class="row">
					<div class="col-md-6 mb100">
						<form class="form" method="POST" novalidate="novalidate">
							<div class="form-group">
								<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Ваше имя">
							</div>
							<div class="form-group">
								<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Ваш E-mail">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Ваш номер">
							</div>
							<div class="form-group">
								<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required="required" aria-required="true">
							</div>
							<div class="form-group">
								<textarea></textarea>
							</div>
							<div class="form-group">
								<input type="file" id="exampleInputFile">
								<p class="help-block">Example block-level help text here.</p>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox"> Check me out
								</label>
							</div>
							<button type="submit" class="btn btn-default">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>



	

<? include('footer.php'); ?>