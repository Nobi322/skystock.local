<?
if(!defined("site_root")){exit();}

include("content_list_vars.php");

$search_page="<table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr valign='top'>
		<td class='search_left'>
			{MENU}
		</td>
		<td class='search_right'>
			{RESULTS}
		</td>
	</tr>
</table>";

if(file_exists($DOCUMENT_ROOT."/".$site_template_url."catalog.tpl"))
{
	$search_page=file_get_contents($DOCUMENT_ROOT."/".$site_template_url."catalog.tpl");
}

$search_page=str_replace("{MENU}","{SEPARATOR}",$search_page);
$search_page=str_replace("{RESULTS}","{SEPARATOR}",$search_page);

$search_parts=explode("{SEPARATOR}",$search_page);

$search_header=$search_parts[0];
$search_middle=@$search_parts[1];
$search_footer=@$search_parts[2];

?>


<?=$search_header?>
<div class="container-fluid manipulation">
<div class="container">
<form id='listing_form' method="get" action="<?=site_root?>/index.php" style="margin:0px">
<?
//Default variables

if(isset($_REQUEST["id_parent"]))
{
	echo("<input type='hidden' name='id_parent' value='".(int)$_REQUEST["id_parent"]."'>");
}

if(isset($_REQUEST["acategory"]))
{
	echo("<input type='hidden' name='acategory' value='".(int)$_REQUEST["acategory"]."'>");
}

if(isset($_REQUEST["items"]))
{
	echo("<input type='hidden' name='items' value='".(int)$_REQUEST["items"]."'>");
}

if(isset($_REQUEST["c"]))
{
	echo("<input type='hidden' name='c' value='".result($_REQUEST["c"])."'>");
}

$vars_categories=build_variables("id_parent","acategory",true,"category");
$vars_portfolio=build_variables("user","portfolio");
$vars_author=build_variables("author","");
$vars_lightbox=build_variables("lightbox","");
$vars_format=build_variables("format","");

//End. Default variables.
?>
	
	
	<div class="row pt17">
		<div id="search_keywords" class="col-md-5 search-col">
			<input name="search" type="text" class="form-control" value="<?=$_GET["search"];?>" onClick="this.value=''">
			<button type="submit" class="btn btn-primary"><i class="i-magni"></i> <?=word_lang("search")?></button>
		</div>
		<div class="col-md-7 category-selected">
		<?
			
			if($id_parent!=5)
			{
				$sql2="select title from category where id_parent=".(int)$id_parent;
				$dr->open($sql2);
				if(!$dr->eof)
				{
					$search_title=$dr->row["title"];
				}
			}
			$search_title= $_GET["search"];
			
		?>
			Кадры <span class="sel"><?=$search_title?></span> (<?=$record_count?>)
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 line-divider"></div>
	</div>
	<div class="row">
		<div class="col-md-5 pb23">
			<select name="category" class="big-select" data-placeholder="Фильтровать по категориям" onchange="document.location=this.options[this.selectedIndex].value">
				<option value="">...</option>
				<?
					$sql2="select * from category";
					$dr->open($sql2);
					while(!$dr->eof)
					{
						$sel="";
						if(isset($_REQUEST["category"]) and $_REQUEST["category"]==$dr->row["title"])
						{
							$sel="selected";
						}
						?>
						<option value="<?=$dr->row["title"]?>" <?=$sel?>><?=$dr->row["title"]?></option>
						<?
						$dr->movenext();
					}
				?>
			</select>
		</div>
		<div class="col-md-7 resolution-filter">
			Разрешение:
			<ul>
			<?
				$sql2="select * from video_format";
				$dr->open($sql2);
				while(!$dr->eof)
				{
					?>
					<li><a href="<?=$vars_format?>&format=<?=$dr->row["name"]?>"><?=$dr->row["name"]?></a> (854)</li>
					<?
					$dr->movenext();
				}
			?>
			</ul>
		</div>
	</div>
	<script languages="javascript">
	 	function listing_submit()
	 	{
	 		$('#listing_form').submit();
	 	}
	 	
	 	function show_sub(value,value2)
	 	{
	 		if(document.getElementById(value).style.display=='none')
	 		{
	 			$("#"+value).slideDown("fast");
	 			document.getElementById(value2).className='search_title4';
	 			document.cookie = "z_" + value + "=" + escape (1) + ";path=/";
	 		}
	 		else
	 		{
	 			$("#"+value).slideUp("fast");
	 			document.getElementById(value2).className='search_title3';
	 			document.cookie = "z_" + value + "=" + escape (0) + ";path=/";
	 		}
	 	}
	 	

	</script>
	</form>
<?=$search_middle?>
<section class="container-fluid catalog">
<div class="container">
<div class="row pt20">
	<div class="col-md-7 additional-tags">
		Похожее:
		<a href="#">слоны,</a>
		<a href="#">белый тигр,</a>
		<a href="#">слоны в африке,</a>
		<a href="#">группа слонов</a>
	</div>
	<div class="col-md-5 ctl-controls">
		<div class="number-settings">
			<a href="#" class="nbs-controll"><i class="i-friction"></i></a>
			<div class="nbs-popup">
				<a href="#" class="i-arr-down"></a>
				Показать:
				<?=$itemsmenu?>
			</div>
		</div>
		<? if($record_count>$kolvo and $autopaging==0) { ?>
			<?=$paging_text?>
		<? } ?>
	</div>
</div>
<div class='row items-grid' id="flow_body">
	<?
		include("content_list_items.php");
					
		//Show result
		echo($search_content);
	?>
</div>

<div class="row mb60">
	<div class="col-md-5 ctl-controls pull-right">
		<div class="number-settings">
			<a href="#" class="nbs-controll"><i class="i-friction"></i></a>
			<div class="nbs-popup">
				<a href="#" class="i-arr-down"></a>
				Показать:
				<?=$itemsmenu?>
			</div>
		</div>
		<? if($record_count>$kolvo and $autopaging==0) { ?>
			<?=$paging_text?>
		<? } ?>
	</div>
</div>
</div>
</section>
<?=$search_footer?>

