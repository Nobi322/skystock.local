﻿<?
if(!defined("site_root")){exit();}

if($site_paypal_account!="")
{
	if(isset($_GET["mode"]) and $_GET["mode"]=="notification")
	{
		if($site_paypal_ipn==true)
		{
			$postdata=""; 
 			foreach ($_POST as $key=>$value) $postdata.=$key."=".urlencode($value)."&"; 
 			$postdata .= "cmd=_notify-validate"; 

 			$curl = curl_init(paypal_url); 
  			curl_setopt ($curl, CURLOPT_HEADER, 0); 
  			curl_setopt ($curl, CURLOPT_POST, 1); 
  			curl_setopt ($curl, CURLOPT_POSTFIELDS, $postdata); 
 			curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0); 
  			curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1); 
  			curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 1); 
  			$response = curl_exec ($curl); 
  			curl_close ($curl); 

			//$response = "VERIFIED";
			if ($response == "VERIFIED" and $_POST["payment_status"] == "Completed")
			{
				$transaction_id=transaction_add("paypal",$_POST["txn_id"],$_GET["product_type"],$_POST["item_number"]);

				if($_GET["product_type"]=="credits")
				{
					credits_approve($_POST["item_number"],$transaction_id);
					send_notification('credits_to_user',$_POST["item_number"]);
					send_notification('credits_to_admin',$_POST["item_number"]);
				}

				if($_GET["product_type"]=="subscription")
				{
					subscription_approve($_POST["item_number"]);
					send_notification('subscription_to_user',$_POST["item_number"]);
					send_notification('subscription_to_admin',$_POST["item_number"]);
				}

				if($_GET["product_type"]=="order")
				{
					order_approve($_POST["item_number"]);
					commission_add($_POST["item_number"]);

					coupons_add(order_user($_POST["item_number"]));
					send_notification('neworder_to_user',$_POST["item_number"]);
					send_notification('neworder_to_admin',$_POST["item_number"]);
				}
				if($_GET["product_type"]=="payout_seller" or $_GET["product_type"]=="payout_affiliate")
				{
					payout_approve($_POST["item_number"],$_GET["product_type"]);
				}
			}
		}
	}
	else
	{
		?>
		<form method="post" name="process" id="process" action="<?=paypal_url?>">
		<input type="hidden" name="rm" value="2"/>
		<input type="hidden" name="cmd" value="_xclick"/>
		<input type="hidden" name="business" value="<?=$site_paypal_account?>"/>
		<input type="hidden" name="cancel_return" value="<?=surl.site_root."/members/payments_result.php?d=2"?>"/>
		<input type="hidden" name="notify_url" value="<?=surl.site_root."/members/payments_process.php?mode=notification&product_type=".$product_type?>&processor=paypal"/>
		<input type="hidden" name="return" value="<?=surl.site_root."/members/payments_result.php?d=1"?>"/>
		<input type="hidden" name="item_name" value="<?=$product_name?>"/>
		<input type="hidden" name="item_number" value="<?=$product_id?>"/>
		<input type="hidden" name="amount" value="<?=$product_total?>"/>
		<input type="hidden" name="currency_code" value="<?=$currency_code1?>"/>
		</form>
		<?
	}		
}
?>