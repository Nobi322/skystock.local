<?
if(!defined("site_root")){exit();}


if(!isset($_SESSION["box_shopping_cart"]) or !isset($_SESSION["box_shopping_cart_lite"]))
{
	$box_shopping_cart=word_lang("empty shopping cart");
	$box_shopping_cart_lite=word_lang("empty shopping cart");
	$script_carts="";

	$cart_id=shopping_cart_id();
	$total=0;
	$quantity=0;

	$sql="select item_id,prints_id,publication_id,quantity,option1_id,option1_value,option2_id,option2_value,option3_id,option3_value from carts_content where id_parent=".$cart_id;
	$rs->open($sql);
	if(!$rs->eof)
	{
		$box_shopping_cart="<table border=0 cellpadding=3 cellspacing=1 class='tborder'><tr><td class='theader'><span class='smalltext'><b>ID</b></span></td><td class=theader><span class=smalltext><b>".word_lang("item")."</b></td><td class=theader><span class=smalltext><b>".word_lang("price")."</b></td><td class=theader><span class=smalltext><b>".word_lang("qty")."</b></td></tr>";
	
		while(!$rs->eof)
		{
			if($script_carts!="")
			{
				$script_carts.=",";
			}
			$script_carts.=$rs->row["publication_id"];
		
			if($rs->row["item_id"]>0)
			{
				$sql="select id,name,price,id_parent from items where id=".$rs->row["item_id"];
				$dr->open($sql);
				if(!$dr->eof)
				{
					$box_shopping_cart.="<tr><td class='tcontent'><span class='smalltext'><a href='".item_url($dr->row["id_parent"])."'>".$dr->row["id_parent"]."</a></td><td class=tcontent><span class=smalltext>".$dr->row["name"]."</td><td class=tcontent><span class=smalltext><span class='price'>".float_opt($dr->row["price"],2,true)."</span></td><td class=tcontent><span class=smalltext>".$rs->row["quantity"]."</td></tr>";
					$total+=$dr->row["price"]*$rs->row["quantity"];
				}
			}
		
			if($rs->row["prints_id"]>0)
			{
				$sql="select id_parent,title,price,itemid from prints_items where id_parent=".$rs->row["prints_id"];
				$dr->open($sql);
				if(!$dr->eof)
				{
					$price=define_prints_price($dr->row["price"],$rs->row["option1_id"],$rs->row["option1_value"],$rs->row["option2_id"],$rs->row["option2_value"],$rs->row["option3_id"],$rs->row["option3_value"]);
				
					$box_shopping_cart.="<tr><td class='tcontent'><span class='smalltext'><a href='".item_url($dr->row["itemid"])."'>".$dr->row["itemid"]."</a></td><td  class=tcontent><span class=smalltext>".word_lang("prints").": ".$dr->row["title"]."</td><td  class=tcontent><span class=smalltext><span class='price'>".float_opt($price,2,true)."</span></td><td class=tcontent><span class=smalltext>".$rs->row["quantity"]."</td></tr>";
					$total+=$price*$rs->row["quantity"];
				}
			}
		
			$quantity+=$rs->row["quantity"];
			$rs->movenext();
		}
	
		$box_shopping_cart.="</table><div class=smalltext style='margin-top:5'><b>".word_lang("total").":</b> ".currency(1).float_opt($total,2,true)." ".currency(2)."</div><div style='margin-top:5'><a href='".site_root."/members/shopping_cart.php' class='o'><b>".word_lang("view shopping cart")."</b></a></div>";

		$box_shopping_cart_lite="<a href='".site_root."/members/shopping_cart.php'>".word_lang("shopping cart")."</a> ".$quantity." (".currency(1).float_opt($total,2,true)." ".currency(2).")" ;
	}

	$script_carts="<script>cart_mass=new Array(".$script_carts.")</script>";
	
	$box_shopping_cart.=$script_carts;
	$box_shopping_cart_lite.=$script_carts;
	
	$_SESSION["box_shopping_cart"]=$box_shopping_cart;
	$_SESSION["box_shopping_cart_lite"]=$box_shopping_cart_lite;
}
else
{
	$box_shopping_cart=$_SESSION["box_shopping_cart"];
	$box_shopping_cart_lite=$_SESSION["box_shopping_cart_lite"];
}
?>