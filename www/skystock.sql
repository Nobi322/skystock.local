/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50538
 Source Host           : 127.0.0.1
 Source Database       : skystock

 Target Server Type    : MySQL
 Target Server Version : 50538
 File Encoding         : utf-8

 Date: 08/25/2015 15:05:23 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `affiliates_settings`
-- ----------------------------
DROP TABLE IF EXISTS `affiliates_settings`;
CREATE TABLE `affiliates_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `param` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `meaning` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `affiliates_settings`
-- ----------------------------
BEGIN;
INSERT INTO `affiliates_settings` VALUES ('1', 'Buyer signup commission', '15'), ('2', 'Seller signup commission', '10');
COMMIT;

-- ----------------------------
--  Table structure for `affiliates_signups`
-- ----------------------------
DROP TABLE IF EXISTS `affiliates_signups`;
CREATE TABLE `affiliates_signups` (
  `userid` int(11) DEFAULT NULL,
  `types` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `types_id` int(11) DEFAULT NULL,
  `rates` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `aff_referal` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `gateway` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `userid` (`userid`),
  KEY `types` (`types`),
  KEY `types_id` (`types_id`),
  KEY `total` (`total`),
  KEY `data` (`data`),
  KEY `aff_referal` (`aff_referal`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `affiliates_signups`
-- ----------------------------
BEGIN;
INSERT INTO `affiliates_signups` VALUES ('6355', 'subscription', '22', '15', '1.5', '1373815943', '7292', '1', null, null), ('6355', 'credits', '132', '15', '1.35', '1351948360', '7292', '1', null, null), ('6355', 'credits', '131', '15', '1.35', '1351948373', '7292', '1', null, null), ('6355', 'subscription', '21', '15', '1.5', '1351434542', '7292', '1', null, null), ('6355', 'credits', '28', '15', '0.15', '1312624808', '7292', '1', null, null), ('6355', 'credits', '27', '15', '0.15', '1312624378', '7292', '1', null, null), ('0', 'refund', '0', '0', '-1', '1301676718', '7292', '1', null, null), ('6355', 'credits', '99', '15', '0', '1337343807', '7292', '1', null, null), ('6355', 'orders', '119', '15', '0.15', '1345648846', '7292', '1', null, null), ('6355', 'credits', '137', '15', '0.15', '1351948337', '7292', '1', null, null), ('6355', 'credits', '136', '15', '0.15', '1351948338', '7292', '1', null, null), ('6355', 'credits', '150', '15', '0.15', '1355489185', '7292', '1', null, null), ('6355', 'subscription', '26', '15', '1.5', '1355489279', '7292', '1', null, null), ('6355', 'orders', '123', '15', '0.15', '1355489371', '7292', '1', null, null), ('0', 'refund', '0', '0', '-1', '1362830880', '7292', '1', null, null), ('0', 'refund', '0', '0', '-1', '1376826211', '7292', '0', 0x0a56544232343a20313233342d353637382d313233342d3536373831, 'bank'), ('6355', 'credits', '185', '15', '0.15', '1362832884', '7292', '1', null, null), ('6355', 'credits', '197', '15', '0.15', '1365838033', '7292', '1', null, null), ('6355', 'credits', '198', '15', '0.15', '1365838197', '7292', '1', null, null), ('6355', 'credits', '199', '15', '0.15', '1365838297', '7292', '1', null, null), ('0', 'refund', '0', '0', '-1', '1376826328', '7292', '0', '', 'other');
COMMIT;

-- ----------------------------
--  Table structure for `affiliates_stats`
-- ----------------------------
DROP TABLE IF EXISTS `affiliates_stats`;
CREATE TABLE `affiliates_stats` (
  `userid` int(11) DEFAULT NULL,
  `seller` int(11) DEFAULT NULL,
  `buyer` int(11) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `aff_referal` int(11) DEFAULT NULL,
  KEY `userid` (`userid`),
  KEY `seller` (`seller`),
  KEY `buyer` (`buyer`),
  KEY `data` (`data`),
  KEY `aff_referal` (`aff_referal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `affiliates_stats`
-- ----------------------------
BEGIN;
INSERT INTO `affiliates_stats` VALUES ('6355', '0', '1', '1301644908', '127.0.0.1', '7292'), ('3931', '1', '0', '1301645011', '127.0.0.1', '7292');
COMMIT;

-- ----------------------------
--  Table structure for `audio`
-- ----------------------------
DROP TABLE IF EXISTS `audio`;
CREATE TABLE `audio` (
  `id_parent` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `keywords` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `author` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `viewed` int(11) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `folder` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `holder` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `source` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `format` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `duration` int(10) DEFAULT NULL,
  `content_type` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `free` tinyint(1) DEFAULT NULL,
  `downloaded` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `model` int(10) DEFAULT NULL,
  `server1` int(11) DEFAULT NULL,
  `server2` int(11) DEFAULT NULL,
  `server3` tinyint(1) DEFAULT NULL,
  `examination` tinyint(1) DEFAULT NULL,
  `category2` int(11) DEFAULT NULL,
  `category3` int(11) DEFAULT NULL,
  `google_x` double DEFAULT NULL,
  `google_y` double DEFAULT NULL,
  `refuse_reason` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `adult` tinyint(4) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `data` (`data`),
  KEY `title` (`title`),
  KEY `published` (`published`),
  KEY `featured` (`featured`),
  KEY `viewed` (`viewed`),
  KEY `downloaded` (`downloaded`),
  KEY `author` (`author`),
  KEY `userid` (`userid`),
  KEY `free` (`free`),
  KEY `rating` (`rating`),
  KEY `examination` (`examination`),
  KEY `category2` (`category2`),
  KEY `category3` (`category3`),
  KEY `google_x` (`google_x`),
  KEY `google_y` (`google_y`),
  KEY `server1` (`server1`),
  KEY `server2` (`server2`),
  KEY `duration` (`duration`),
  KEY `adult` (`adult`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `audio`
-- ----------------------------
BEGIN;
INSERT INTO `audio` VALUES ('7658', 'Waxwing', 0x54686520626972642073696e67732e, 'bird,sing', 'common', '1361096227', '29', '1', '0', '0', '7658', 'John Smith', 'MP3', 'Stereo', '23', 'Common', '0', '0', null, '0', '2', '0', null, '0', '5', '5', '0', '0', null, '/stock-audio/waxwing-7658.html', '0'), ('7657', 'Yellowhammer', 0x5468652079656c6c6f7768616d6d65722073696e67732e, 'bird,sing', 'common', '1361096017', '8', '1', '0', '0', '7657', 'John Smith', 'MP3', 'Stereo', '22', 'Common', '0', '0', null, '0', '2', '0', null, '0', '5', '5', '0', '0', null, '/stock-audio/yellowhammer-7657.html', '0');
COMMIT;

-- ----------------------------
--  Table structure for `audio_fields`
-- ----------------------------
DROP TABLE IF EXISTS `audio_fields`;
CREATE TABLE `audio_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `always` int(11) DEFAULT NULL,
  `fname` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `audio_fields`
-- ----------------------------
BEGIN;
INSERT INTO `audio_fields` VALUES ('1', 'category', '1', '1', '1', '1', 'folder'), ('2', 'title', '2', '1', '1', '1', 'title'), ('3', 'description', '3', '1', '1', '0', 'description'), ('4', 'keywords', '4', '1', '1', '0', 'keywords'), ('5', 'file for sale', '5', '1', '1', '1', 'sale'), ('6', 'preview audio', '6', '1', '1', '1', 'previewaudio'), ('7', 'preview picture', '7', '1', '1', '1', 'previewpicture'), ('9', 'duration', '9', '1', '1', '0', 'duration'), ('10', 'track source', '10', '1', '1', '0', 'source'), ('11', 'track format', '11', '1', '1', '0', 'format'), ('12', 'copyright holder', '12', '1', '1', '0', 'holder'), ('13', 'terms and conditions', '13', '1', '1', '0', 'terms');
COMMIT;

-- ----------------------------
--  Table structure for `audio_format`
-- ----------------------------
DROP TABLE IF EXISTS `audio_format`;
CREATE TABLE `audio_format` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `audio_format`
-- ----------------------------
BEGIN;
INSERT INTO `audio_format` VALUES ('1', 'Mono'), ('2', 'Stereo'), ('3', '5.1'), ('4', '7.1'), ('5', 'Other');
COMMIT;

-- ----------------------------
--  Table structure for `audio_source`
-- ----------------------------
DROP TABLE IF EXISTS `audio_source`;
CREATE TABLE `audio_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `audio_source`
-- ----------------------------
BEGIN;
INSERT INTO `audio_source` VALUES ('1', 'Wav'), ('2', 'MP3'), ('4', 'Other');
COMMIT;

-- ----------------------------
--  Table structure for `audio_types`
-- ----------------------------
DROP TABLE IF EXISTS `audio_types`;
CREATE TABLE `audio_types` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `types` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `shipped` int(11) DEFAULT NULL,
  `license` int(10) NOT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `priority` (`priority`),
  KEY `license` (`license`)
) ENGINE=InnoDB AUTO_INCREMENT=6786 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `audio_types`
-- ----------------------------
BEGIN;
INSERT INTO `audio_types` VALUES ('4801', 'MP3', 'mp3', '1', '1', '0', '4583'), ('4802', 'WAV', 'wav', '2', '2', '0', '4583'), ('5893', 'Shipped CD', 'shipped', '10', '5', '1', '4583'), ('6783', 'MP3', 'mp3', '10', '1', '0', '4584'), ('6784', 'Shipped CD', 'shipped', '100', '3', '1', '4584'), ('6785', 'WAV', 'wav', '20', '2', '0', '4584');
COMMIT;

-- ----------------------------
--  Table structure for `banks`
-- ----------------------------
DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `banks`
-- ----------------------------
BEGIN;
INSERT INTO `banks` VALUES ('1', 'VTB24'), ('2', 'Privatbank');
COMMIT;

-- ----------------------------
--  Table structure for `blog`
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `data` int(11) DEFAULT NULL,
  `user` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `published` int(11) DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `categories` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `comments` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `published` (`published`),
  KEY `data` (`data`),
  KEY `user` (`user`),
  KEY `categories` (`categories`)
) ENGINE=InnoDB AUTO_INCREMENT=4218 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `blog`
-- ----------------------------
BEGIN;
INSERT INTO `blog` VALUES ('4170', 'Hello world!', 0x4c6f72656d20697073756d20646f6c6f722073697420616d65742c20636f6e7365637465747565722061646970697363696e6720656c69742c20736564206469616d206e6f6e756d6d79206e69626820657569736d6f642074696e636964756e74207574206c616f7265657420646f6c6f7265206d61676e6120616c697175616d206572617420766f6c75747061742e205574207769736920656e696d206164206d696e696d2076656e69616d2c2071756973206e6f73747275642065786572636920746174696f6e20756c6c616d636f72706572207375736369706974206c6f626f72746973206e69736c20757420616c697175697020657820656120636f6d6d6f646f20636f6e7365717561742e204475697320617574656d2076656c2065756d2069726975726520646f6c6f7220696e2068656e64726572697420696e2076756c7075746174652076656c69742065737365206d6f6c657374696520636f6e7365717561742c2076656c20696c6c756d20646f6c6f72652065752066657567696174206e756c6c6120666163696c69736973206174207665726f2065726f7320657420616363756d73616e20657420697573746f206f64696f206469676e697373696d2071756920626c616e646974207072616573656e74206c7570746174756d207a7a72696c2064656c656e6974206175677565206475697320646f6c6f72652074652066657567616974206e756c6c6120666163696c6973692e, '1194823132', 'demo', '1', '/content/blog/post_4170.jpg', '', '1'), ('4174', 'Second post', 0x4869206576657279626f64792e3c6272202f3e0d0a3c6272202f3e0d0a4c6f72656d20697073756d20646f6c6f722073697420616d65742c20636f6e7365637465747565722061646970697363696e6720656c69742c20736564206469616d206e6f6e756d6d79206e69626820657569736d6f642074696e636964756e74207574206c616f7265657420646f6c6f7265206d61676e6120616c697175616d206572617420766f6c75747061742e205574207769736920656e696d206164206d696e696d2076656e69616d2c2071756973206e6f73747275642065786572636920746174696f6e20756c6c616d636f72706572207375736369706974206c6f626f72746973206e69736c20757420616c697175697020657820656120636f6d6d6f646f20636f6e7365717561742e204475697320617574656d2076656c2065756d2069726975726520646f6c6f7220696e2068656e64726572697420696e2076756c7075746174652076656c69742065737365206d6f6c657374696520636f6e7365717561742c2076656c20696c6c756d20646f6c6f72652065752066657567696174206e756c6c6120666163696c69736973206174207665726f2065726f7320657420616363756d73616e20657420697573746f206f64696f206469676e697373696d2071756920626c616e646974207072616573656e74206c7570746174756d207a7a72696c2064656c656e6974206175677565206475697320646f6c6f72652074652066657567616974206e756c6c6120666163696c6973692e, '1194823891', 'demo', '1', '/content/blog/post_4174.jpg', 'Photo,Video', '1'), ('4209', 'My first post', 0x4c6f72656d20697073756d20646f6c6f722073697420616d65742c20636f6e7365637465747565722061646970697363696e6720656c69742c20736564206469616d206e6f6e756d6d79206e69626820657569736d6f642074696e636964756e74207574206c616f7265657420646f6c6f7265206d61676e6120616c697175616d206572617420766f6c75747061742e205574207769736920656e696d206164206d696e696d2076656e69616d2c2071756973206e6f73747275642065786572636920746174696f6e20756c6c616d636f72706572207375736369706974206c6f626f72746973206e69736c20757420616c697175697020657820656120636f6d6d6f646f20636f6e7365717561742e204475697320617574656d2076656c2065756d2069726975726520646f6c6f7220696e2068656e64726572697420696e2076756c7075746174652076656c69742065737365206d6f6c657374696520636f6e7365717561742c2076656c20696c6c756d20646f6c6f72652065752066657567696174206e756c6c6120666163696c69736973206174207665726f2065726f7320657420616363756d73616e20657420697573746f206f64696f206469676e697373696d2071756920626c616e646974207072616573656e74206c7570746174756d207a7a72696c2064656c656e6974206175677565206475697320646f6c6f72652074652066657567616974206e756c6c6120666163696c6973692e, '1195405355', 'siteowner', '1', '/upload/images/post_4209.jpg', 'Funny', '1'), ('4217', 'Second post', 0x5574207769736920656e696d206164206d696e696d2076656e69616d2c2071756973206e6f73747275642065786572636920746174696f6e20756c6c616d636f72706572207375736369706974206c6f626f72746973206e69736c20757420616c697175697020657820656120636f6d6d6f646f20636f6e7365717561742e204475697320617574656d2076656c2065756d2069726975726520646f6c6f7220696e2068656e64726572697420696e2076756c7075746174652076656c69742065737365206d6f6c657374696520636f6e7365717561742c2076656c20696c6c756d20646f6c6f72652065752066657567696174206e756c6c6120666163696c69736973206174207665726f2065726f7320657420616363756d73616e20657420697573746f206f64696f206469676e697373696d2071756920626c616e646974207072616573656e74206c7570746174756d207a7a72696c2064656c656e6974206175677565206475697320646f6c6f72652074652066657567616974206e756c6c6120666163696c6973692e, '1195419308', 'siteowner', '1', '/upload/images/post_4217.jpg', 'Jokes', '1');
COMMIT;

-- ----------------------------
--  Table structure for `blog_categories`
-- ----------------------------
DROP TABLE IF EXISTS `blog_categories`;
CREATE TABLE `blog_categories` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `user` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=4217 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `blog_categories`
-- ----------------------------
BEGIN;
INSERT INTO `blog_categories` VALUES ('4160', 'Photo', 'demo'), ('4163', 'Video', 'demo'), ('4215', 'Funny', 'siteowner'), ('4216', 'Jokes', 'siteowner');
COMMIT;

-- ----------------------------
--  Table structure for `blog_comments`
-- ----------------------------
DROP TABLE IF EXISTS `blog_comments`;
CREATE TABLE `blog_comments` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `data` int(11) DEFAULT NULL,
  `postid` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `data` (`data`),
  KEY `user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=4216 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `blog_comments`
-- ----------------------------
BEGIN;
INSERT INTO `blog_comments` VALUES ('4206', 'siteowner', 0x5665727920696e746572657374696e6721, '1195402323', '4174'), ('4208', 'siteowner', 0x5468616e6b20796f7520666f7220796f757220706f7374, '1195403608', '4174'), ('4210', 'siteowner', 0x4d7920666972737420636f6d6d656e74212121, '1195405410', '4209'), ('4213', 'demo', 0x4d79207365636f6e6420636f6d6d656e7421, '1195408050', '4209'), ('4215', 'buyer', 0x46616e746173746963, '1337271333', '4174');
COMMIT;

-- ----------------------------
--  Table structure for `caching`
-- ----------------------------
DROP TABLE IF EXISTS `caching`;
CREATE TABLE `caching` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `time_refresh` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `caching`
-- ----------------------------
BEGIN;
INSERT INTO `caching` VALUES ('1', 'Header', '-1'), ('2', 'Footer', '-1'), ('3', 'Home page', '-1'), ('4', 'Publication pages', '-1'), ('5', 'Catalog listing', '-1'), ('6', 'Home page photo sets', '-1'), ('7', 'Stats', '-1');
COMMIT;

-- ----------------------------
--  Table structure for `carts`
-- ----------------------------
DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `checked` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `data` (`data`),
  KEY `user_id` (`user_id`),
  KEY `order_id` (`order_id`),
  KEY `checked` (`checked`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `carts`
-- ----------------------------
BEGIN;
INSERT INTO `carts` VALUES ('87', '1e267ac1233e9c19ec92209e777a65f5', '1376822635', '7440', '152', '127.0.0.1', null), ('88', '1e267ac1233e9c19ec92209e777a65f5', '1376838066', '0', '0', '127.0.0.1', null);
COMMIT;

-- ----------------------------
--  Table structure for `carts_content`
-- ----------------------------
DROP TABLE IF EXISTS `carts_content`;
CREATE TABLE `carts_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `prints_id` int(11) DEFAULT NULL,
  `publication_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `option1_id` int(11) DEFAULT NULL,
  `option1_value` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `option2_id` int(11) DEFAULT NULL,
  `option2_value` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `option3_id` int(11) DEFAULT NULL,
  `option3_value` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`),
  KEY `item_id` (`item_id`),
  KEY `prints_id` (`prints_id`),
  KEY `publication_id` (`publication_id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `carts_content`
-- ----------------------------
BEGIN;
INSERT INTO `carts_content` VALUES ('165', '87', '3832', '0', '7471', '1', '0', '', '0', '', '0', '');
COMMIT;

-- ----------------------------
--  Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id_parent` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `password` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `photo` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `upload` tinyint(1) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `priority` (`priority`),
  KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `category`
-- ----------------------------
BEGIN;
INSERT INTO `category` VALUES ('3913', 'Cities', '2', '', 0x416d617a696e672063697479207069637475726573, '/content/categories/category_3913.jpg', '0', null, '1', '/category/cities.html'), ('3914', 'Movies', '3', '', 0x4578616d706c65206f6620766964656f207075626c69636174696f6e73, '/content/categories/category_3914.jpg', '1', null, '1', '/category/movies.html'), ('3918', 'Animals', '1', '', 0x446966666572656e742070657473, '/content/categories/category_3918.jpg', '1', null, '1', '/category/animals.html'), ('7460', 'Cats', '0', '', 0x546865206265737420616e696d616c73, '/content/categories/category_7460.jpg', '0', '0', '1', '/category/cats.html'), ('7472', 'Architecture', '0', '', 0x427269646765732c2068696768776179732c20747261696e2073746174696f6e73, '/content/categories/category_7472.jpg', '1', '0', '1', '/category/architecture.html'), ('7473', 'Birds', '0', '', 0x4d65777320616e64206f74686572206269726473, '/content/categories/category_7473.jpg', '1', '0', '1', '/category/birds.html'), ('7474', 'Nature', '0', '', 0x42656175746966756c206e61747572652070686f746f73, '/content/categories/category_7474.jpg', '1', '0', '1', '/category/nature.html'), ('7712', 'CD collections', '8', '', '', '/content/categories/category_7712.jpg', '1', '0', '1', '/category/cd-collections.html'), ('4375', 'Sounds', '5', '', 0x4578616d706c6573206f6620617564696f2066696c6573, '/content/categories/category_4375.jpg', '1', null, '1', '/category/sounds.html'), ('4377', 'Illustrations', '6', '', 0x4578616d706c65206f6620766563746f72207075626c69636174696f6e, '/content/categories/category_4377.jpg', '1', null, '1', '/category/illustrations.html'), ('6251', 'Flash', '7', '', 0x466c61736820616e696d617465642065666665637473, '/content/categories/category_6251.jpg', '1', null, '1', '/category/flash.html'), ('7450', 'Firenze', '0', '', 0x546f7363616e612070686f746f73, '/content/categories/category_7450.jpg', '1', '0', '1', '/category/firenze.html'), ('7449', 'Venezia', '0', '', 0x50616c616365732c2063616e616c732c2073717561726573, '/content/categories/category_7449.jpg', '1', '0', '1', '/category/venezia.html');
COMMIT;

-- ----------------------------
--  Table structure for `commission`
-- ----------------------------
DROP TABLE IF EXISTS `commission`;
CREATE TABLE `commission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` float DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `publication` int(11) DEFAULT NULL,
  `types` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `gateway` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `orderid` (`orderid`),
  KEY `item` (`item`),
  KEY `total` (`total`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=319 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `commission`
-- ----------------------------
BEGIN;
INSERT INTO `commission` VALUES ('93', '2.25', '3931', '7177', '1965', '6470', 'photos', '1292963901', null, 'subscription', '1'), ('95', '1.5', '3931', '7177', '1941', '6470', 'photos', '1292965626', null, 'subscription', '1'), ('96', '3', '3931', '7177', '1989', '6470', 'photos', '1292965931', null, 'subscription', '1'), ('97', '0.75', '3931', '7177', '1917', '6470', 'photos', '1292965936', null, 'subscription', '1'), ('98', '3.75', '4131', '7177', '1096', '4815', 'videos', '1292966033', null, 'subscription', '1'), ('101', '3.75', '4131', '7177', '957', '4839', 'vector', '1292966406', null, 'subscription', '1'), ('102', '1.5', '3931', '7177', '1931', '4735', 'photos', '1292993901', null, 'subscription', '1'), ('105', '1.5', '3931', '7294', '1941', '6470', 'photos', '1301593402', null, 'subscription', '1'), ('106', '1.5', '3931', '7294', '1941', '6470', 'photos', '1301593480', null, 'subscription', '1'), ('107', '2.25', '3931', '7294', '1962', '6446', 'photos', '1301593507', null, 'subscription', '1'), ('108', '2.25', '4131', '7294', '1951', '3988', 'photos', '1301593548', null, 'subscription', '1'), ('109', '3', '3931', '7295', '1983', '6422', 'photos', '1301594184', null, 'subscription', '1'), ('110', '3', '4131', '7295', '1970', '3975', 'photos', '1301594222', null, 'subscription', '1'), ('124', '1.5', '3931', '7305', '1935', '6422', 'photos', '1301659511', null, 'subscription', '1'), ('125', '3', '3931', '7305', '1983', '6422', 'photos', '1301659593', null, 'subscription', '1'), ('126', '2.25', '3931', '7305', '1960', '6429', 'photos', '1301659980', null, 'subscription', '1'), ('184', '0.75', '4131', '100', '250', '7466', 'photos', '1321957627', null, null, '1'), ('185', '0.75', '4131', '100', '249', '7465', 'photos', '1321957627', null, null, '1'), ('186', '3.75', '4131', '101', '253', '4839', 'vector', '1321958964', null, 'order', '1'), ('187', '0.75', '4131', '101', '252', '4817', 'audio', '1321958964', null, null, '1'), ('188', '2.25', '4131', '101', '251', '5041', 'videos', '1321958964', null, null, '1'), ('189', '1.5', '4131', '21', '3771', '7464', 'photos', '1321972438', null, 'subscription', '1'), ('190', '0.75', '4131', '21', '3769', '7464', 'photos', '1321972660', null, 'subscription', '1'), ('191', '-1', '4131', '0', '0', '0', 'refund', '1321974044', '', '', '1'), ('192', '0.75', '4131', '102', '254', '7465', 'photos', '1326904633', null, 'order', '1'), ('195', '0.75', '4131', '117', '269', '7471', 'photos', '1344945805', null, 'order', '1'), ('196', '0.75', '4131', '117', '270', '7465', 'photos', '1344945805', null, 'order', '1'), ('197', '0.75', '4131', '119', '272', '7464', 'photos', '1345648660', null, 'order', '1'), ('198', '1.88', '4131', '120', '273', '7471', 'photos', '1348159108', null, 'order', '1'), ('200', '93.75', '4131', '111', '263', '7471', 'photos', '1340960823', null, 'order', '1'), ('203', '0.75', '4131', '122', '276', '7470', 'photos', '1352801009', null, 'order', '1'), ('204', '0.75', '4131', '122', '275', '7471', 'photos', '1352801009', null, 'order', '1'), ('205', '0.75', '4131', '123', '277', '7471', 'photos', '1355489344', null, 'order', '1'), ('211', '0.75', '4131', '128', '283', '7471', 'photos', '1359828517', null, 'order', '1'), ('212', '0.75', '4131', '129', '284', '7471', 'photos', '1360580757', null, 'order', '1'), ('228', '-1', '3931', '0', '0', '0', 'refund', '1362490646', 'dwolla', '', '1'), ('244', '-1', '3931', '0', '0', '0', 'refund', '1362826307', 'webmoney', '', '1'), ('245', '-1', '3931', '0', '0', '0', 'refund', '1362827344', 'paypal', '', '0'), ('246', '-1', '3931', '0', '0', '0', 'refund', '1362827372', 'moneybookers', '', '0'), ('247', '-1', '3931', '0', '0', '0', 'refund', '1362827398', 'qiwi', '', '0'), ('248', '-1', '3931', '0', '0', '0', 'refund', '1362827423', 'dwolla', '', '0'), ('249', '-1', '4131', '0', '0', '0', 'refund', '1362827675', 'paypal', '', '0'), ('252', '3', '4131', '130', '285', '7471', 'photos', '1362832950', null, 'order', '1'), ('299', '2.25', '4131', '134', '299', '7471', 'photos', '1365409882', null, 'order', '1'), ('300', '8.25', '4131', '134', '298', '9111', 'prints_items', '1365409882', null, 'order', '1'), ('301', '1.5', '4131', '134', '297', '8194', 'prints_items', '1365409882', null, 'order', '1'), ('307', '1.5', '4131', '142', '311', '8152', 'prints_items', '1366019007', null, 'order', '1'), ('308', '4.5', '4131', '142', '310', '8171', 'prints_items', '1366019007', null, 'order', '1'), ('309', '0.75', '4131', '146', '315', '7471', 'photos', '1372075943', null, 'order', '1'), ('310', '0.75', '4131', '147', '316', '7465', 'photos', '1376066670', null, 'order', '1'), ('315', '-1', '3931', '0', '0', '0', 'refund', '1376824241', 'bank', '\nPrivatbank: 123456789', '0'), ('318', '-1', '3931', '0', '0', '0', 'refund', '1376826095', 'other', '', '0');
COMMIT;

-- ----------------------------
--  Table structure for `components`
-- ----------------------------
DROP TABLE IF EXISTS `components`;
CREATE TABLE `components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `arows` int(11) DEFAULT NULL,
  `acells` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `slideshow` int(11) DEFAULT NULL,
  `slideshowtime` int(11) DEFAULT NULL,
  `types` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `user` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `components`
-- ----------------------------
BEGIN;
INSERT INTO `components` VALUES ('4', 'Most popular small photos 1x5', 'photo1', '1', '5', '5', '0', '1', 'popular', '0', ''), ('5', 'Featured big photos slideshow 1x1', 'photo2', '1', '1', '8', '1', '8', 'featured', '0', ''), ('6', 'New small photos 20', 'photo2', '2', '10', '20', '0', '1', 'new', '0', ''), ('7', 'Small random photos 4x3 slideshow', 'photo1', '3', '4', '20', '1', '2', 'random', '0', ''), ('9', 'Most popular small photos 1x7', 'photo1', '1', '7', '7', '0', '1', 'popular', '0', ''), ('10', 'New small photos 1x7', 'photo1', '1', '7', '7', '0', '1', 'new', '0', ''), ('11', 'Random small photos 3x3', 'photo1', '3', '3', '9', '0', '1', 'random', '0', ''), ('12', 'Featured photos 7x2', 'photo1', '2', '7', '14', '0', '1', 'featured', '0', ''), ('13', 'Most Downloaded photos 7x2', 'photo1', '2', '7', '14', '0', '1', 'downloaded', '0', ''), ('14', 'Most popular photos 7x2', 'photo1', '2', '7', '14', '0', '1', 'popular', '0', ''), ('15', 'Random photos 7x2', 'photo1', '2', '7', '14', '0', '1', 'random', '0', ''), ('16', 'Newest photos 7x2', 'photo1', '2', '7', '14', '0', '1', 'new', '0', ''), ('17', 'Free photos 7x2', 'photo1', '2', '7', '14', '0', '1', 'free', '0', ''), ('18', 'Most Downloaded photos 10', 'photo2', '1', '10', '10', '0', '1', 'downloaded', '0', ''), ('19', 'Featured photos 10', 'photo2', '1', '10', '10', '0', '1', 'featured', '0', ''), ('20', 'Popular photos 10', 'photo2', '1', '10', '10', '0', '1', 'popular', '0', ''), ('21', 'New photos 10', 'photo2', '1', '10', '10', '0', '1', 'new', '0', ''), ('22', 'Free photos 10', 'photo2', '1', '10', '10', '0', '1', 'free', '0', ''), ('23', 'Random photos 10', 'photo2', '1', '10', '10', '0', '1', 'random', '0', '');
COMMIT;

-- ----------------------------
--  Table structure for `content_filter`
-- ----------------------------
DROP TABLE IF EXISTS `content_filter`;
CREATE TABLE `content_filter` (
  `words` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `content_filter`
-- ----------------------------
BEGIN;
INSERT INTO `content_filter` VALUES (0x6261642c776f726473);
COMMIT;

-- ----------------------------
--  Table structure for `content_type`
-- ----------------------------
DROP TABLE IF EXISTS `content_type`;
CREATE TABLE `content_type` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `priority` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `name` (`name`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=4460 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `content_type`
-- ----------------------------
BEGIN;
INSERT INTO `content_type` VALUES ('4458', '1', 'Common'), ('4459', '2', 'Premium');
COMMIT;

-- ----------------------------
--  Table structure for `coupons`
-- ----------------------------
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `user` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `data2` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `percentage` float DEFAULT NULL,
  `url` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `used` int(11) DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `ulimit` int(11) DEFAULT NULL,
  `tlimit` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `coupon_code` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `coupon_id` (`coupon_id`),
  KEY `user` (`user`),
  KEY `data2` (`data2`),
  KEY `used` (`used`),
  KEY `tlimit` (`tlimit`),
  KEY `ulimit` (`ulimit`),
  KEY `data` (`data`),
  KEY `coupon_code` (`coupon_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7436 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `coupons`
-- ----------------------------
BEGIN;
INSERT INTO `coupons` VALUES ('7418', 'Order discount', 'buyer', '1321955454', '0', '10', '', '1', null, '1324547454', '1', '0', '4000', 'a5d5e7c06099a4fea2b9074673c407f3'), ('7419', 'Order discount', 'buyer', '1340811039', '0', '10', '', '1', null, '1343403039', '1', '0', '4000', '16ad53afffe8ce425fc977760a67f49e'), ('7420', 'Signup Bonus', 'guest7431', '1348338140', '0', '0', '', '1', null, '1350930140', '1', '0', '4001', '03f4e6eeb82f24c5f8791331f94de72f'), ('7413', 'Promotion code', '', '1340344975', '0', '10', '', '1', null, '1340344975', '100', '0', '0', '613e8cf3dbab6104a010c670cc021aaf'), ('7421', 'Signup Bonus', 'guest7432', '1348339434', '0', '0', '', '1', null, '1350931434', '1', '0', '4001', 'acd1fe04e0a0d533bfb19c06977385af'), ('7422', 'Signup Bonus', 'guest7433', '1348340266', '0', '0', '', '1', null, '1350932266', '1', '0', '4001', 'fddf8534e44945f9ea7d171183660ce6'), ('7423', 'Order discount', 'buyer', '1351347030', '0', '10', '', '1', null, '1353939030', '1', '0', '4000', 'cea6456e4ebcbf577b38631160f80b23'), ('7425', 'Order discount', 'buyer', '1359537053', '0', '10', '', '1', null, '1362129053', '1', '0', '4000', '8c3a4c69bce3db8e8a95411f014ef08c'), ('7426', 'Order discount', 'demo2', '1359828517', '0', '10', '', '1', null, '1362420517', '1', '0', '4000', 'bcbcbafe8dfd48c3a07c9f6b10ab6003'), ('7427', 'Order discount', 'seller2', '1360580757', '0', '10', '', '1', null, '1363172757', '1', '0', '4000', 'afb8649cea5ec8cfb3e65c9a74f47cbe'), ('7428', 'New coupon', '', '1360760863', '0.6', '0', '', '1', null, '1363352863', '1', '0', '0', '69d6225645ebf97c72ba43d27f26f43e'), ('7429', 'Order discount', 'buyer', '1364900349', '0', '10', '', '1', null, '1367492349', '1', '0', '4000', '2126c8629095d33b27760d61b0d4812a'), ('7430', 'Signup Bonus', 'tester', '1365617146', '0', '0', '', '1', null, '1368209146', '1', '0', '4001', 'dc5135acad41946f8fcc5d3eefd876c2'), ('7431', 'New coupon', 'buyer', '1372066661', '0', '10', '', '1', null, '1374658661', '1', '0', '0', '4663f6b646e6a167250c2a15d7898676'), ('7432', 'Order discount', 'buyer', '1372075944', '0', '10', '', '1', null, '1374667944', '1', '0', '4000', '15e7745fe60566c92282a70916197fd0'), ('7433', 'Order discount', 'common', '1376066671', '0', '10', '', '0', null, '1378658671', '1', '0', '4000', '8def5e98fc3287f2c5df57c0534c1032'), ('7434', 'Signup Bonus', 'guest7440', '1376838020', '0', '0', '', '0', null, '1379430020', '1', '0', '4001', '98c57a1e81a338e728b1a2b1945bd17c'), ('7435', 'Signup Bonus', 'seller3', '1376838828', '0', '0', '', '0', null, '1379430828', '1', '0', '4001', '6990020877dbd915762376fa49f4aa5f');
COMMIT;

-- ----------------------------
--  Table structure for `coupons_types`
-- ----------------------------
DROP TABLE IF EXISTS `coupons_types`;
CREATE TABLE `coupons_types` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `percentage` float DEFAULT NULL,
  `url` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `events` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ulimit` int(11) DEFAULT NULL,
  `bonus` float DEFAULT NULL,
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=4002 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `coupons_types`
-- ----------------------------
BEGIN;
INSERT INTO `coupons_types` VALUES ('4000', 'Order discount', '30', '0', '10', '', 'New Order', '1', '0'), ('4001', 'Signup Bonus', '30', '0', '0', '', 'New Signup', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `credits`
-- ----------------------------
DROP TABLE IF EXISTS `credits`;
CREATE TABLE `credits` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=4419 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `credits`
-- ----------------------------
BEGIN;
INSERT INTO `credits` VALUES ('4415', '1 Credits', '1', '1', '1', '0'), ('4416', '10 Credits', '10', '9', '2', '0'), ('4417', '20 Credits', '20', '17', '3', '0'), ('4418', '50 Credits', '50', '43', '4', '0');
COMMIT;

-- ----------------------------
--  Table structure for `credits_list`
-- ----------------------------
DROP TABLE IF EXISTS `credits_list`;
CREATE TABLE `credits_list` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `user` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL,
  `payment` int(11) DEFAULT NULL,
  `credits` int(11) DEFAULT NULL,
  `expiration_date` int(11) DEFAULT NULL,
  `subtotal` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `taxes` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `billing_firstname` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_lastname` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_address` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_city` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_zip` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_country` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `data` (`data`),
  KEY `user` (`user`),
  KEY `payment` (`payment`),
  KEY `approved` (`approved`),
  KEY `expiration_date` (`expiration_date`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `credits_list`
-- ----------------------------
BEGIN;
INSERT INTO `credits_list` VALUES ('202', 'Credits bonus', '1000', 'buyer', '1365933219', '1', '0', '0', '0', null, null, null, null, null, null, null, null, null, null), ('27', '1 Credits', '1', 'buyer', '1312624220', '1', '0', '4415', '0', '0', '0', '0', '0', null, null, null, null, null, null), ('28', '1 Credits', '1', 'buyer', '1312624786', '1', '0', '4415', '0', '0', '0', '0', '0', null, null, null, null, null, null), ('47', 'Order _resh_88', '-1', 'common', '1318183427', '1', '0', null, '0', '0', '0', '0', '0', null, null, null, null, null, null), ('46', '50 Credits', '50', 'common', '1318183400', '1', '0', '4418', '0', '0', '0', '0', '0', null, null, null, null, null, null), ('48', 'Credits bonus', '20', 'buyer', '1318594133', '1', '0', '0', '1321186133', '0', '0', '0', '0', null, null, null, null, null, null), ('83', 'Signup Bonus', '1', 'qwert', '1328345657', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('80', 'Signup Bonus', '1', 'ewrwer', '1326281944', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('81', 'Signup Bonus', '1', 'ewrwer', '1326282744', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('82', 'Order _resh_102', '-1', 'buyer', '1326904633', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('79', 'Signup Bonus', '1', 'ewrwer', '1326281871', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('78', 'Signup Bonus', '1', 'tester', '1322126542', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('77', 'Signup Bonus', '1', 'tester', '1322125812', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('76', 'Order _resh_101', '-9', 'buyer', '1321958964', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('75', 'Order _resh_100', '-2', 'buyer', '1321957627', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('73', 'Order _resh_98', '-2', 'buyer', '1321955454', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('74', 'Order _resh_99', '-2', 'buyer', '1321956317', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('132', '10 Credits', '10', 'buyer', '1347621192', '1', '0', '4416', '0', '9', '0', '0', '9', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('133', '10 Credits', '10', 'buyer', '1347621272', '0', '0', '4416', '0', '9', '0', '0', '9', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('131', '10 Credits', '10', 'buyer', '1347620505', '1', '0', '4416', '0', '9', '0', '0', '9', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('125', 'Order _resh_111', '-125', 'buyer', '1340960823', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('126', 'Signup Bonus', '1', 'tester', '1341215968', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('128', 'Order _resh_117', '-2', 'buyer', '1344945806', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('129', '1 Credits', '1', 'buyer', '1347618342', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('130', '1 Credits', '1', 'buyer', '1347618410', '0', '0', '4415', '0', '1', '0', '0', '1', '', '', '', '', '', ''), ('99', '25 Credits', '25', 'buyer', '1337343807', '1', '7388', '0', '0', '50', '0', '0', '50', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('134', 'Order _resh_120', '-2.5', 'buyer', '1348159109', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('135', 'Signup Bonus', '1', 'demo3', '1348481040', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('136', '1 Credits', '1', 'buyer', '1348913224', '1', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('137', '1 Credits', '1', 'buyer', '1348913273', '1', '0', '4415', '0', '1', '0', '0', '1', '', '', '', '', '', ''), ('138', 'Order _resh_122', '-2', 'buyer', '1352801010', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('139', '1 Credits', '1', 'buyer', '1354298698', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('140', '1 Credits', '1', 'buyer', '1354380364', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('141', '1 Credits', '1', 'buyer', '1354383103', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('142', '1 Credits', '1', 'buyer', '1354383148', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('143', '1 Credits', '1', 'buyer', '1354383194', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('144', '1 Credits', '1', 'buyer', '1355486497', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('145', '1 Credits', '1', 'buyer', '1355486672', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('146', '1 Credits', '1', 'buyer', '1355486924', '0', '0', '4415', '0', '1', '0', '0', '1', '', '', '', '', '', ''), ('147', '1 Credits', '1', 'buyer', '1355486968', '0', '0', '4415', '0', '1', '0', '0', '1', '', '', '', '', '', ''), ('148', '1 Credits', '1', 'buyer', '1355487086', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('149', '1 Credits', '1', 'buyer', '1355487154', '0', '0', '4415', '0', '1', '0', '0', '1', '', '', '', '', '', ''), ('150', '1 Credits', '1', 'buyer', '1355487211', '1', '2147483647', '4415', '0', '1', '0', '0', '1', '', '', '', '', '', ''), ('151', '1 Credits', '1', 'buyer', '1355489827', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('152', '1 Credits', '1', 'buyer', '1355742632', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('153', '1 Credits', '1', 'buyer', '1355742665', '0', '0', '4415', '0', '1', '0', '0', '1', '', '', '', '', '', ''), ('154', '1 Credits', '1', 'buyer', '1355743979', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('155', '1 Credits', '1', 'buyer', '1356120744', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('156', '1 Credits', '1', 'buyer', '1356121006', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('158', 'Order _resh_124', '-3', 'buyer', '1359537053', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('159', 'Order _resh_125', '-4', 'buyer', '1359547266', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('160', 'Order _resh_126', '-7', 'buyer', '1359558982', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('161', 'Order _resh_127', '-2', 'buyer', '1359635544', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('165', '1 Credits', '1', 'demo2', '1359829315', '1', '0', '4415', '0', '1', '0', '0', '1', 'Jeff', 'Ri_char_ds', '1st Rd.', 'New York', '12345', ''), ('163', 'Credits bonus', '5', 'demo2', '1359828140', '1', '0', '0', '1359828519', null, null, null, null, null, null, null, null, null, null), ('164', 'Order _resh_128', '-1', 'demo2', '1359828517', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('166', 'Signup Bonus', '1', 'seller2', '1360235342', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('167', 'Order _resh_129', '-1', 'seller2', '1360580757', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('185', '1 Credits', '1', 'buyer', '1362832875', '1', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('186', 'Order _resh_130', '-4', 'buyer', '1362832950', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('187', '1 Credits', '1', 'buyer', '1363084579', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('182', '1 Credits', '1', 'buyer', '1362503485', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('183', '1 Credits', '1', 'buyer', '1362504310', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('184', '1 Credits', '1', 'common', '1362825263', '0', '0', '4415', '0', '1', '0', '0', '1', 'Bob', 'Smith', '', 'Wien', '', 'Austria'), ('188', '1 Credits', '1', 'buyer', '1363084874', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('189', '1 Credits', '1', 'buyer', '1363085086', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('190', '1 Credits', '1', 'buyer', '1363085835', '0', '0', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('192', 'Order _resh_132', '-51', 'buyer', '1364900732', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('193', 'Order _resh_133', '-1', 'buyer', '1365409686', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('194', 'Order _resh_134', '-18', 'buyer', '1365409882', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('199', '1 Credits', '1', 'buyer', '1365838266', '1', '7398', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('198', '1 Credits', '1', 'buyer', '1365838159', '1', '7397', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('197', '1 Credits', '1', 'buyer', '1365836623', '1', '7396', '4415', '0', '1', '0', '0', '1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('203', 'Order _resh_138', '-1', 'buyer', '1365933559', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('204', 'Order _resh_139', '-6', 'buyer', '1365934052', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('205', 'Order _resh_140', '-1', 'buyer', '1365934185', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('206', 'Order _resh_142', '-9', 'buyer', '1366019008', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('210', '1 Credits', '1', 'buyer', '1368729317', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('209', '1 Credits', '1', 'buyer', '1367662492', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('211', '1 Credits', '1', 'buyer', '1369382571', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('212', '1 Credits', '1', 'buyer', '1369386453', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('213', '1 Credits', '1', 'buyer', '1370621690', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('214', '1 Credits', '1', 'buyer', '1370621953', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', '', '', '', '', '', ''), ('215', '1 Credits', '1', 'buyer', '1370621992', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', '', '', '', '', '', ''), ('216', '1 Credits', '1', 'buyer', '1370622150', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', '', '', '', '', '', ''), ('217', '1 Credits', '1', 'buyer', '1370622326', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('218', '1 Credits', '1', 'buyer', '1370623018', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('219', '1 Credits', '1', 'buyer', '1370860325', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('220', '1 Credits', '1', 'buyer', '1370860440', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', '', '', '', '', '', ''), ('221', '1 Credits', '1', 'buyer', '1370861083', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('222', '1 Credits', '1', 'common', '1371797590', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'Bob', 'Smith', '', 'Wien', '', 'Austria'), ('223', '1 Credits', '1', 'common', '1371797649', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', '', '', '', '', '', ''), ('224', '1 Credits', '1', 'common', '1371797818', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'Bob', 'Smith', '', 'Wien', '', 'Austria'), ('225', '1 Credits', '1', 'common', '1371797858', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'Bob', 'Smith', '', 'Wien', '', 'Austria'), ('226', '1 Credits', '1', 'common', '1371920235', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'Bob', 'Smith', '', 'Wien', '', 'Austria'), ('227', 'Order _resh_146', '-1', 'buyer', '1372075944', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('228', 'Order _resh_147', '-1', 'common', '1376066670', '1', '0', null, '0', null, null, null, null, null, null, null, null, null, null), ('229', '1 Credits', '1', 'buyer', '1376841347', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('230', '1 Credits', '1', 'buyer', '1376841633', '0', '0', '4415', '0', '1', '0', '0.1', '1.1', 'John', 'Brown', '', 'Los Angeles', '', 'United States');
COMMIT;

-- ----------------------------
--  Table structure for `currency`
-- ----------------------------
DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `code1` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `code2` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `currency`
-- ----------------------------
BEGIN;
INSERT INTO `currency` VALUES ('1', 'Australian Dollars', 'AUD', 'A $', '0'), ('2', 'Canadian Dollars', 'CAD', 'C $', '0'), ('3', 'Euros', 'EUR', '&euro;', '0'), ('4', 'Pounds Sterling', 'GBP', '&pound;', '0'), ('5', 'Yen', 'JPY', '&yen;', '0'), ('6', 'U.S. Dollars', 'USD', '$', '1'), ('7', 'New Zealand Dollar', 'NZD', '$', '0'), ('8', 'Swiss Franc', 'CHF', null, '0'), ('9', 'Hong Kong Dollar', 'HKD', '$', '0'), ('10', 'Singapore Dollar', 'SGD', '$', '0'), ('11', 'Swedish Krona', 'SEK', null, '0'), ('12', 'Danish Krone', 'DKK', null, '0'), ('13', 'Polish Zloty', 'PLN', null, '0'), ('14', 'Norwegian Krone', 'NOK', null, '0'), ('15', 'Hungarian Forint', 'HUF', null, '0'), ('16', 'Czech Koruna', 'CZK', null, '0'), ('17', 'UAE Dirham', 'AED', null, '0'), ('18', 'Jordanian dinar', 'JOD', null, '0'), ('19', 'Egyptian Pound', 'EGP', null, '0'), ('20', 'Saudi Riyal', 'SAR', null, '0'), ('21', 'Russian Ruble', 'RUB', null, '0'), ('22', 'Ukraine Hryvnia', 'UAH', null, '0'), ('23', 'Belarus Ruble', 'BYR', null, '0'), ('24', 'Uzbekistan Sum', 'UZS', null, '0'), ('25', 'Thai Baht', 'THB', null, '0'), ('26', 'Israeli Shekel', 'ILS', null, '0'), ('27', 'Mexican Peso', 'MXN', null, '0'), ('28', 'Lithuanian Litas', 'LT', null, '0'), ('29', 'Indian rupee', 'INR', null, '0'), ('30', 'Bulgarian Lev', 'BGN', null, '0'), ('32', 'Rial', 'IRR', null, '0'), ('33', 'Leu', 'RON', null, '0'), ('34', 'Manat', 'AZN', null, '0'), ('35', 'Brazilian Real', 'BRL', null, '0'), ('36', 'Korean Won', 'KRW', '&_resh_836', '0'), ('38', 'Kazakhstan Tenge', 'KZT', '', '0');
COMMIT;

-- ----------------------------
--  Table structure for `downloads`
-- ----------------------------
DROP TABLE IF EXISTS `downloads`;
CREATE TABLE `downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `tlimit` int(11) DEFAULT NULL,
  `ulimit` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `publication_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`),
  KEY `link` (`link`),
  KEY `data` (`data`),
  KEY `tlimit` (`tlimit`),
  KEY `ulimit` (`ulimit`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `subscription_id` (`subscription_id`),
  KEY `publication_id` (`publication_id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `downloads`
-- ----------------------------
BEGIN;
INSERT INTO `downloads` VALUES ('96', '3778', 'c573e8fc3049eedda366de176124494a', '1323253627', '2', '5', '100', '6355', '0', '7465'), ('97', '3787', 'd8c7ea71d07437f30a309ba8c53b2b3f', '1323253627', '2', '5', '100', '6355', '0', '7466'), ('98', '1094', '5eaec0488594b3427d894af96ec74f64', '1323254964', '0', '5', '101', '6355', '0', '5041'), ('99', '1098', '1d2facc7eb8f3823fd5862468cfe7ad2', '1323254964', '1', '5', '101', '6355', '0', '4817'), ('100', '957', '0e9a0598257ceaf36138667e6a845626', '1323254964', '0', '5', '101', '6355', '0', '4839'), ('101', '3771', '9305cb8a4e178fa184cc3b64d4546a01', '1323268438', '6', '5', '0', '6355', '21', '7464'), ('102', '3769', '5bb681eabac251924c4400345744c878', '1323268660', '1', '5', '0', '6355', '21', '7464'), ('103', '3778', '5cd80c0c3373359dc0ecda1615974d5b', '1328200633', '0', '5', '102', '6355', '0', '7465'), ('105', '3833', '7d7ab87599e68460f209a30ccd02d03a', '1342256823', '0', '5', '111', '6355', '0', '7471'), ('106', '3832', '508eae473c149a25234ed7363d0c49f0', '1346241805', '0', '5', '117', '6355', '0', '7471'), ('107', '3778', '461dcdabea8e11cc6c8f99f8d2bf2b4f', '1346241806', '0', '5', '117', '6355', '0', '7465'), ('108', '3769', '85e5ad3a903d6f3055902f38858ca864', '1346944847', '0', '5', '119', '6355', '0', '7464'), ('109', '3834', 'e9cecbfff1982543ef79a9e6b5fecc41', '1350977415', '0', '5', '120', '6355', '0', '7471'), ('110', '3796', '7015524c2298f28b45124f15d265d402', '1352360086', '0', '5', '105', '6355', '0', '7467'), ('111', '3746', '848c68e80081744ee9847d9bc3b4f464', '1352729720', '0', '5', '118', '6355', '0', '7461'), ('112', '3832', '0da81f2d2fc8ac091e7b03b47d65294a', '1354097010', '2', '5', '122', '6355', '0', '7471'), ('113', '3823', 'e4760f8b85cfde5b27a36f1e9de5dcc5', '1354097010', '0', '5', '122', '6355', '0', '7470'), ('114', '3832', 'de3360af4975a57718ce315b227385d7', '1356785371', '0', '5', '123', '6355', '0', '7471'), ('120', '3832', '6250a4c207e5971b899d54c57a2f5a00', '1361124517', '0', '5', '128', '4113', '0', '7471'), ('121', '3832', '809177f7f478f972b84cbaf2321b270c', '1361876757', '0', '5', '129', '7435', '0', '7471'), ('122', '3838', '92acd9dd95c3f575f97a428405157f3f', '1364128950', '0', '5', '130', '6355', '0', '7471'), ('126', '3836', '9e414cb1fd4486ab62b6dfd4496ef0bb', '1366705882', '0', '5', '134', '6355', '0', '7471'), ('131', '3832', 'fed57945ea7f39d18242cd2ee34e661a', '1373371943', '0', '5', '146', '6355', '0', '7471'), ('132', '3778', '64cea583b6fdde987ee7cc53096da3de', '1377362670', '0', '5', '147', '7403', '0', '7465');
COMMIT;

-- ----------------------------
--  Table structure for `examinations`
-- ----------------------------
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `comments` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`data`),
  KEY `user` (`user`),
  KEY `data` (`data`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `examinations`
-- ----------------------------
BEGIN;
INSERT INTO `examinations` VALUES ('2', '3931', '1241736827', '1', 0x596f75206172652077656c636f6d6521), ('7', '6360', '1297165561', '1', ''), ('8', '7403', '1345188276', '1', ''), ('9', '7435', '1360519118', '1', '');
COMMIT;

-- ----------------------------
--  Table structure for `ffmpeg`
-- ----------------------------
DROP TABLE IF EXISTS `ffmpeg`;
CREATE TABLE `ffmpeg` (
  `fpath` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `video_width` int(11) DEFAULT NULL,
  `video_height` int(11) DEFAULT NULL,
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_height` int(11) DEFAULT NULL,
  `ffmpeg` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `video_format` varchar(10) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `ffmpeg`
-- ----------------------------
BEGIN;
INSERT INTO `ffmpeg` VALUES ('/usr/local/bin/ffmpeg', '400', '280', '120', '80', '0', '5', '10', 'mp4');
COMMIT;

-- ----------------------------
--  Table structure for `filestorage`
-- ----------------------------
DROP TABLE IF EXISTS `filestorage`;
CREATE TABLE `filestorage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `types` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activ` (`activ`),
  KEY `types` (`types`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `filestorage`
-- ----------------------------
BEGIN;
INSERT INTO `filestorage` VALUES ('1', '/content', '0', '0', 'Local server'), ('2', '/content2', '1', '0', 'Local server'), ('4', null, '0', '1', 'Rackspace cloud'), ('5', null, '0', '2', 'Amazon S3');
COMMIT;

-- ----------------------------
--  Table structure for `filestorage_amazon`
-- ----------------------------
DROP TABLE IF EXISTS `filestorage_amazon`;
CREATE TABLE `filestorage_amazon` (
  `prefix` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `api_key` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `region` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `filestorage_amazon`
-- ----------------------------
BEGIN;
INSERT INTO `filestorage_amazon` VALUES ('cmsaccount', '0', 'cmsaccount', 'test', 'REGION_US_E1');
COMMIT;

-- ----------------------------
--  Table structure for `filestorage_files`
-- ----------------------------
DROP TABLE IF EXISTS `filestorage_files`;
CREATE TABLE `filestorage_files` (
  `id_parent` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `filename1` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `filename2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `server1` int(11) DEFAULT NULL,
  `pdelete` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Table structure for `filestorage_logs`
-- ----------------------------
DROP TABLE IF EXISTS `filestorage_logs`;
CREATE TABLE `filestorage_logs` (
  `publication_id` int(11) DEFAULT NULL,
  `logs` text COLLATE utf8_bin,
  `data` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Table structure for `filestorage_rackspace`
-- ----------------------------
DROP TABLE IF EXISTS `filestorage_rackspace`;
CREATE TABLE `filestorage_rackspace` (
  `prefix` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `api_key` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `cron` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `filestorage_rackspace`
-- ----------------------------
BEGIN;
INSERT INTO `filestorage_rackspace` VALUES ('cmsaccount1', '0', 'cmsaccount', 'test', '1');
COMMIT;

-- ----------------------------
--  Table structure for `friends`
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id_parent` int(11) DEFAULT NULL,
  `friend1` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `friend2` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `friend1` (`friend1`),
  KEY `friend2` (`friend2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `friends`
-- ----------------------------
BEGIN;
INSERT INTO `friends` VALUES (null, 'demo', 'john'), (null, 'demo', 'siteowner'), (null, 'siteowner', 'demo'), (null, 'john', 'demo'), (null, 'buyer', 'siteowner'), (null, 'buyer', 'john');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_2checkout`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_2checkout`;
CREATE TABLE `gateway_2checkout` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `word` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_2checkout`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_2checkout` VALUES ('1111112', 'good', 'https://www2.2checkout.com/2co/buyer/purchase', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_alertpay`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_alertpay`;
CREATE TABLE `gateway_alertpay` (
  `account` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `security_code` varchar(250) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_alertpay`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_alertpay` VALUES ('sales@cmsaccount.com', '1', 'https://www.alertpay.com/checkout', '0', 'YnEmI8amtCXPw8Tq');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_authorize`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_authorize`;
CREATE TABLE `gateway_authorize` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `txnkey` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_authorize`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_authorize` VALUES ('cmsaccount', 'HjIiT4wOFskhYnwh', 'https://secure.authorize.net/gateway/transact.dll', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_cashu`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_cashu`;
CREATE TABLE `gateway_cashu` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `ecode` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_cashu`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_cashu` VALUES ('test@cashucard.com', '234b23b42kj423', 'https://www.cashu.com/cgi-bin/pcashu.cgi', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_cashx`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_cashx`;
CREATE TABLE `gateway_cashx` (
  `account` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `security_code` varchar(250) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_cashx`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_cashx` VALUES ('sales@cmsaccount.com', '1', 'https://www.cashx.com/checkout', '0', '1I1rdlDNL9dHkUPWHhW8y4tq');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_ccbill`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_ccbill`;
CREATE TABLE `gateway_ccbill` (
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `product_id` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `subscription` int(11) DEFAULT NULL,
  `credits` int(11) DEFAULT NULL,
  `account` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `account2` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `account3` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_ccbill`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_ccbill` VALUES ('https://bill.ccbill.com/jpost/signup.cgi', '0', '1', null, '0', '0', 'account', 'subaccount', 'formID'), ('', '0', '0', '0000008001', '4461', '0', '', null, null), ('', '0', '0', '0000008002', '4462', '0', '', null, null), ('', '0', '0', '0000008003', '4464', '0', '', null, null), ('', '0', '0', '0000008004', '4465', '0', '', null, null), ('', '0', '0', '0000008005', '0', '4415', '', null, null), ('', '0', '0', '0000008006', '0', '4416', '', null, null), ('', '0', '0', '0000008007', '0', '4417', '', null, null), ('', '0', '0', '0000008008', '0', '4418', '', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `gateway_chronopay`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_chronopay`;
CREATE TABLE `gateway_chronopay` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `ekey` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_chronopay`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_chronopay` VALUES ('cmsaccount', 'world', 'https://payments.chronopay.com/', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_clickbank`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_clickbank`;
CREATE TABLE `gateway_clickbank` (
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `product_id` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `subscription` int(11) DEFAULT NULL,
  `credits` int(11) DEFAULT NULL,
  `account` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `account2` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_clickbank`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_clickbank` VALUES ('http://www.clickbank.net/sell.cgi', '0', '1', null, '0', '0', 'videosourc', 'TESTTESTTESTTEST'), ('', '0', '0', '1', '4461', '0', '', null), ('', '0', '0', '1', '4462', '0', '', null), ('', '0', '0', '1', '4464', '0', '', null), ('', '0', '0', '1', '4465', '0', '', null), ('', '0', '0', '1', '0', '4415', '', null), ('', '0', '0', '1', '0', '4416', '', null), ('', '0', '0', '1', '0', '4417', '', null), ('', '0', '0', '1', '0', '4418', '', null);
COMMIT;

-- ----------------------------
--  Table structure for `gateway_dotpay`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_dotpay`;
CREATE TABLE `gateway_dotpay` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_dotpay`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_dotpay` VALUES ('test', 'test', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_dwolla`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_dwolla`;
CREATE TABLE `gateway_dwolla` (
  `account` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `pin` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `apikey` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `apisecret` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `test` tinyint(4) DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_dwolla`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_dwolla` VALUES ('210-268-9238', '1111', 'test', 'test', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_egold`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_egold`;
CREATE TABLE `gateway_egold` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_egold`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_egold` VALUES ('123', 'cmsaccount', '', 'https://www.e-gold.com/sci_asp/payments.asp', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_enets`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_enets`;
CREATE TABLE `gateway_enets` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_enets`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_enets` VALUES ('cmsaccount', 'https://www.enetspayments.com.sg/masterMerchant/collectionPage.jsp', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_epassporte`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_epassporte`;
CREATE TABLE `gateway_epassporte` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `pcode` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_epassporte`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_epassporte` VALUES ('rt_5222', '1234', 'https://www.epassporte.com/secure/eppurchase.cgi', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_epay`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_epay`;
CREATE TABLE `gateway_epay` (
  `account` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `security_code` varchar(250) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_epay`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_epay` VALUES ('D229773874', '1', 'https://www.epay.bg', '0', 'RJAVAMNGCDWG5PQAK5MNLFJD15MJCJCTH1WGIMUTWT46N2WR7BM0B6U6WK58VF5P');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_epaykkbkz`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_epaykkbkz`;
CREATE TABLE `gateway_epaykkbkz` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_epaykkbkz`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_epaykkbkz` VALUES ('1', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_epoch`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_epoch`;
CREATE TABLE `gateway_epoch` (
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `product_id` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `subscription` int(11) DEFAULT NULL,
  `credits` int(11) DEFAULT NULL,
  `account` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_epoch`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_epoch` VALUES ('https://wnu.com/secure/fpost.cgi', '0', '1', '0', '0', '0', '123451'), ('', '0', '0', '1', '4461', '0', ''), ('', '0', '0', '0', '4462', '0', ''), ('', '0', '0', '0', '4464', '0', ''), ('', '0', '0', '0', '4465', '0', ''), ('', '0', '0', '0', '0', '4415', ''), ('', '0', '0', '0', '0', '4416', ''), ('', '0', '0', '0', '0', '4417', ''), ('', '0', '0', '0', '0', '4418', '');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_eway`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_eway`;
CREATE TABLE `gateway_eway` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_eway`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_eway` VALUES ('cmsaccount', 'https://www.eway.com.au/gateway/payment.asp', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_fortumo`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_fortumo`;
CREATE TABLE `gateway_fortumo` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_fortumo`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_fortumo` VALUES ('6c501a2a9a6c3f64ebe9431800ed95a7', '618906475986414b69aea96768edb318', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_google`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_google`;
CREATE TABLE `gateway_google` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `mkey` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_google`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_google` VALUES ('539866586074106', 'https://sandbox.google.com/checkout/api/checkout/v2/checkoutForm/Merchant/{merchant_id}', '0', '1', 'MIZmNZU08S0_PZYsh7x6ZQ');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_linkpoint`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_linkpoint`;
CREATE TABLE `gateway_linkpoint` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_linkpoint`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_linkpoint` VALUES ('123455', 'https://www.linkpointcentral.com/lpc/servlet/lppay', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_moneybookers`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_moneybookers`;
CREATE TABLE `gateway_moneybookers` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_moneybookers`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_moneybookers` VALUES ('sales@cmsaccount.com', 'https://www.moneybookers.com/app/payment.pl', '0', '1', 'test');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_moneyua`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_moneyua`;
CREATE TABLE `gateway_moneyua` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL,
  `testmode` tinyint(4) DEFAULT NULL,
  `commission` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_moneyua`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_moneyua` VALUES ('test', 'test', '0', '1', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_multicards`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_multicards`;
CREATE TABLE `gateway_multicards` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `account2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_multicards`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_multicards` VALUES ('123456', 'https://secure.multicards.com/cgi-bin/order2/processorder1.pl', '0', '1', '1', 'test');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_myvirtualmerchant`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_myvirtualmerchant`;
CREATE TABLE `gateway_myvirtualmerchant` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `account2` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_myvirtualmerchant`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_myvirtualmerchant` VALUES ('test1', 'test2', 'test3', '', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_nochex`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_nochex`;
CREATE TABLE `gateway_nochex` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_nochex`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_nochex` VALUES ('cmsaccount', 'https://secure.nochex.com/', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_pagseguro`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_pagseguro`;
CREATE TABLE `gateway_pagseguro` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_pagseguro`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_pagseguro` VALUES ('ricardo.limarh@gmail.com', 'test2', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_paxum`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_paxum`;
CREATE TABLE `gateway_paxum` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_paxum`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_paxum` VALUES ('test@mail.com', 'test2', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_paypal`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_paypal`;
CREATE TABLE `gateway_paypal` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_paypal`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_paypal` VALUES ('sales@cmsaccount.com', 'https://www.paypal.com/cgi-bin/webscr', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_paypalpro`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_paypalpro`;
CREATE TABLE `gateway_paypalpro` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `signature` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_paypalpro`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_paypalpro` VALUES ('sales@cmsaccount.com', '123456', 'test', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_payprin`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_payprin`;
CREATE TABLE `gateway_payprin` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_payprin`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_payprin` VALUES ('cmsaccount', '1234', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_paysera`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_paysera`;
CREATE TABLE `gateway_paysera` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_paysera`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_paysera` VALUES ('test', 'test', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_payu`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_payu`;
CREATE TABLE `gateway_payu` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password2` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password3` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_payu`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_payu` VALUES ('test', 'test', 'test', 'test', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_privatbank`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_privatbank`;
CREATE TABLE `gateway_privatbank` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_privatbank`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_privatbank` VALUES ('test', 'test', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_qiwi`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_qiwi`;
CREATE TABLE `gateway_qiwi` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_qiwi`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_qiwi` VALUES ('10859', 'test', '', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_rbkmoney`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_rbkmoney`;
CREATE TABLE `gateway_rbkmoney` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_rbkmoney`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_rbkmoney` VALUES ('cmsaccount', 'test1', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_robokassa`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_robokassa`;
CREATE TABLE `gateway_robokassa` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password2` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_robokassa`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_robokassa` VALUES ('cmsaccount', 'test1234', 'test4321', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_secpay`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_secpay`;
CREATE TABLE `gateway_secpay` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `subject` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `message` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_secpay`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_secpay` VALUES ('cmsaccount', '1', 'subject', 'message', 'https://www.secpay.com/java-bin/ValCard', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_segpay`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_segpay`;
CREATE TABLE `gateway_segpay` (
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL,
  `package_id` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `product_id` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `subscription` int(11) DEFAULT NULL,
  `credits` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_segpay`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_segpay` VALUES ('https://secure2.segpay.com/billing/poset.cgi', '0', '1', '0', '0', '0', '0'), ('', '0', '0', '1111', '4', '4465', '0'), ('', '0', '0', '1111', '3', '4464', '0'), ('', '0', '0', '1111', '2', '4462', '0'), ('', '0', '0', '1111', '1', '4461', '0'), ('', '0', '0', '2222', '5', '0', '4415'), ('', '0', '0', '2222', '6', '0', '4416'), ('', '0', '0', '2222', '7', '0', '4417'), ('', '0', '0', '2222', '8', '0', '4418');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_stripe`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_stripe`;
CREATE TABLE `gateway_stripe` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_stripe`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_stripe` VALUES ('pk_test_pa0tWISqU9pjRw6vggQhPGpY', 'sk_test_bG32VhNfdOWM5NvLMgG5G67X', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_webmoney`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_webmoney`;
CREATE TABLE `gateway_webmoney` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `ecode` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_webmoney`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_webmoney` VALUES ('Z160139856701', 'grgtrgrtgrt', 'https://merchant.webmoney.ru/lmi/payment.asp', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_worldpay`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_worldpay`;
CREATE TABLE `gateway_worldpay` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `ipn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_worldpay`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_worldpay` VALUES ('123', '1', 'https://select.worldpay.com/wcc/purchase', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gateway_zombaio`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_zombaio`;
CREATE TABLE `gateway_zombaio` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `price` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `gateway_zombaio`
-- ----------------------------
BEGIN;
INSERT INTO `gateway_zombaio` VALUES ('287654948', '84WS4308V3X4R6T7223N', '1479069', '0');
COMMIT;

-- ----------------------------
--  Table structure for `items`
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `shipped` tinyint(1) DEFAULT NULL,
  `price_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`),
  KEY `priority` (`priority`),
  KEY `price_id` (`price_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5260 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `items`
-- ----------------------------
BEGIN;
INSERT INTO `items` VALUES ('957', '4839', 'ZIP', 'vector.zip', '5', '0', '0', '4804'), ('1732', '4839', 'ZIP', 'vector.zip', '50', '0', '0', '6786'), ('1738', '4839', 'Shipped CD', '', '100', '5', '1', '6787'), ('2284', '7011', 'ZIP', 'cd.zip', '5', '2', '0', '4804'), ('2285', '7011', 'Shipped CD', '', '10', '5', '1', '5894'), ('2286', '7011', 'Shipped CD', '', '100', '5', '1', '6787'), ('2662', '7176', 'ZIP', 'cd.zip', '5', '2', '0', '4804'), ('2663', '7176', 'Shipped CD', '', '10', '5', '1', '5894'), ('2664', '7176', 'Shipped CD', '', '100', '5', '1', '6787'), ('3660', '7451', 'Multi-Seat (unlimited)', '_IMG_2381.jpg', '75', '1', '0', '6889'), ('3661', '7451', 'Small', '_IMG_2381.jpg', '1', '1', '0', '6885'), ('3662', '7451', 'Unlimited Reproduction / Print Runs', '_IMG_2381.jpg', '125', '2', '0', '6890'), ('3663', '7451', 'Medium', '_IMG_2381.jpg', '2', '2', '0', '6886'), ('3664', '7451', 'Items for Resale (limited run)', '_IMG_2381.jpg', '125', '3', '0', '6891'), ('3665', '7451', 'Large', '_IMG_2381.jpg', '3', '3', '0', '6887'), ('3666', '7451', 'Electronic Items for Resale (unlimited run)', '_IMG_2381.jpg', '125', '4', '0', '6892'), ('3667', '7451', 'Original size', '_IMG_2381.jpg', '4', '4', '0', '6888'), ('3668', '7451', 'Extended Legal Guarantee covers up to $250,000', '_IMG_2381.jpg', '100', '5', '0', '6893'), ('3669', '7452', 'Multi-Seat (unlimited)', '_IMG_2634.jpg', '75', '1', '0', '6889'), ('3670', '7452', 'Small', '_IMG_2634.jpg', '1', '1', '0', '6885'), ('3671', '7452', 'Unlimited Reproduction / Print Runs', '_IMG_2634.jpg', '125', '2', '0', '6890'), ('3672', '7452', 'Medium', '_IMG_2634.jpg', '2', '2', '0', '6886'), ('3673', '7452', 'Items for Resale (limited run)', '_IMG_2634.jpg', '125', '3', '0', '6891'), ('3674', '7452', 'Large', '_IMG_2634.jpg', '3', '3', '0', '6887'), ('3675', '7452', 'Electronic Items for Resale (unlimited run)', '_IMG_2634.jpg', '125', '4', '0', '6892'), ('3676', '7452', 'Original size', '_IMG_2634.jpg', '4', '4', '0', '6888'), ('3677', '7452', 'Extended Legal Guarantee covers up to $250,000', '_IMG_2634.jpg', '100', '5', '0', '6893'), ('3678', '7453', 'Multi-Seat (unlimited)', '_IMG_2639.jpg', '75', '1', '0', '6889'), ('3679', '7453', 'Small', '_IMG_2639.jpg', '1', '1', '0', '6885'), ('3680', '7453', 'Unlimited Reproduction / Print Runs', '_IMG_2639.jpg', '125', '2', '0', '6890'), ('3681', '7453', 'Medium', '_IMG_2639.jpg', '2', '2', '0', '6886'), ('3682', '7453', 'Items for Resale (limited run)', '_IMG_2639.jpg', '125', '3', '0', '6891'), ('3683', '7453', 'Large', '_IMG_2639.jpg', '3', '3', '0', '6887'), ('3684', '7453', 'Electronic Items for Resale (unlimited run)', '_IMG_2639.jpg', '125', '4', '0', '6892'), ('3685', '7453', 'Original size', '_IMG_2639.jpg', '4', '4', '0', '6888'), ('3686', '7453', 'Extended Legal Guarantee covers up to $250,000', '_IMG_2639.jpg', '100', '5', '0', '6893'), ('3687', '7454', 'Multi-Seat (unlimited)', '_IMG_2691.jpg', '75', '1', '0', '6889'), ('3688', '7454', 'Small', '_IMG_2691.jpg', '1', '1', '0', '6885'), ('3689', '7454', 'Unlimited Reproduction / Print Runs', '_IMG_2691.jpg', '125', '2', '0', '6890'), ('3690', '7454', 'Medium', '_IMG_2691.jpg', '2', '2', '0', '6886'), ('3691', '7454', 'Items for Resale (limited run)', '_IMG_2691.jpg', '125', '3', '0', '6891'), ('3692', '7454', 'Large', '_IMG_2691.jpg', '3', '3', '0', '6887'), ('3693', '7454', 'Electronic Items for Resale (unlimited run)', '_IMG_2691.jpg', '125', '4', '0', '6892'), ('3694', '7454', 'Original size', '_IMG_2691.jpg', '4', '4', '0', '6888'), ('3695', '7454', 'Extended Legal Guarantee covers up to $250,000', '_IMG_2691.jpg', '100', '5', '0', '6893'), ('3696', '7455', 'Multi-Seat (unlimited)', '_IMG_2735.jpg', '75', '1', '0', '6889'), ('3697', '7455', 'Small', '_IMG_2735.jpg', '1', '1', '0', '6885'), ('3698', '7455', 'Unlimited Reproduction / Print Runs', '_IMG_2735.jpg', '125', '2', '0', '6890'), ('3699', '7455', 'Medium', '_IMG_2735.jpg', '2', '2', '0', '6886'), ('3700', '7455', 'Items for Resale (limited run)', '_IMG_2735.jpg', '125', '3', '0', '6891'), ('3701', '7455', 'Large', '_IMG_2735.jpg', '3', '3', '0', '6887'), ('3702', '7455', 'Electronic Items for Resale (unlimited run)', '_IMG_2735.jpg', '125', '4', '0', '6892'), ('3703', '7455', 'Original size', '_IMG_2735.jpg', '4', '4', '0', '6888'), ('3704', '7455', 'Extended Legal Guarantee covers up to $250,000', '_IMG_2735.jpg', '100', '5', '0', '6893'), ('3705', '7456', 'Multi-Seat (unlimited)', '_IMG_2842.jpg', '75', '1', '0', '6889'), ('3706', '7456', 'Small', '_IMG_2842.jpg', '1', '1', '0', '6885'), ('3707', '7456', 'Unlimited Reproduction / Print Runs', '_IMG_2842.jpg', '125', '2', '0', '6890'), ('3708', '7456', 'Medium', '_IMG_2842.jpg', '2', '2', '0', '6886'), ('3709', '7456', 'Items for Resale (limited run)', '_IMG_2842.jpg', '125', '3', '0', '6891'), ('3710', '7456', 'Large', '_IMG_2842.jpg', '3', '3', '0', '6887'), ('3711', '7456', 'Electronic Items for Resale (unlimited run)', '_IMG_2842.jpg', '125', '4', '0', '6892'), ('3712', '7456', 'Original size', '_IMG_2842.jpg', '4', '4', '0', '6888'), ('3713', '7456', 'Extended Legal Guarantee covers up to $250,000', '_IMG_2842.jpg', '100', '5', '0', '6893'), ('3714', '7457', 'Multi-Seat (unlimited)', '_IMG_2911.jpg', '75', '1', '0', '6889'), ('3715', '7457', 'Small', '_IMG_2911.jpg', '1', '1', '0', '6885'), ('3716', '7457', 'Unlimited Reproduction / Print Runs', '_IMG_2911.jpg', '125', '2', '0', '6890'), ('3717', '7457', 'Medium', '_IMG_2911.jpg', '2', '2', '0', '6886'), ('3718', '7457', 'Items for Resale (limited run)', '_IMG_2911.jpg', '125', '3', '0', '6891'), ('3719', '7457', 'Large', '_IMG_2911.jpg', '3', '3', '0', '6887'), ('3720', '7457', 'Electronic Items for Resale (unlimited run)', '_IMG_2911.jpg', '125', '4', '0', '6892'), ('3721', '7457', 'Original size', '_IMG_2911.jpg', '4', '4', '0', '6888'), ('3722', '7457', 'Extended Legal Guarantee covers up to $250,000', '_IMG_2911.jpg', '100', '5', '0', '6893'), ('3723', '7458', 'Multi-Seat (unlimited)', '_IMG_2975.jpg', '75', '1', '0', '6889'), ('3724', '7458', 'Small', '_IMG_2975.jpg', '1', '1', '0', '6885'), ('3725', '7458', 'Unlimited Reproduction / Print Runs', '_IMG_2975.jpg', '125', '2', '0', '6890'), ('3726', '7458', 'Medium', '_IMG_2975.jpg', '2', '2', '0', '6886'), ('3727', '7458', 'Items for Resale (limited run)', '_IMG_2975.jpg', '125', '3', '0', '6891'), ('3728', '7458', 'Large', '_IMG_2975.jpg', '3', '3', '0', '6887'), ('3729', '7458', 'Electronic Items for Resale (unlimited run)', '_IMG_2975.jpg', '125', '4', '0', '6892'), ('3730', '7458', 'Original size', '_IMG_2975.jpg', '4', '4', '0', '6888'), ('3731', '7458', 'Extended Legal Guarantee covers up to $250,000', '_IMG_2975.jpg', '100', '5', '0', '6893'), ('3732', '7459', 'Multi-Seat (unlimited)', '_IMG_3036.jpg', '75', '1', '0', '6889'), ('3733', '7459', 'Small', '_IMG_3036.jpg', '1', '1', '0', '6885'), ('3734', '7459', 'Unlimited Reproduction / Print Runs', '_IMG_3036.jpg', '125', '2', '0', '6890'), ('3735', '7459', 'Medium', '_IMG_3036.jpg', '2', '2', '0', '6886'), ('3736', '7459', 'Items for Resale (limited run)', '_IMG_3036.jpg', '125', '3', '0', '6891'), ('3737', '7459', 'Large', '_IMG_3036.jpg', '3', '3', '0', '6887'), ('3738', '7459', 'Electronic Items for Resale (unlimited run)', '_IMG_3036.jpg', '125', '4', '0', '6892'), ('3739', '7459', 'Original size', '_IMG_3036.jpg', '4', '4', '0', '6888'), ('3740', '7459', 'Extended Legal Guarantee covers up to $250,000', '_IMG_3036.jpg', '100', '5', '0', '6893'), ('3741', '7461', 'Multi-Seat (unlimited)', '_IMG_1659.jpg', '75', '1', '0', '6889'), ('3742', '7461', 'Small', '_IMG_1659.jpg', '1', '1', '0', '6885'), ('3743', '7461', 'Unlimited Reproduction / Print Runs', '_IMG_1659.jpg', '125', '2', '0', '6890'), ('3744', '7461', 'Medium', '_IMG_1659.jpg', '2', '2', '0', '6886'), ('3745', '7461', 'Items for Resale (limited run)', '_IMG_1659.jpg', '125', '3', '0', '6891'), ('3746', '7461', 'Large', '_IMG_1659.jpg', '3', '3', '0', '6887'), ('3747', '7461', 'Electronic Items for Resale (unlimited run)', '_IMG_1659.jpg', '125', '4', '0', '6892'), ('3748', '7461', 'Original size', '_IMG_1659.jpg', '4', '4', '0', '6888'), ('3749', '7461', 'Extended Legal Guarantee covers up to $250,000', '_IMG_1659.jpg', '100', '5', '0', '6893'), ('3750', '7462', 'Multi-Seat (unlimited)', '_IMG_3319.jpg', '75', '1', '0', '6889'), ('3751', '7462', 'Small', '_IMG_3319.jpg', '1', '1', '0', '6885'), ('3752', '7462', 'Unlimited Reproduction / Print Runs', '_IMG_3319.jpg', '125', '2', '0', '6890'), ('3753', '7462', 'Medium', '_IMG_3319.jpg', '2', '2', '0', '6886'), ('3754', '7462', 'Items for Resale (limited run)', '_IMG_3319.jpg', '125', '3', '0', '6891'), ('3755', '7462', 'Large', '_IMG_3319.jpg', '3', '3', '0', '6887'), ('3756', '7462', 'Electronic Items for Resale (unlimited run)', '_IMG_3319.jpg', '125', '4', '0', '6892'), ('3757', '7462', 'Original size', '_IMG_3319.jpg', '4', '4', '0', '6888'), ('3758', '7462', 'Extended Legal Guarantee covers up to $250,000', '_IMG_3319.jpg', '100', '5', '0', '6893'), ('3759', '7463', 'Multi-Seat (unlimited)', '_IMG_3733.jpg', '75', '1', '0', '6889'), ('3760', '7463', 'Small', '_IMG_3733.jpg', '1', '1', '0', '6885'), ('3761', '7463', 'Unlimited Reproduction / Print Runs', '_IMG_3733.jpg', '125', '2', '0', '6890'), ('3762', '7463', 'Medium', '_IMG_3733.jpg', '2', '2', '0', '6886'), ('3763', '7463', 'Items for Resale (limited run)', '_IMG_3733.jpg', '125', '3', '0', '6891'), ('3764', '7463', 'Large', '_IMG_3733.jpg', '3', '3', '0', '6887'), ('3765', '7463', 'Electronic Items for Resale (unlimited run)', '_IMG_3733.jpg', '125', '4', '0', '6892'), ('3766', '7463', 'Original size', '_IMG_3733.jpg', '4', '4', '0', '6888'), ('3767', '7463', 'Extended Legal Guarantee covers up to $250,000', '_IMG_3733.jpg', '100', '5', '0', '6893'), ('3768', '7464', 'Multi-Seat (unlimited)', '_IMG_4055.jpg', '75', '1', '0', '6889'), ('3769', '7464', 'Small', '_IMG_4055.jpg', '1', '1', '0', '6885'), ('3770', '7464', 'Unlimited Reproduction / Print Runs', '_IMG_4055.jpg', '125', '2', '0', '6890'), ('3771', '7464', 'Medium', '_IMG_4055.jpg', '2', '2', '0', '6886'), ('3772', '7464', 'Items for Resale (limited run)', '_IMG_4055.jpg', '125', '3', '0', '6891'), ('3773', '7464', 'Large', '_IMG_4055.jpg', '3', '3', '0', '6887'), ('3774', '7464', 'Electronic Items for Resale (unlimited run)', '_IMG_4055.jpg', '125', '4', '0', '6892'), ('3775', '7464', 'Original size', '_IMG_4055.jpg', '4', '4', '0', '6888'), ('3776', '7464', 'Extended Legal Guarantee covers up to $250,000', '_IMG_4055.jpg', '100', '5', '0', '6893'), ('3777', '7465', 'Multi-Seat (unlimited)', '_IMG_1978.jpg', '75', '1', '0', '6889'), ('3778', '7465', 'Small', '_IMG_1978.jpg', '1', '1', '0', '6885'), ('3779', '7465', 'Unlimited Reproduction / Print Runs', '_IMG_1978.jpg', '125', '2', '0', '6890'), ('3780', '7465', 'Medium', '_IMG_1978.jpg', '2', '2', '0', '6886'), ('3781', '7465', 'Items for Resale (limited run)', '_IMG_1978.jpg', '125', '3', '0', '6891'), ('3782', '7465', 'Large', '_IMG_1978.jpg', '3', '3', '0', '6887'), ('3783', '7465', 'Electronic Items for Resale (unlimited run)', '_IMG_1978.jpg', '125', '4', '0', '6892'), ('3784', '7465', 'Original size', '_IMG_1978.jpg', '4', '4', '0', '6888'), ('3785', '7465', 'Extended Legal Guarantee covers up to $250,000', '_IMG_1978.jpg', '100', '5', '0', '6893'), ('3786', '7466', 'Multi-Seat (unlimited)', '_IMG_3289.jpg', '75', '1', '0', '6889'), ('3787', '7466', 'Small', '_IMG_3289.jpg', '1', '1', '0', '6885'), ('3788', '7466', 'Unlimited Reproduction / Print Runs', '_IMG_3289.jpg', '125', '2', '0', '6890'), ('3789', '7466', 'Medium', '_IMG_3289.jpg', '2', '2', '0', '6886'), ('3790', '7466', 'Items for Resale (limited run)', '_IMG_3289.jpg', '125', '3', '0', '6891'), ('3791', '7466', 'Large', '_IMG_3289.jpg', '3', '3', '0', '6887'), ('3792', '7466', 'Electronic Items for Resale (unlimited run)', '_IMG_3289.jpg', '125', '4', '0', '6892'), ('3793', '7466', 'Original size', '_IMG_3289.jpg', '4', '4', '0', '6888'), ('3794', '7466', 'Extended Legal Guarantee covers up to $250,000', '_IMG_3289.jpg', '100', '5', '0', '6893'), ('3795', '7467', 'Multi-Seat (unlimited)', '_IMG_3329.jpg', '75', '1', '0', '6889'), ('3796', '7467', 'Small', '_IMG_3329.jpg', '1', '1', '0', '6885'), ('3797', '7467', 'Unlimited Reproduction / Print Runs', '_IMG_3329.jpg', '125', '2', '0', '6890'), ('3798', '7467', 'Medium', '_IMG_3329.jpg', '2', '2', '0', '6886'), ('3799', '7467', 'Items for Resale (limited run)', '_IMG_3329.jpg', '125', '3', '0', '6891'), ('3800', '7467', 'Large', '_IMG_3329.jpg', '3', '3', '0', '6887'), ('3801', '7467', 'Electronic Items for Resale (unlimited run)', '_IMG_3329.jpg', '125', '4', '0', '6892'), ('3802', '7467', 'Original size', '_IMG_3329.jpg', '4', '4', '0', '6888'), ('3803', '7467', 'Extended Legal Guarantee covers up to $250,000', '_IMG_3329.jpg', '100', '5', '0', '6893'), ('3804', '7468', 'Multi-Seat (unlimited)', '_IMG_4143.jpg', '75', '1', '0', '6889'), ('3805', '7468', 'Small', '_IMG_4143.jpg', '1', '1', '0', '6885'), ('3806', '7468', 'Unlimited Reproduction / Print Runs', '_IMG_4143.jpg', '125', '2', '0', '6890'), ('3807', '7468', 'Medium', '_IMG_4143.jpg', '2', '2', '0', '6886'), ('3808', '7468', 'Items for Resale (limited run)', '_IMG_4143.jpg', '125', '3', '0', '6891'), ('3809', '7468', 'Large', '_IMG_4143.jpg', '3', '3', '0', '6887'), ('3810', '7468', 'Electronic Items for Resale (unlimited run)', '_IMG_4143.jpg', '125', '4', '0', '6892'), ('3811', '7468', 'Original size', '_IMG_4143.jpg', '4', '4', '0', '6888'), ('3812', '7468', 'Extended Legal Guarantee covers up to $250,000', '_IMG_4143.jpg', '100', '5', '0', '6893'), ('3813', '7469', 'Multi-Seat (unlimited)', '_IMG_4648.jpg', '75', '1', '0', '6889'), ('3814', '7469', 'Small', '_IMG_4648.jpg', '1', '1', '0', '6885'), ('3815', '7469', 'Unlimited Reproduction / Print Runs', '_IMG_4648.jpg', '125', '2', '0', '6890'), ('3816', '7469', 'Medium', '_IMG_4648.jpg', '2', '2', '0', '6886'), ('3817', '7469', 'Items for Resale (limited run)', '_IMG_4648.jpg', '125', '3', '0', '6891'), ('3818', '7469', 'Large', '_IMG_4648.jpg', '3', '3', '0', '6887'), ('3819', '7469', 'Electronic Items for Resale (unlimited run)', '_IMG_4648.jpg', '125', '4', '0', '6892'), ('3820', '7469', 'Original size', '_IMG_4648.jpg', '4', '4', '0', '6888'), ('3821', '7469', 'Extended Legal Guarantee covers up to $250,000', '_IMG_4648.jpg', '100', '5', '0', '6893'), ('3822', '7470', 'Multi-Seat (unlimited)', '_IMG_5257.jpg', '75', '1', '0', '6889'), ('3823', '7470', 'Small', '_IMG_5257.jpg', '1', '1', '0', '6885'), ('3824', '7470', 'Unlimited Reproduction / Print Runs', '_IMG_5257.jpg', '125', '2', '0', '6890'), ('3825', '7470', 'Medium', '_IMG_5257.jpg', '2', '2', '0', '6886'), ('3826', '7470', 'Items for Resale (limited run)', '_IMG_5257.jpg', '125', '3', '0', '6891'), ('3827', '7470', 'Large', '_IMG_5257.jpg', '3', '3', '0', '6887'), ('3828', '7470', 'Electronic Items for Resale (unlimited run)', '_IMG_5257.jpg', '125', '4', '0', '6892'), ('3829', '7470', 'Original size', '_IMG_5257.jpg', '4', '4', '0', '6888'), ('3830', '7470', 'Extended Legal Guarantee covers up to $250,000', '_IMG_5257.jpg', '100', '5', '0', '6893'), ('3831', '7471', 'Multi-Seat (unlimited)', '_IMG_6638.jpg', '75', '1', '0', '6889'), ('3832', '7471', 'Small', '_IMG_6638.jpg', '1', '1', '0', '6885'), ('3833', '7471', 'Unlimited Reproduction / Print Runs', '_IMG_6638.jpg', '125', '2', '0', '6890'), ('3834', '7471', 'Medium', '_IMG_6638.jpg', '2', '2', '0', '6886'), ('3835', '7471', 'Items for Resale (limited run)', '_IMG_6638.jpg', '125', '3', '0', '6891'), ('3836', '7471', 'Large', '_IMG_6638.jpg', '3', '3', '0', '6887'), ('3837', '7471', 'Electronic Items for Resale (unlimited run)', '_IMG_6638.jpg', '125', '4', '0', '6892'), ('3838', '7471', 'Original size', '_IMG_6638.jpg', '4', '4', '0', '6888'), ('3839', '7471', 'Extended Legal Guarantee covers up to $250,000', '_IMG_6638.jpg', '100', '5', '0', '6893'), ('4489', '7587', 'Multi-Seat (unlimited)', '_IMG_0783_iptc.jpg', '75', '1', '0', '6889'), ('4490', '7587', 'Small', '_IMG_0783_iptc.jpg', '1', '1', '0', '6885'), ('4491', '7587', 'Unlimited Reproduction / Print Runs', '_IMG_0783_iptc.jpg', '125', '2', '0', '6890'), ('4492', '7587', 'Medium', '_IMG_0783_iptc.jpg', '2', '2', '0', '6886'), ('4493', '7587', 'Items for Resale (limited run)', '_IMG_0783_iptc.jpg', '125', '3', '0', '6891'), ('4494', '7587', 'Large', '_IMG_0783_iptc.jpg', '3', '3', '0', '6887'), ('4495', '7587', 'Electronic Items for Resale (unlimited run)', '_IMG_0783_iptc.jpg', '125', '4', '0', '6892'), ('4496', '7587', 'Original size', '_IMG_0783_iptc.jpg', '4', '4', '0', '6888'), ('4497', '7587', 'Extended Legal Guarantee covers up to $250,000', '_IMG_0783_iptc.jpg', '100', '5', '0', '6893'), ('4498', '7588', 'Multi-Seat (unlimited)', '_IMG_0802.JPG', '75', '1', '0', '6889'), ('4499', '7588', 'Small', '_IMG_0802.JPG', '1', '1', '0', '6885'), ('4500', '7588', 'Unlimited Reproduction / Print Runs', '_IMG_0802.JPG', '125', '2', '0', '6890'), ('4501', '7588', 'Medium', '_IMG_0802.JPG', '2', '2', '0', '6886'), ('4502', '7588', 'Items for Resale (limited run)', '_IMG_0802.JPG', '125', '3', '0', '6891'), ('4503', '7588', 'Large', '_IMG_0802.JPG', '3', '3', '0', '6887'), ('4504', '7588', 'Electronic Items for Resale (unlimited run)', '_IMG_0802.JPG', '125', '4', '0', '6892'), ('4505', '7588', 'Original size', '_IMG_0802.JPG', '4', '4', '0', '6888'), ('4506', '7588', 'Extended Legal Guarantee covers up to $250,000', '_IMG_0802.JPG', '100', '5', '0', '6893'), ('4507', '7589', 'Multi-Seat (unlimited)', '_IMG_0811.jpg', '75', '1', '0', '6889'), ('4508', '7589', 'Small', '_IMG_0811.jpg', '1', '1', '0', '6885'), ('4509', '7589', 'Unlimited Reproduction / Print Runs', '_IMG_0811.jpg', '125', '2', '0', '6890'), ('4510', '7589', 'Medium', '_IMG_0811.jpg', '2', '2', '0', '6886'), ('4511', '7589', 'Items for Resale (limited run)', '_IMG_0811.jpg', '125', '3', '0', '6891'), ('4512', '7589', 'Large', '_IMG_0811.jpg', '3', '3', '0', '6887'), ('4513', '7589', 'Electronic Items for Resale (unlimited run)', '_IMG_0811.jpg', '125', '4', '0', '6892'), ('4514', '7589', 'Original size', '_IMG_0811.jpg', '4', '4', '0', '6888'), ('4515', '7589', 'Extended Legal Guarantee covers up to $250,000', '_IMG_0811.jpg', '100', '5', '0', '6893'), ('4931', '7655', 'MP4', 'video.mp4', '40', '0', '0', '6786'), ('4932', '7655', 'MP4', 'video.mp4', '5', '0', '0', '6784'), ('4933', '7655', 'Shipped CD', '', '10', '5', '1', '5892'), ('4934', '7655', 'Shipped CD', '', '100', '5', '1', '6781'), ('4935', '7656', 'MP4', 'video2.mp4', '40', '0', '0', '6786'), ('4936', '7656', 'MP4', 'video2.mp4', '5', '0', '0', '6784'), ('4937', '7656', 'Shipped CD', '', '10', '5', '1', '5892'), ('4938', '7656', 'Shipped CD', '', '100', '5', '1', '6781'), ('4939', '7657', 'MP3', 'audio1.mp3', '1', '1', '0', '4801'), ('4940', '7657', 'MP3', 'audio1.mp3', '10', '1', '0', '6783'), ('4941', '7657', 'Shipped CD', '', '100', '3', '1', '6784'), ('4942', '7657', 'Shipped CD', '', '10', '5', '1', '5893'), ('4943', '7658', 'MP3', 'audio2.mp3', '1', '1', '0', '4801'), ('4944', '7658', 'MP3', 'audio2.mp3', '10', '1', '0', '6783'), ('4945', '7658', 'Shipped CD', '', '100', '3', '1', '6784'), ('4946', '7658', 'Shipped CD', '', '10', '5', '1', '5893'), ('5027', '7670', 'Multi-Seat (unlimited)', 'IMG_1608.jpg', '75', '1', '0', '6889'), ('5028', '7670', 'Small', 'IMG_1608.jpg', '1', '1', '0', '6885'), ('5029', '7670', 'Unlimited Reproduction / Print Runs', 'IMG_1608.jpg', '125', '2', '0', '6890'), ('5030', '7670', 'Medium', 'IMG_1608.jpg', '2', '2', '0', '6886'), ('5031', '7670', 'Items for Resale (limited run)', 'IMG_1608.jpg', '125', '3', '0', '6891'), ('5032', '7670', 'Large', 'IMG_1608.jpg', '3', '3', '0', '6887'), ('5033', '7670', 'Electronic Items for Resale (unlimited run)', 'IMG_1608.jpg', '125', '4', '0', '6892'), ('5034', '7670', 'Original size', 'IMG_1608.jpg', '4', '4', '0', '6888'), ('5035', '7670', 'Extended Legal Guarantee covers up to $250,000', 'IMG_1608.jpg', '100', '5', '0', '6893'), ('5036', '7671', 'Multi-Seat (unlimited)', '_IMG_0783_iptc.jpg', '75', '1', '0', '6889'), ('5037', '7671', 'Small', '_IMG_0783_iptc.jpg', '1', '1', '0', '6885'), ('5038', '7671', 'Unlimited Reproduction / Print Runs', '_IMG_0783_iptc.jpg', '125', '2', '0', '6890'), ('5039', '7671', 'Medium', '_IMG_0783_iptc.jpg', '2', '2', '0', '6886'), ('5040', '7671', 'Items for Resale (limited run)', '_IMG_0783_iptc.jpg', '125', '3', '0', '6891'), ('5041', '7671', 'Large', '_IMG_0783_iptc.jpg', '3', '3', '0', '6887'), ('5042', '7671', 'Electronic Items for Resale (unlimited run)', '_IMG_0783_iptc.jpg', '125', '4', '0', '6892'), ('5043', '7671', 'Original size', '_IMG_0783_iptc.jpg', '4', '4', '0', '6888'), ('5044', '7671', 'Extended Legal Guarantee covers up to $250,000', '_IMG_0783_iptc.jpg', '100', '5', '0', '6893'), ('5045', '7672', 'Multi-Seat (unlimited)', 'IMG_1608.jpg', '75', '1', '0', '6889'), ('5046', '7672', 'Small', 'IMG_1608.jpg', '1', '1', '0', '6885'), ('5047', '7672', 'Unlimited Reproduction / Print Runs', 'IMG_1608.jpg', '125', '2', '0', '6890'), ('5048', '7672', 'Medium', 'IMG_1608.jpg', '2', '2', '0', '6886'), ('5049', '7672', 'Items for Resale (limited run)', 'IMG_1608.jpg', '125', '3', '0', '6891'), ('5050', '7672', 'Large', 'IMG_1608.jpg', '3', '3', '0', '6887'), ('5051', '7672', 'Electronic Items for Resale (unlimited run)', 'IMG_1608.jpg', '125', '4', '0', '6892'), ('5052', '7672', 'Original size', 'IMG_1608.jpg', '4', '4', '0', '6888'), ('5053', '7672', 'Extended Legal Guarantee covers up to $250,000', 'IMG_1608.jpg', '100', '5', '0', '6893'), ('5054', '7673', 'Multi-Seat (unlimited)', '_IMG_0783_iptc.jpg', '75', '1', '0', '6889'), ('5055', '7673', 'Small', '_IMG_0783_iptc.jpg', '1', '1', '0', '6885'), ('5056', '7673', 'Unlimited Reproduction / Print Runs', '_IMG_0783_iptc.jpg', '125', '2', '0', '6890'), ('5057', '7673', 'Medium', '_IMG_0783_iptc.jpg', '2', '2', '0', '6886'), ('5058', '7673', 'Items for Resale (limited run)', '_IMG_0783_iptc.jpg', '125', '3', '0', '6891'), ('5059', '7673', 'Large', '_IMG_0783_iptc.jpg', '3', '3', '0', '6887'), ('5060', '7673', 'Electronic Items for Resale (unlimited run)', '_IMG_0783_iptc.jpg', '125', '4', '0', '6892'), ('5061', '7673', 'Original size', '_IMG_0783_iptc.jpg', '4', '4', '0', '6888'), ('5062', '7673', 'Extended Legal Guarantee covers up to $250,000', '_IMG_0783_iptc.jpg', '100', '5', '0', '6893'), ('5257', '7713', 'ZIP', 'cd.zip', '5', '2', '0', '4804'), ('5258', '7713', 'Shipped CD', '', '10', '5', '1', '5894'), ('5259', '7713', 'Shipped CD', '', '100', '5', '1', '6787');
COMMIT;

-- ----------------------------
--  Table structure for `languages`
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `display` int(11) DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `metatags` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `languages`
-- ----------------------------
BEGIN;
INSERT INTO `languages` VALUES ('English', '1', '1', 'utf-8'), ('French', '1', '0', 'utf-8'), ('German', '1', '0', 'utf-8'), ('Italian', '1', '0', 'utf-8'), ('Portuguese', '1', '0', 'utf-8'), ('Spanish', '1', '0', 'utf-8'), ('Russian', '1', '0', 'utf-8'), ('Swedish', '1', '0', 'utf-8'), ('Chinese simplified', '1', '0', 'utf-8'), ('Catalan', '1', '0', 'utf-8'), ('Arabic', '1', '0', 'utf-8'), ('Malaysian', '1', '0', 'utf-8'), ('Bulgarian', '1', '0', 'utf-8'), ('Polish', '1', '0', 'utf-8'), ('Japanese', '1', '0', 'utf-8'), ('Greek', '1', '0', 'utf-8'), ('Dutch', '1', '0', 'utf-8'), ('Norwegian', '1', '0', 'utf-8'), ('Finnish', '1', '0', 'utf-8'), ('Czech', '1', '0', 'utf-8'), ('Estonian', '1', '0', 'utf-8'), ('Serbian', '1', '0', 'utf-8'), ('Hungarian', '1', '0', 'utf-8'), ('Danish', '1', '0', 'utf-8'), ('Romanian', '1', '0', 'utf-8'), ('Hebrew', '1', '0', 'utf-8'), ('Indonesian', '1', '0', 'utf-8'), ('Chinese traditional', '1', '0', 'utf-8'), ('Afrikaans formal', '1', '0', 'utf-8'), ('Afrikaans informal', '1', '0', 'utf-8'), ('Slovakian', '1', '0', 'utf-8'), ('Persian', '1', '0', 'utf-8'), ('Latvian', '1', '0', 'utf-8'), ('Slovenian', '1', '0', 'utf-8'), ('Lithuanian', '1', '0', 'utf-8'), ('Turkish', '1', '0', 'utf-8'), ('Thai', '1', '0', 'utf-8'), ('Brazilian', '1', '0', 'utf-8'), ('Ukrainian', '1', '0', 'utf-8'), ('Georgian', '1', '0', 'utf-8'), ('Croatian', '1', '0', 'utf-8'), ('Icelandic', '1', '0', 'utf-8');
COMMIT;

-- ----------------------------
--  Table structure for `licenses`
-- ----------------------------
DROP TABLE IF EXISTS `licenses`;
CREATE TABLE `licenses` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `priority` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=4585 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `licenses`
-- ----------------------------
BEGIN;
INSERT INTO `licenses` VALUES ('4583', 'Standard', 0x3c703e4465736372697074696f6e206f6620526f79616c74792046726565206c6963656e73652e3c2f703e, '1'), ('4584', 'Extended', 0x4465736372697074696f6e206f6620457874656e646564206c6963656e73652e3c6272202f3e, '2');
COMMIT;

-- ----------------------------
--  Table structure for `lightboxes`
-- ----------------------------
DROP TABLE IF EXISTS `lightboxes`;
CREATE TABLE `lightboxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `lightboxes`
-- ----------------------------
BEGIN;
INSERT INTO `lightboxes` VALUES ('3', 'Trees', ''), ('4', 'Cities', ''), ('7', 'My lightbox', ''), ('8', 'Premium', ''), ('10', 'Excellent photos', ''), ('11', 'Common', ''), ('12', 'Wow', '');
COMMIT;

-- ----------------------------
--  Table structure for `lightboxes_admin`
-- ----------------------------
DROP TABLE IF EXISTS `lightboxes_admin`;
CREATE TABLE `lightboxes_admin` (
  `id_parent` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `user_owner` tinyint(4) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `user` (`user`),
  KEY `user_owner` (`user_owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `lightboxes_admin`
-- ----------------------------
BEGIN;
INSERT INTO `lightboxes_admin` VALUES ('3', '3931', '1'), ('4', '3931', '1'), ('12', '7403', '1'), ('11', '7403', '1'), ('7', '6355', '1'), ('8', '6355', '1'), ('10', '3931', '0'), ('10', '4131', '1'), ('7', '4131', '0'), ('7', '4132', '0');
COMMIT;

-- ----------------------------
--  Table structure for `lightboxes_files`
-- ----------------------------
DROP TABLE IF EXISTS `lightboxes_files`;
CREATE TABLE `lightboxes_files` (
  `id_parent` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `item` (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `lightboxes_files`
-- ----------------------------
BEGIN;
INSERT INTO `lightboxes_files` VALUES ('3', '7465'), ('10', '7471'), ('3', '7466'), ('12', '7176'), ('12', '7471'), ('11', '7471'), ('12', '7464'), ('11', '7713'), ('4', '7470'), ('3', '7469'), ('4', '7465'), ('3', '7468'), ('12', '7468'), ('7', '7465'), ('8', '7471'), ('7', '7451'), ('7', '7471'), ('3', '7471'), ('12', '7451'), ('12', '7658'), ('11', '7464');
COMMIT;

-- ----------------------------
--  Table structure for `messages`
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `touser` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `fromuser` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `subject` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `data` int(11) DEFAULT NULL,
  `viewed` int(11) DEFAULT NULL,
  `trash` int(11) DEFAULT NULL,
  `del` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `touser` (`touser`),
  KEY `fromuser` (`fromuser`),
  KEY `data` (`data`)
) ENGINE=InnoDB AUTO_INCREMENT=4780 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `messages`
-- ----------------------------
BEGIN;
INSERT INTO `messages` VALUES ('4143', 'john', 'demo', 'Hi', 0x4869204a6f686e202620426f6279210d0a0d0a486f77206120726520796f753f0d0a0d0a5468616e6b20796f752c0d0a64656d6f, '1193657959', '0', '0', '0'), ('4142', 'siteowner', 'demo', 'Hi', 0x4869204a6f686e202620426f6279210d0a0d0a486f77206120726520796f753f0d0a0d0a5468616e6b20796f752c0d0a64656d6f, '1193657959', '0', '0', '0'), ('4146', 'demo', 'siteowner', 'Salut!', 0x486f772061726520796f753f0d0a0d0a426f6279, '1193828492', '1', '0', '0'), ('4147', 'demo', 'john', 'New topic', 0x57686572652061726520796f753f0d0a0d0a4a6f686e, '1193829019', '1', '1', '0'), ('4148', 'siteowner', 'demo', 'Re: Salut!', 0x4920616d2066696e652e0d0a5468616e6b730d0a0d0a0d0a0d0a0d0a596f752077726f74653a2031302f33312f323030372031343a30313a33320d0a486f772061726520796f753f0d0a0d0a426f6279, '1193861398', '0', '0', '0'), ('4776', 'demo', 'Site Administration', 'Test Newsletter!', 0x4869206576657279626f647921, '1216833339', '1', '1', '1'), ('4778', 'siteowner', 'Site Administration', 'Test Newsletter!', 0x4869206576657279626f647921, '1216833339', '0', '0', '0'), ('4779', 'john', 'Site Administration', 'Test Newsletter!', 0x4869206576657279626f647921, '1216833339', '0', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `models`
-- ----------------------------
DROP TABLE IF EXISTS `models`;
CREATE TABLE `models` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `user` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `model` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `modelphoto` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `user` (`user`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6206 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `models`
-- ----------------------------
BEGIN;
INSERT INTO `models` VALUES ('6205', 'Model 1', 0x4465736372697074696f6e206f66204d6f64656c2031, 'demo', '/content/models/model6205.jpg', '/content/models/modelphoto6205.JPG');
COMMIT;

-- ----------------------------
--  Table structure for `navigation`
-- ----------------------------
DROP TABLE IF EXISTS `navigation`;
CREATE TABLE `navigation` (
  `id_parent` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `priority` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `navigation`
-- ----------------------------
BEGIN;
INSERT INTO `navigation` VALUES ('4016', 'About', '/pages/about.html', '2'), ('4017', 'News', '/news/', '3'), ('4018', 'Contacts', '/contacts/', '4'), ('4020', 'About', '/pages/about.html', '2'), ('4021', 'News', '/news/', '3'), ('4022', 'Contacts', '/contacts/', '4');
COMMIT;

-- ----------------------------
--  Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `announce` text COLLATE utf8_bin,
  `content` text COLLATE utf8_bin,
  `data` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `data` (`data`)
) ENGINE=InnoDB AUTO_INCREMENT=3928 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `news`
-- ----------------------------
BEGIN;
INSERT INTO `news` VALUES ('3925', 0x546865206e65772076657273696f6e206f662050686f746f20566964656f2053656c6c696e6720536f66747761726520697320617661696c61626c652e2054776f206e65772074656d706c6174657320776572652061646465642e0d0a, '', '1341666595', 'Version 12.06 released'), ('3926', 0x546865206e65772076657273696f6e206f662050686f746f20566964656f2053746f726520736f66747761726520686173206265656e2072656c65617365643a2061206e65772074656d706c617465732c2061206e65772061646d696e2070616e656c2c2068746d6c20666f726d617420666f7220656d61696c732e, '', '1352815920', 'New version 12.10 '), ('3927', 0x546865206e65772076657273696f6e206f662050686f746f20566964656f2053656c6c696e672073637269707420686173206265656e2072656c65617365643a206f6e65206e65772074656d706c6174652c206175746f20706167696e672c206e65772074617865732f7368697070696e672f7072696e74732073797374656d7320616e64206d616e79206f746865722066656174757265732e, '', '1366722782', 'Version 13.04');
COMMIT;

-- ----------------------------
--  Table structure for `newsletter`
-- ----------------------------
DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` int(11) DEFAULT NULL,
  `touser` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `types` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `subject` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `html` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `newsletter`
-- ----------------------------
BEGIN;
INSERT INTO `newsletter` VALUES ('4', '1216833339', 'newsletter', 'message', 'Test Newsletter!', 0x4869206576657279626f647921, null), ('7', '1360423685', 'seller_newsletter', 'message', 'New newsletter', 0x3c703e546573743c2f703e0d0a3c703e266e6273703b3c2f703e, '1');
COMMIT;

-- ----------------------------
--  Table structure for `notifications`
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `events` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `message` text COLLATE utf8_bin,
  `enabled` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `subject` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `html` tinyint(4) DEFAULT NULL,
  KEY `priority` (`priority`),
  KEY `events` (`events`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `notifications`
-- ----------------------------
BEGIN;
INSERT INTO `notifications` VALUES ('contacts_to_admin', 'Contact email to admin', 0x7b6c616e672e4e616d657d3a207b4e414d457d0d0a7b6c616e672e452d6d61696c7d3a207b454d41494c7d0d0a7b6c616e672e54656c6570686f6e657d3a207b54454c4550484f4e457d0d0a7b6c616e672e4d6574686f647d3a207b4d4554484f447d0d0a7b6c616e672e5175657374696f6e7d3a207b5155455354494f4e7d0d0a7b6c616e672e446174657d3a207b444154457d, '1', '10', 'Contact Us on {SITE_NAME}', '1'), ('contacts_to_user', 'Contacts response to user', 0x5468616e6b20796f752c207b4e414d457d2e20596f7572206d65737361676520686173206265656e2072656365697665642e2057652077696c6c20726573706f6e73652073746f72746c792e0d0a0d0a4265737420726567617264732c0d0a7b534954455f4e414d457d, '1', '20', 'Re: Contact Us on {SITE_NAME}', '1'), ('fraud_to_user', 'Fraud auth email to user', 0x48656c6c6f207b4e414d457d2c0d0a0d0a536f6d65626f647920756e7375636365737366756c6c7920747269656420746f206c6f6720696e20746f20796f7572206163636f756e74207365766572616c2074696d65732e2057652068617665206368616e67656420796f75722070617373776f72642e2049742069732073656375726974792069737375652e0d0a0d0a0d0a596f7572204e45572070617373776f72643a20207b4e455750415353574f52447d0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '30', 'You password has been changed on {SITE_NAME}', '1'), ('neworder_to_user', 'New order email to user', 0x48656c6c6f207b4e414d457d2c0d0a0d0a5468616e6b7320666f7220627579696e67206f6e207b534954455f4e414d457d212057652077696c6c205f636861725f676520616e64207368697020796f7572206f7264657220736f6f6e2e200d0a0d0a7b6c616e672e4f726465727d205f726573685f207b4f524445527d0d0a7b6c616e672e446174657d3a207b444154457d0d0a0d0a7b4954454d5f4c4953547d0d0a0d0a7b6c616e672e537562746f74616c7d3a207b535542544f54414c7d0d0a7b6c616e672e446973636f756e747d3a207b444953434f554e547d0d0a7b6c616e672e5368697070696e677d3a207b5348495050494e477d0d0a7b6c616e672e54617865737d3a207b54415845537d0d0a7b6c616e672e546f74616c7d3a207b544f54414c7d0d0a0d0a0d0a7b6c616e672e42696c6c696e6720616464726573737d0d0a7b42494c4c494e475f46495253544e414d457d207b42494c4c494e475f4c4153544e414d457d0d0a7b42494c4c494e475f414444524553537d0d0a7b42494c4c494e475f434954597d207b42494c4c494e475f5a49507d2c207b42494c4c494e475f434f554e5452597d0d0a0d0a0d0a7b6c616e672e5368697070696e6720616464726573737d0d0a7b5348495050494e475f46495253544e414d457d207b5348495050494e475f4c4153544e414d457d0d0a7b5348495050494e475f414444524553537d0d0a7b5348495050494e475f434954597d207b5348495050494e475f5a49507d2c207b5348495050494e475f434f554e5452597d0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '40', 'Order _resh_ {ORDER_ID} on {SITE_NAME}', '1'), ('neworder_to_admin', 'New order email to admin', 0x7b6c616e672e4f726465727d205f726573685f207b4f524445527d0d0a7b6c616e672e646174657d3a207b444154457d0d0a0d0a7b4954454d5f4c4953547d0d0a0d0a7b6c616e672e537562746f74616c7d3a207b535542544f54414c7d0d0a7b6c616e672e446973636f756e747d3a207b444953434f554e547d0d0a7b6c616e672e5368697070696e677d3a207b5348495050494e477d0d0a7b6c616e672e54617865737d3a207b54415845537d0d0a7b6c616e672e546f74616c7d3a207b544f54414c7d0d0a0d0a7b574f52445f435553544f4d45525f49447d3a207b435553544f4d455249447d0d0a7b6c616e672e4c6f67696e7d3a207b4c4f47494e7d0d0a7b6c616e672e4e616d657d3a207b4e414d457d0d0a7b6c616e672e452d6d61696c7d3a207b454d41494c7d0d0a7b6c616e672e54656c6570686f6e657d3a207b54454c4550484f4e457d0d0a0d0a0d0a7b6c616e672e42696c6c696e6720616464726573737d0d0a7b42494c4c494e475f46495253544e414d457d207b42494c4c494e475f4c4153544e414d457d0d0a7b42494c4c494e475f414444524553537d0d0a7b42494c4c494e475f434954597d207b42494c4c494e475f5a49507d2c207b42494c4c494e475f434f554e5452597d0d0a0d0a0d0a7b6c616e672e53686970696e6720616464726573737d0d0a7b5348495050494e475f46495253544e414d457d207b5348495050494e475f4c4153544e414d457d0d0a7b5348495050494e475f414444524553537d0d0a7b5348495050494e475f434954597d207b5348495050494e475f5a49507d2c207b5348495050494e475f434f554e5452597d, '1', '50', 'Order _resh_ {ORDER_ID} on {SITE_NAME}', '1'), ('signup_to_admin', 'New signup email to admin', 0x54686572652069732061206e657720726567697374726174696f6e206f6e207b534954455f4e414d457d3a0d0a0d0a7b6c616e672e4c6f67696e7d3a207b4c4f47494e7d0d0a7b6c616e672e4e616d657d3a207b4e414d457d0d0a7b6c616e672e452d6d61696c7d3a207b454d41494c7d0d0a7b6c616e672e54656c6570686f6e657d3a207b54454c4550484f4e457d0d0a7b6c616e672e416464726573737d3a207b414444524553537d0d0a7b6c616e672e436974797d3a207b434954597d0d0a7b6c616e672e436f756e7472797d3a207b434f554e5452597d0d0a7b6c616e672e446174657d3a207b444154457d, '1', '60', 'Registration on  {SITE_NAME}', '1'), ('signup_to_user', 'New signup email to user', 0x4869207b4e414d457d2c0d0a0d0a5468616e6b20796f7520666f7220796f757220726567697374726174696f6e206f6e207b534954455f4e414d457d0d0a506c656173652c20636c69636b206e657874206c696e6b3a207b434f4e4649524d4154494f4e5f4c494e4b7d20746f20636f6e6669726d20796f757220726567697374726174696f6e2e0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '70', 'Registration on  {SITE_NAME}', '1'), ('forgot_password', 'Forgot password email to user', 0x48656c6c6f207b4e414d457d2c0d0a0d0a54686520666f6c6c6f77696e672050617373776f72642077696c6c20616c6c6f7720796f7520746f2061636365737320796f7572207b534954455f4e414d457d2070726f66696c652e2020546865204c6f67696e20616e642050617373776f7264206172652074776f20636f64657320616e6420617265206e6f74207075626c69636c792073686f776e206f72206b6e6f776e2e0d0a0d0a596f7572206c6f67696e3a20207b4c4f47494e7d0d0a50617373776f72643a207b50415353574f52447d0d0a0d0a596f752063616e206368616e676520796f75722070617373776f726420616674657220796f75206c6f6720696e746f207b534954455f4e414d457d2e0d0a0d0a496620796f75206861766520616e792070726f626c656d7320616363657373696e6720796f75722070726f66696c652c20706c65617365206c6574207573206b6e6f772e0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '80', 'The password on {SITE_NAME}', '1'), ('tell_a_friend', 'Tell a friend email', 0x48656c6c6f207b4e414d45327d2c0d0a0d0a596f757220667269656e642c207b4e414d457d206174207b454d41494c7d2c207265636f6d6d656e6465642074686973206c696e6b3a207b55524c7d0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '90', 'Tell a friend about {SITE_NAME}', '1'), ('subscription_to_admin', 'New subscription email to admin', 0x7b535542534352495054494f4e5f44455441494c537d0d0a7b6c616e672e446174657d3a207b444154457d0d0a0d0a7b6c616e672e4c6f67696e7d3a207b4c4f47494e7d0d0a7b6c616e672e4e616d657d3a207b4e414d457d0d0a7b6c616e672e452d6d61696c7d3a207b454d41494c7d0d0a7b6c616e672e54656c6570686f6e657d3a207b54454c4550484f4e457d0d0a7b6c616e672e416464726573737d3a207b414444524553537d0d0a7b6c616e672e436f756e7472797d3a207b434f554e5452597d, '1', '53', 'Subscription order {SUBSCRIPTION} on {SITE_NAME}', '1'), ('subscription_to_user', 'New subscription email to user', 0x48656c6c6f207b4e414d457d2c0d0a0d0a5468616e6b7320666f7220627579696e67206120737562736372697074696f6e206f6e207b534954455f4e414d457d21200d0a0d0a7b535542534352495054494f4e5f44455441494c537d0d0a7b6c616e672e444154457d3a207b444154457d0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '52', 'Subscription order {SUBSCRIPTION} on {SITE_NAME}', '1'), ('credits_to_admin', 'New credits email to admin', 0x7b435245444954535f44455441494c537d0d0a7b6c616e672e446174657d3a207b444154457d0d0a0d0a7b6c616e672e4c6f67696e7d3a207b4c4f47494e7d0d0a7b6c616e672e4e616d657d3a207b4e414d457d0d0a7b6c616e672e452d6d61696c7d3a207b454d41494c7d0d0a7b6c616e672e54656c6570686f6e657d3a207b54454c4550484f4e457d0d0a7b6c616e672e416464726573737d3a207b414444524553537d0d0a7b6c616e672e436f756e7472797d3a207b434f554e5452597d, '1', '55', 'Credits order {CREDITS} on {SITE_NAME}', '1'), ('credits_to_user', 'New credits email to user', 0x48656c6c6f207b4e414d457d2c0d0a0d0a5468616e6b7320666f7220627579696e6720612063726564697473206f6e207b534954455f4e414d457d21200d0a0d0a7b435245444954535f44455441494c537d0d0a7b6c616e672e446174657d3a207b444154457d0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '54', 'Credits order {CREDITS} on {SITE_NAME}', '1'), ('signup_guest', 'New signup email to guest', 0x48692c0d0a0d0a5468616e6b20796f7520666f7220796f757220726567697374726174696f6e206f6e207b534954455f4e414d457d0d0a596f7572206c6f67696e3a207b4c4f47494e7d0d0a50617373776f72643a207b50415353574f52447d0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '75', 'Registration on  {SITE_NAME}', '1'), ('commission_to_seller', 'New commission to seller', 0x48656c6c6f207b4e414d457d2c0d0a0d0a54686572652069732061206e65772073616c65206174207b534954455f4e414d457d2e0d0a0d0a7b6c616e672e46696c657d3a207b46494c457d0d0a7b6c616e672e4f726465727d3a207b4f524445525f49447d0d0a7b6c616e672e4561726e696e677d3a207b4541524e494e477d0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '95', 'New sale on  {SITE_NAME}', '1'), ('commission_to_affiliate', 'New commission to affiliate', 0x48656c6c6f207b4e414d457d2c0d0a0d0a54686572652069732061206e65772073616c65206174207b534954455f4e414d457d2e0d0a7b6c616e672e4f726465727d3a207b4f524445525f49447d0d0a7b6c616e672e4561726e696e677d3a207b4541524e494e477d0d0a0d0a5468616e6b20796f752c0d0a7b534954455f4e414d457d, '1', '105', 'New sale on  {SITE_NAME}', '1'), ('exam_to_admin', 'New examination to admin', 0x546865207573657220746f6f6b20616e206578616d696e6174696f6e2e0d0a0d0a4c6f67696e3a207b4c4f47494e7d0d0a446174653a207b444154457d0d0a49443a207b49447d, '1', '110', 'New examination on {SITE_NAME}', '1'), ('exam_to_seller', 'New examination to seller', 0x48656c6c6f207b4e414d457d2c0d0a0d0a5468616e6b20796f7520666f722074616b696e6720616e206578616d696e6174696f6e206174207b534954455f4e414d457d210d0a0d0a526573756c743a207b524553554c547d0d0a446174653a207b444154457d0d0a436f6d6d656e74733a207b434f4d4d454e54537d0d0a0d0a4265737420726567617264732c0d0a7b534954455f4e414d457d, '1', '115', 'The examination on {SITE_NAME}', '1');
COMMIT;

-- ----------------------------
--  Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `payment` int(11) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `subtotal` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `shipping` float DEFAULT NULL,
  `tax` float DEFAULT NULL,
  `shipping_firstname` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `shipping_lastname` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `shipping_address` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `shipping_country` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `shipping_city` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `shipping_zip` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `shipped` int(11) DEFAULT NULL,
  `billing_firstname` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `billing_lastname` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `billing_address` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `billing_country` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `billing_city` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `billing_zip` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `shipping_method` int(11) DEFAULT NULL,
  `shipping_state` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_state` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `weight` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `orders`
-- ----------------------------
BEGIN;
INSERT INTO `orders` VALUES ('100', '6355', '2', '1', '0', '1321957627', '2', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('101', '6355', '9', '1', '0', '1321958964', '9', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('102', '6355', '1', '1', '0', '1326904633', '1', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('103', '6355', '1', '0', '0', '1329918423', '1', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('104', '6355', '1', '0', '0', '1329918756', '1', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('105', '6355', '1', '0', '0', '1329918963', '1', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('111', '6355', '125', '1', '0', '1340960823', '125', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('117', '6355', '2', '1', '0', '1344945805', '2', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('118', '6355', '3', '0', '0', '1345648241', '3', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('119', '6355', '1', '1', '0', '1345648660', '1', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('120', '6355', '2.5', '1', '0', '1348159108', '2.5', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('122', '6355', '2', '1', '0', '1352801009', '2', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('123', '6355', '1', '1', '0', '1355489344', '1', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('128', '4113', '1', '1', '0', '1359828517', '1', '0', '0', '0', 'Jeff', 'Ri_char_ds', '1st Rd.', 'USA', 'New York', '12345', '0', 'Jeff', 'Ri_char_ds', '1st Rd.', 'USA', 'New York', '12345', '0', null, null, null), ('129', '7435', '1', '1', '0', '1360580757', '1', '0', '0', '0', 'seller2', 'seller2', '', 'Austria', '', '', '0', 'seller2', 'seller2', '', 'Austria', '', '', '0', null, null, null), ('130', '6355', '4', '1', '0', '1362832950', '4', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', null, null, null), ('134', '6355', '18', '1', '0', '1365409882', '17', '0', '1', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '6', '', '', '0.202'), ('142', '6355', '9', '1', '0', '1366019007', '8', '0', '1', '0', 'John', 'Brown', 'test', 'United States', 'Los Angeles', 'test', '0', 'John', 'Brown', 'test', 'United States', 'Los Angeles', 'test', '6', '', '', '0.005'), ('146', '6355', '1', '1', '0', '1372075943', '1', '0', '0', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', '', '', '0'), ('147', '7403', '1', '1', '0', '1376066670', '1', '0', '0', '0', 'Bob', 'Smith', '', 'Austria', 'Wien', '', '0', 'Bob', 'Smith', '', 'Austria', 'Wien', '', '0', '', '', '0'), ('148', '6355', '1.1', '0', '0', '1376476285', '1', '0', '0', '0.1', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', '', '', '0'), ('149', '6355', '1.1', '0', '0', '1376577474', '1', '0', '0', '0.1', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', '', '', '0'), ('150', '6355', '1.1', '0', '0', '1376577555', '1', '0', '0', '0.1', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', '', '', '0'), ('151', '6355', '1.1', '0', '0', '1376577612', '1', '0', '0', '0.1', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', 'John', 'Brown', '', 'United States', 'Los Angeles', '', '0', '', '', '0'), ('152', '7440', '1.1', '0', '0', '1376838034', '1', '0', '0', '0.1', 'Guest', 'Guest', '', 'United States', '', '', '0', 'Guest', 'Guest', '', 'United States', '', '', '0', '', '', '0');
COMMIT;

-- ----------------------------
--  Table structure for `orders_content`
-- ----------------------------
DROP TABLE IF EXISTS `orders_content`;
CREATE TABLE `orders_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `prints` int(11) DEFAULT NULL,
  `option1_id` int(11) DEFAULT NULL,
  `option1_value` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `option2_id` int(11) DEFAULT NULL,
  `option2_value` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `option3_id` int(11) DEFAULT NULL,
  `option3_value` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`),
  KEY `id_parent_2` (`id_parent`),
  KEY `prints` (`prints`)
) ENGINE=InnoDB AUTO_INCREMENT=322 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `orders_content`
-- ----------------------------
BEGIN;
INSERT INTO `orders_content` VALUES ('249', '100', '1', '3778', '1', '0', '0', '', '0', '', '0', ''), ('250', '100', '1', '3787', '1', '0', '0', '', '0', '', '0', ''), ('251', '101', '3', '1094', '1', '0', '0', '', '0', '', '0', ''), ('252', '101', '1', '1098', '1', '0', '0', '', '0', '', '0', ''), ('253', '101', '5', '957', '1', '0', '0', '', '0', '', '0', ''), ('254', '102', '1', '3778', '1', '0', '0', '', '0', '', '0', ''), ('255', '103', '1', '3787', '1', '0', '0', '', '0', '', '0', ''), ('256', '104', '1', '3769', '1', '0', '0', '', '0', '', '0', ''), ('257', '105', '1', '3796', '1', '0', '0', '', '0', '', '0', ''), ('263', '111', '125', '3833', '1', '0', '0', '', '0', '', '0', ''), ('269', '117', '1', '3832', '1', '0', '0', '', '0', '', '0', ''), ('270', '117', '1', '3778', '1', '0', '0', '', '0', '', '0', ''), ('271', '118', '3', '3746', '1', '0', '0', '', '0', '', '0', ''), ('272', '119', '1', '3769', '1', '0', '0', '', '0', '', '0', ''), ('273', '120', '2.5', '3834', '1', '0', '0', '', '0', '', '0', ''), ('275', '122', '1', '3832', '1', '0', '0', '', '0', '', '0', ''), ('276', '122', '1', '3823', '1', '0', '0', '', '0', '', '0', ''), ('277', '123', '1', '3832', '1', '0', '0', '', '0', '', '0', ''), ('283', '128', '1', '3832', '1', '0', '0', '', '0', '', '0', ''), ('284', '129', '1', '3832', '1', '0', '0', '', '0', '', '0', ''), ('285', '130', '4', '3838', '1', '0', '0', '', '0', '', '0', ''), ('297', '134', '2', '8194', '1', '1', '1', 'Glossy', '6', 'Yes', '0', ''), ('298', '134', '11', '9111', '1', '1', '4', 'Black', '0', '', '0', ''), ('299', '134', '3', '3836', '1', '0', '0', '', '0', '', '0', ''), ('310', '142', '3', '8171', '2', '1', '1', 'Matte', '6', 'No', '0', ''), ('311', '142', '2', '8152', '1', '1', '1', 'Matte', '6', 'No', '0', ''), ('315', '146', '1', '3832', '1', '0', '0', '', '0', '', '0', ''), ('316', '147', '1', '3778', '1', '0', '0', '', '0', '', '0', ''), ('317', '148', '1', '3832', '1', '0', '0', '', '0', '', '0', ''), ('318', '149', '1', '3832', '1', '0', '0', '', '0', '', '0', ''), ('319', '150', '1', '3823', '1', '0', '0', '', '0', '', '0', ''), ('320', '151', '1', '3814', '1', '0', '0', '', '0', '', '0', ''), ('321', '152', '1', '3832', '1', '0', '0', '', '0', '', '0', '');
COMMIT;

-- ----------------------------
--  Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `priority` int(11) DEFAULT NULL,
  `link` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `siteinfo` tinyint(4) DEFAULT NULL,
  KEY `priority` (`priority`),
  KEY `id_parent` (`id_parent`),
  KEY `siteinfo` (`siteinfo`)
) ENGINE=InnoDB AUTO_INCREMENT=7499 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `pages`
-- ----------------------------
BEGIN;
INSERT INTO `pages` VALUES ('15', 'Sign Up', 0x3c703e20506c656173652c2066696c6c20696e2074686520666f726d2062656c6f773a3c2f703e, '8', 'signup', '/pages/sign-up.html', '0'), ('36', 'Forgot password', 0x3c703e506c656173652c20656e74727920796f757220656d61696c2e3c2f703e0d0a3c703e266e6273703b3c2f703e, '7', 'forgot', '/pages/forgot-password.html', '0'), ('4334', 'Terms and Conditions', 0x5465726d7320616e6420636f6e646974696f6e733c6272202f3e3c6272202f3e496e666f726d6174696f6e2e2e2e3c6272202f3e, '9', 'terms', '/pages/terms-and-conditions.html', '0'), ('4035', 'About', 0x3c703e7b696620656e676c6973687d3c2f703e0d0a3c703e546578742061626f757420796f757220636f6d70616e792e2e2e3c2f703e0d0a3c703e7b2f69667d207b6966206672656e63687d3c2f703e0d0a3c703e4c61206465736372697074696f6e20647520736974652e2e2e3c2f703e0d0a3c703e7b2f69667d3c2f703e0d0a3c703e266e6273703b266e6273703b3c2f703e0d0a3c703e266e6273703b3c2f703e, '1', '', '/pages/about.html', '1'), ('4036', 'Support', 0x546578742061626f757420737570706f72743c6272202f3e, '2', '', '/pages/support.html', '1'), ('4037', 'Privacy Policy', 0x546578742061626f757420706f6c696379, '3', null, '/pages/privacy-policy.html', '1'), ('4038', 'FAQ', 0x203c703e46415120706167653c2f703e, '4', null, '/pages/faq.html', '1'), ('4039', 'Contacts', 0x3c6272202f3e, '5', '/contacts/', '/pages/contacts.html', '1'), ('6288', 'Examination', 0x546f206265636f6d6520612073656c6c657220796f752073686f756c642075706c6f6164207365766572616c206578616d706c6573206f6620796f757220617274776f726b7320666f7220746865206578616d696e6174696f6e2e204f75722073746166662077696c6c2076696577207468652066696c657320616e642064656369646520696620746865792061726520617070726f70726961746520666f72206f7572207369746520616e642063616e20626520736f6c6420686572652e3c6272202f3e3c6272202f3e506c656173652075706c6f61642031302070686f746f732066697273742e203c6272202f3e, '10', 'examination', '/pages/examination.html', '0'), ('6345', 'Examination - Thank you', 0x5468616e6b20796f75212057652077696c6c20636865636b207468652066696c657320616e64206c657420796f75206b6e6f7720696620796f752063616e2073656c6c20796f757220617274776f726b73206f6e206f757220736974652e, '11', 'take_exam', '/pages/6345.html', '0'), ('6353', 'Buyer agreement', 0x54686973204d656d626572736869702041677265656d656e7420676f7665726e7320796f7572206d656d6265727368697020696e2074686520596f7572536974652e636f6d20636f6d6d756e6974792c20616c6c6f77696e6720796f752066756c6c2061636365737320746f20746865206d656d6265727368697020706f7274696f6e73206f6620746865207765622073697465206c6f63617465642061742028746865205c22536974655c22292e2054686973204d656d626572736869702041677265656d656e7420697320696e206164646974696f6e20746f20746865205465726d73206f6620557365206170706c696361626c6520746f2074686520536974652e205468652053697465206973206f7065726174656420627920596f7572536974652e3c6272202f3e3c6272202f3e41636365737320616e6420757365206f6620746865206d656d6265727368697020706f7274696f6e73206f66207468652053697465206172652070726f766964656420627920596f75725369746520746f20796f75206f6e20636f6e646974696f6e207468617420796f752061636365707420746865207465726d7320616e6420636f6e646974696f6e73206f662074686973204d656d626572736869702041677265656d656e7420616e6420746865205465726d73206f66205573652c20616e6420627920616363657373696e67206f72207573696e6720746865206d656d6265727368697020706f7274696f6e73206f662074686520536974652c20796f7520616772656520746f20746865207465726d7320616e6420636f6e646974696f6e73206f662074686973204d656d626572736869702041677265656d656e7420616e6420746865205465726d73206f66205573652e20496620796f7520646f206e6f7420616772656520746f2061636365707420616e642061626964652062792074686973204d656d626572736869702041677265656d656e7420616e6420746865205465726d73206f66205573652c20796f752073686f756c64206e6f7420616363657373206f722075736520746865206d656d6265727368697020706f7274696f6e73206f662074686520536974652e20496e20746865206576656e74206f6620616e7920696e636f6e73697374656e6379206265747765656e2074686973204d656d626572736869702041677265656d656e7420616e6420746865205465726d73206f66205573652c20746865207465726d73206f662074686973204d656d626572736869702041677265656d656e74207368616c6c20676f7665726e2e3c6272202f3e3c6272202f3e596f757253697465207265736572766573207468652072696768742c20696e206974732064697363726574696f6e2c20746f206368616e6765206f72206d6f6469667920616c6c206f7220616e792070617274206f662074686973204d656d626572736869702041677265656d656e7420617420616e792074696d652c2065666665637469766520696d6d6564696174656c792075706f6e206e6f74696365207075626c6973686564206f6e2074686520536974652e20596f757220636f6e74696e75656420757365206f6620746865206d656d6265727368697020706f7274696f6e73206f662074686520536974652061667465722073756368206e6f7469636520636f6e737469747574657320796f75722062696e64696e6720616363657074616e6365206f6620746865207465726d7320616e6420636f6e646974696f6e7320696e2074686973204d656d626572736869702041677265656d656e742c20696e636c7564696e6720616e79206368616e676573206f72206d6f64696669636174696f6e73206d61646520627920596f757253697465206173207065726d69747465642061626f76652e20496620617420616e792074696d6520746865207465726d7320616e6420636f6e646974696f6e73206f662074686973204d656d626572736869702041677265656d656e7420617265206e6f206c6f6e6765722061636365707461626c6520746f20796f752c20796f752073686f756c6420696d6d6564696174656c7920636561736520757365206f6620746865206d656d6265727368697020706f7274696f6e73206f662074686520536974652e3c7374726f6e673e3c6272202f3e3c6272202f3e3c2f7374726f6e673e, '12', 'buyer', '/pages/buyer-agreement.html', '0'), ('6354', 'Seller agreement', 0x54686973204d656d626572736869702041677265656d656e7420676f7665726e7320796f7572206d656d6265727368697020696e2074686520596f7572536974652e636f6d20636f6d6d756e6974792c20616c6c6f77696e6720796f752066756c6c2061636365737320746f20746865206d656d6265727368697020706f7274696f6e73206f6620746865207765622073697465206c6f63617465642061742028746865205c22536974655c22292e2054686973204d656d626572736869702041677265656d656e7420697320696e206164646974696f6e20746f20746865205465726d73206f6620557365206170706c696361626c6520746f2074686520536974652e205468652053697465206973206f7065726174656420627920596f7572536974652e3c6272202f3e3c6272202f3e41636365737320616e6420757365206f6620746865206d656d6265727368697020706f7274696f6e73206f66207468652053697465206172652070726f766964656420627920596f75725369746520746f20796f75206f6e20636f6e646974696f6e207468617420796f752061636365707420746865207465726d7320616e6420636f6e646974696f6e73206f662074686973204d656d626572736869702041677265656d656e7420616e6420746865205465726d73206f66205573652c20616e6420627920616363657373696e67206f72207573696e6720746865206d656d6265727368697020706f7274696f6e73206f662074686520536974652c20796f7520616772656520746f20746865207465726d7320616e6420636f6e646974696f6e73206f662074686973204d656d626572736869702041677265656d656e7420616e6420746865205465726d73206f66205573652e20496620796f7520646f206e6f7420616772656520746f2061636365707420616e642061626964652062792074686973204d656d626572736869702041677265656d656e7420616e6420746865205465726d73206f66205573652c20796f752073686f756c64206e6f7420616363657373206f722075736520746865206d656d6265727368697020706f7274696f6e73206f662074686520536974652e20496e20746865206576656e74206f6620616e7920696e636f6e73697374656e6379206265747765656e2074686973204d656d626572736869702041677265656d656e7420616e6420746865205465726d73206f66205573652c20746865207465726d73206f662074686973204d656d626572736869702041677265656d656e74207368616c6c20676f7665726e2e3c6272202f3e3c6272202f3e596f757253697465207265736572766573207468652072696768742c20696e206974732064697363726574696f6e2c20746f206368616e6765206f72206d6f6469667920616c6c206f7220616e792070617274206f662074686973204d656d626572736869702041677265656d656e7420617420616e792074696d652c2065666665637469766520696d6d6564696174656c792075706f6e206e6f74696365207075626c6973686564206f6e2074686520536974652e20596f757220636f6e74696e75656420757365206f6620746865206d656d6265727368697020706f7274696f6e73206f662074686520536974652061667465722073756368206e6f7469636520636f6e737469747574657320796f75722062696e64696e6720616363657074616e6365206f6620746865207465726d7320616e6420636f6e646974696f6e7320696e2074686973204d656d626572736869702041677265656d656e742c20696e636c7564696e6720616e79206368616e676573206f72206d6f64696669636174696f6e73206d61646520627920596f757253697465206173207065726d69747465642061626f76652e20496620617420616e792074696d6520746865207465726d7320616e6420636f6e646974696f6e73206f662074686973204d656d626572736869702041677265656d656e7420617265206e6f206c6f6e6765722061636365707461626c6520746f20796f752c20796f752073686f756c6420696d6d6564696174656c7920636561736520757365206f6620746865206d656d6265727368697020706f7274696f6e73206f662074686520536974652e3c6272202f3e3c6272202f3e, '13', 'seller', '/pages/seller-agreement.html', '0'), ('6359', 'Seller lecture', 0x54657874206f662073656c6c65725c2773206c6563747572652e2e2e2e, '14', 'lecture', '/pages/seller-lecture.html', '0'), ('7278', 'Affiliate agreement', 0x416666696c696174652061677265656d656e742e2e2e, '15', 'affiliate', '/pages/affiliate-agreement.html', '0'), ('7498', 'Customer agreement', 0x437573746f6d65722061677265656d656e7420666f7220746865206275796572732c2073656c6c65727320616e6420616666696c69617465732e2e2e3c6272202f3e, '16', 'common', '/pages/customer-agreement.html', '0');
COMMIT;

-- ----------------------------
--  Table structure for `payments`
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `ip` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `tnumber` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ptype` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `processor` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `user` (`user`),
  KEY `data` (`data`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=7406 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `payments`
-- ----------------------------
BEGIN;
INSERT INTO `payments` VALUES ('4433', 'demo', '1200926818', '43', '127.0.0.1', 'erffeofr35345vdfvfdvd', 'credits', '1', 'paypal'), ('7381', '', '1312622635', '84', '127.0.0.1', '53S120574Y267135A', 'order', '85', 'paypal'), ('7382', '', '1312622983', '18.5', '127.0.0.1', '5VA13519JY9574415', 'order', '86', 'paypal'), ('7383', 'tester', '1320058921', '10', '127.0.0.1', '', 'subscription', '16', 'moneyorder'), ('7384', 'tester', '1320059154', '10', '127.0.0.1', '', 'subscription', '18', 'moneyorder'), ('7385', 'tester', '1320062812', '8.1', '127.0.0.1', '', 'credits', '63', 'moneyorder'), ('7386', 'buyer', '1321959094', '10', '127.0.0.1', '', 'subscription', '21', 'moneyorder'), ('7387', 'buyer', '1337343686', '0', '127.0.0.1', 'sdfew', 'credits', '98', 'fortumo'), ('7388', 'buyer', '1337343807', '0', '127.0.0.1', 'sdfew', 'credits', '99', 'fortumo'), ('7389', 'buyer', '1340866265', '1.15', '127.0.0.1', '', 'credits', '123', 'moneyorder'), ('7390', 'buyer', '1340866380', '1', '127.0.0.1', '', 'credits', '124', 'moneyorder'), ('7391', 'buyer', '1340866427', '10', '127.0.0.1', '', 'subscription', '24', 'moneyorder'), ('7392', 'buyer', '1340866533', '11.5', '127.0.0.1', '', 'subscription', '25', 'moneyorder'), ('7393', 'buyer', '1340866818', '1.15', '127.0.0.1', '', 'order', '109', 'moneyorder'), ('7394', 'buyer', '1340866900', '1', '127.0.0.1', '', 'order', '110', 'moneyorder'), ('7395', 'guest7433', '1348340539', '1', '127.0.0.1', '', 'order', '121', 'moneyorder'), ('7396', 'buyer', '1365838033', '1', '127.0.0.1', '', 'credits', '197', 'stripe'), ('7397', 'buyer', '1365838197', '1', '127.0.0.1', '', 'credits', '198', 'stripe'), ('7398', 'buyer', '1365838297', '1', '127.0.0.1', '', 'credits', '199', 'stripe'), ('7399', 'buyer', '1365861812', '1.1', '127.0.0.1', '', 'order', '137', 'moneyorder'), ('7400', 'buyer', '1365862003', '1.1', '127.0.0.1', '', 'credits', '200', 'moneyorder'), ('7401', 'buyer', '1365862041', '11', '127.0.0.1', '', 'subscription', '27', 'moneyorder'), ('7402', 'buyer', '1365932648', '1.1', '127.0.0.1', '', 'credits', '201', 'moneyorder'), ('7403', 'buyer', '1365932698', '11', '127.0.0.1', '', 'subscription', '28', 'moneyorder'), ('7404', 'buyer', '1365934260', '1.1', '127.0.0.1', '', 'order', '141', 'moneyorder'), ('7405', 'guest7440', '1376838035', '1.1', '127.0.0.1', '', 'order', '152', 'moneyorder');
COMMIT;

-- ----------------------------
--  Table structure for `payout`
-- ----------------------------
DROP TABLE IF EXISTS `payout`;
CREATE TABLE `payout` (
  `title` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `svalue` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `payout`
-- ----------------------------
BEGIN;
INSERT INTO `payout` VALUES ('Paypal account', '1', 'paypal'), ('MoneyBookers account', '1', 'moneybookers'), ('Dwolla account', '1', 'dwolla'), ('QIWI account', '1', 'qiwi'), ('Webmoney account', '1', 'webmoney'), ('Bank account', '1', 'bank');
COMMIT;

-- ----------------------------
--  Table structure for `payout_price`
-- ----------------------------
DROP TABLE IF EXISTS `payout_price`;
CREATE TABLE `payout_price` (
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `payout_price`
-- ----------------------------
BEGIN;
INSERT INTO `payout_price` VALUES ('1 Credit', '1');
COMMIT;

-- ----------------------------
--  Table structure for `people`
-- ----------------------------
DROP TABLE IF EXISTS `people`;
CREATE TABLE `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `login` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `people`
-- ----------------------------
BEGIN;
INSERT INTO `people` VALUES ('1', '0', 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'Administrator');
COMMIT;

-- ----------------------------
--  Table structure for `people_access`
-- ----------------------------
DROP TABLE IF EXISTS `people_access`;
CREATE TABLE `people_access` (
  `user` int(11) DEFAULT NULL,
  `accessdate` int(11) DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  KEY `user` (`user`),
  KEY `accessdate` (`accessdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `people_access`
-- ----------------------------
BEGIN;
INSERT INTO `people_access` VALUES ('1', '1440504061', '::1');
COMMIT;

-- ----------------------------
--  Table structure for `people_rights`
-- ----------------------------
DROP TABLE IF EXISTS `people_rights`;
CREATE TABLE `people_rights` (
  `user` int(11) DEFAULT NULL,
  `user_rights` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `user` (`user`),
  KEY `user_rights` (`user_rights`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `people_rights`
-- ----------------------------
BEGIN;
INSERT INTO `people_rights` VALUES ('1', 'affiliates_settings'), ('1', 'affiliates_payout'), ('1', 'affiliates_commission'), ('1', 'affiliates_stats'), ('1', 'pages_news'), ('1', 'pages_textpages'), ('1', 'templates_home'), ('1', 'templates_caching'), ('1', 'templates_templates'), ('1', 'templates_skins'), ('1', 'settings_phpini'), ('1', 'settings_audio'), ('1', 'settings_ffmpeg'), ('1', 'settings_video'), ('1', 'settings_productsoptions'), ('1', 'settings_previews'), ('1', 'settings_prints'), ('1', 'settings_models'), ('1', 'settings_creditstypes'), ('1', 'settings_subscription'), ('1', 'settings_sellercategories'), ('1', 'settings_content_types'), ('1', 'settings_networks'), ('1', 'settings_signup'), ('1', 'settings_payout'), ('1', 'settings_couponstypes'), ('1', 'settings_shipping'), ('1', 'settings_taxes'), ('1', 'settings_licenses'), ('1', 'settings_prices'), ('1', 'settings_currency'), ('1', 'settings_payments'), ('1', 'settings_languages'), ('1', 'settings_watermark'), ('1', 'settings_storage'), ('1', 'settings_site'), ('1', 'users_blockedip'), ('1', 'users_password'), ('1', 'users_administrators'), ('1', 'users_blogs'), ('1', 'users_testimonials'), ('1', 'users_newsletter'), ('1', 'users_contacts'), ('1', 'users_messages'), ('1', 'users_notifications'), ('1', 'users_customers'), ('1', 'catalog_lightboxes'), ('1', 'orders_coupons'), ('1', 'orders_commission'), ('1', 'orders_carts'), ('1', 'catalog_categories'), ('1', 'catalog_catalog'), ('1', 'catalog_bulkupload'), ('1', 'catalog_upload'), ('1', 'catalog_exam'), ('1', 'settings_pwinty'), ('1', 'catalog_comments'), ('1', 'catalog_search'), ('1', 'orders_subscription'), ('1', 'orders_credits'), ('1', 'orders_orders');
COMMIT;

-- ----------------------------
--  Table structure for `photos`
-- ----------------------------
DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos` (
  `id_parent` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `folder` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `keywords` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `author` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `viewed` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `content_type` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `free` tinyint(1) DEFAULT NULL,
  `orientation` tinyint(1) DEFAULT NULL,
  `color` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `downloaded` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `model` int(10) DEFAULT NULL,
  `server1` int(10) DEFAULT NULL,
  `server2` int(11) DEFAULT NULL,
  `server3` int(1) DEFAULT NULL,
  `examination` tinyint(1) DEFAULT NULL,
  `category2` int(11) DEFAULT NULL,
  `category3` int(11) DEFAULT NULL,
  `google_x` double DEFAULT NULL,
  `google_y` double DEFAULT NULL,
  `refuse_reason` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `editorial` tinyint(1) DEFAULT NULL,
  `adult` tinyint(4) DEFAULT NULL,
  KEY `data` (`data`),
  KEY `title` (`title`),
  KEY `published` (`published`),
  KEY `viewed` (`viewed`),
  KEY `featured` (`featured`),
  KEY `downloaded` (`downloaded`),
  KEY `free` (`free`),
  KEY `color` (`color`),
  KEY `orientation` (`orientation`),
  KEY `watermark` (`watermark`),
  KEY `userid` (`userid`),
  KEY `author` (`author`),
  KEY `id_parent` (`id_parent`),
  KEY `rating` (`rating`),
  KEY `model` (`model`),
  KEY `examination` (`examination`),
  KEY `category2` (`category2`),
  KEY `category3` (`category3`),
  KEY `google_x` (`google_x`),
  KEY `google_y` (`google_y`),
  KEY `server1` (`server1`),
  KEY `server2` (`server2`),
  KEY `editorial` (`editorial`),
  KEY `adult` (`adult`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `photos`
-- ----------------------------
BEGIN;
INSERT INTO `photos` VALUES ('7464', 'Arrogant Cat', '1320587962', '1', 0x5669626f7267, '7464', '1', 'cat', 'siteowner', '124', '0', '5', 'Common', '0', '0', 'blue', '3', null, '0', '2', '0', null, '0', '7460', '5', '60.7133428316', '28.7309646606', null, '/stock-photo/arrogant-cat-7464.html', '1', '0'), ('7465', 'Snow Garden', '1320588473', '1', 0x507573686b696e2e2054736172736b6f79652073656c6f, '7465', '0', '', 'siteowner', '111', '0', '5', 'Common', '0', '0', 'blue', '6', null, '0', '2', '0', null, '0', '5', '5', '59.7132226504', '30.4008865356', null, '/stock-photo/snow-garden-7465.html', '0', '0'), ('7466', 'Fortress', '1320588580', '1', 0x56656c696b6979204e6f76676f726f64, '7466', '0', '', 'siteowner', '19', '0', '5', 'Common', '0', '0', 'green', '3', null, '0', '2', '0', null, '0', '5', '5', '58.5415657276', '31.2856292725', null, '/stock-photo/fortress-7466.html', '0', '0'), ('7467', 'Mill', '1320588615', '1', 0x56656c696b6979204e6f76676f726f642e204d757365756d205c2671756f743b5669746f736c6176697473795c2671756f743b, '7467', '0', '', 'siteowner', '60', '0', '5', 'Common', '0', '0', 'cian', '1', null, '0', '2', '0', null, '0', '5', '5', '58.4868752799', '31.2770462036', null, '/stock-photo/mill-7467.html', '0', '0'), ('7468', 'Mew', '1320588639', '1', 0x5669626f72672e205061726b205c2671756f743b4d6f6e205265706f5c2671756f743b, '7468', '0', 'mew', 'siteowner', '43', '0', '5', 'Premium', '0', '0', 'cian', '3', null, '0', '2', '0', null, '0', '7473', '5', '60.7269436111', '28.8061523438', null, '/stock-photo/mew-7468.html', '0', '0'), ('7469', 'Mews', '1320588698', '1', 0x4b697a68692e204f6e656761206c616b65, '7469', '0', 'mew', 'siteowner', '18', '0', '5', 'Common', '0', '1', 'blue', '1', null, '0', '2', '0', null, '0', '7473', '5', '62.3343099292', '34.8486328125', null, '/stock-photo/mews-7469.html', '0', '0'), ('7470', 'Monastery ', '1320588729', '1', 0x4576656e696e6720676c6f772e204b6972696c6c6f76, '7470', '0', '', 'siteowner', '58', '0', '5', 'Common', '0', '0', 'red', '1', null, '0', '2', '0', null, '0', '5', '5', '59.8448148597', '38.3752441406', null, '/stock-photo/monastery-7470.html', '0', '0'), ('7471', 'Sewer manhole', '1320588782', '1', 0x4b696576, '7471', '1', '', 'siteowner', '340', '0', '5', 'Common', '0', '0', 'red', '14', null, '0', '2', '0', null, '0', '7472', '5', '0', '0', null, '/stock-photo/sewer-manhole-7471.html', '0', '0'), ('7451', 'Venezia 1', '1320586111', '1', 0x506f6e7465206469205269616c746f, '7451', '0', 'bridge,city,venezia', 'siteowner', '28', '0', '5', 'Common', '1', '0', 'magenta', '3', null, '0', '2', '0', null, '0', '5', '5', '45.4380321349', '12.3359942436', null, '/stock-photo/venezia-1-7451.html', '0', '0'), ('7452', 'Venezia 2', '1320586378', '1', 0x526f6f6673, '7452', '0', 'roofs,venezia', 'siteowner', '6', '0', '5', 'Common', '0', '0', 'yellow', '0', null, '0', '2', '0', null, '0', '5', '5', '45.4345991655', '12.3408651352', null, '/stock-photo/venezia-2-7452.html', '0', '0'), ('7453', 'Venezia 3', '1320586554', '1', 0x43616e616c204772616e64652e2053616e2047696f7267696f204d616767696f7265, '7453', '0', '', 'siteowner', '2', '0', '5', 'Common', '0', '0', 'cian', '0', null, '0', '2', '0', null, '0', '5', '5', '45.4335451418', '12.3405647278', null, '/stock-photo/venezia-3-7453.html', '0', '0'), ('7454', 'Venezia 4', '1320586687', '1', 0x47616c6c6579, '7454', '0', 'canals,venezia', 'siteowner', '3', '0', '5', 'Common', '0', '1', 'blue', '0', null, '0', '2', '0', null, '0', '5', '5', '45.4456802804', '12.3276257515', null, '/stock-photo/venezia-4-7454.html', '0', '0'), ('7455', 'Venezia 5', '1320586800', '1', 0x436f757274, '7455', '0', 'venezia', 'siteowner', '3', '0', '5', 'Common', '0', '1', 'red', '0', null, '0', '2', '0', null, '0', '5', '5', '45.4341022711', '12.3450493813', null, '/stock-photo/venezia-5-7455.html', '0', '0'), ('7456', 'Venezia 6', '1320586931', '1', 0x4772616e642063616e616c, '7456', '0', 'venezia,evening glow', 'siteowner', '3', '0', '5', 'Common', '0', '0', 'red', '0', null, '0', '2', '0', null, '0', '5', '5', '45.4336656027', '12.3461008072', null, '/stock-photo/venezia-6-7456.html', '0', '0'), ('7457', 'Firenze 1', '1320587290', '1', 0x4c612043617474656472616c652064692053616e7461204d617269612064656c2046696f7265, '7457', '0', 'firenze', 'siteowner', '15', '0', '5', 'Common', '0', '1', 'yellow', '0', null, '0', '2', '0', null, '0', '5', '5', '43.7727827255', '11.2555575371', null, '/stock-photo/firenze-1-7457.html', '0', '0'), ('7458', 'Firenze 2', '1320587417', '1', 0x4576656e696e6720676c6f772e20506f6e7465205665636368696f, '7458', '1', 'firenze,bridge', 'siteowner', '21', '0', '5', 'Common', '0', '0', 'red', '0', null, '0', '2', '0', null, '0', '5', '5', '43.7700711532', '11.247253418', null, '/stock-photo/firenze-2-7458.html', '0', '0'), ('7459', 'Firenze 3', '1320587482', '1', 0x506f6e7465205665636368696f, '7459', '0', 'firenze,bridge', 'siteowner', '7', '0', '5', 'Common', '1', '0', 'yellow', '3', null, '0', '2', '0', null, '0', '5', '5', '43.7701486284', '11.2457513809', null, '/stock-photo/firenze-3-7459.html', '0', '0'), ('7461', 'Small Cat', '1320587830', '1', 0x5361696e742d50657465727362757267, '7461', '1', 'cat,animal', 'siteowner', '20', '0', '5', 'Common', '0', '0', 'green', '1', null, '0', '2', '0', null, '0', '5', '5', '59.916418919', '30.3524780273', null, '/stock-photo/small-cat-7461.html', '1', '0'), ('7462', 'Thin Cat', '1320587873', '1', 0x526f6d612e205069617a7a6120417267656e74696e61, '7462', '0', 'cat,animal', 'siteowner', '2', '0', '5', 'Common', '1', '1', 'red', '0', null, '0', '2', '0', null, '0', '7460', '5', '41.896655134', '12.4740314484', null, '/stock-photo/thin-cat-7462.html', '0', '0'), ('7463', 'Cat', '1320587930', '1', 0x526f6d612e205069617a7a6120417267656e74696e61, '7463', '0', 'cat', 'siteowner', '7', '0', '5', 'Common', '0', '0', 'red', '0', null, '0', '2', '0', null, '0', '7460', '5', '41.896655134', '12.4740314484', null, '/stock-photo/cat-7463.html', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `prints`
-- ----------------------------
DROP TABLE IF EXISTS `prints`;
CREATE TABLE `prints` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `price` float DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `option1` int(11) DEFAULT NULL,
  `option2` int(11) DEFAULT NULL,
  `option3` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=4135 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `prints`
-- ----------------------------
BEGIN;
INSERT INTO `prints` VALUES ('4125', 'Print 4x6', '', '1', '3', '0.001', '1', '6', '0'), ('4126', 'Print 5x7', '', '2', '4', '0.002', '1', '6', '0'), ('4127', 'Print 8x10', '', '3', '5', '0.003', '1', '6', '0'), ('4128', 'Print 11x14', '', '4', '6', '0.004', '1', '6', '0'), ('4133', 'Mug', '', '11', '7', '0.2', '4', '0', '0'), ('4134', 'T-shirt', '', '10', '8', '0.2', '3', '2', '0');
COMMIT;

-- ----------------------------
--  Table structure for `prints_items`
-- ----------------------------
DROP TABLE IF EXISTS `prints_items`;
CREATE TABLE `prints_items` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `itemid` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `printsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_parent`),
  UNIQUE KEY `id_parent_2` (`id_parent`),
  KEY `id_parent` (`id_parent`),
  KEY `itemid` (`itemid`),
  KEY `priority` (`priority`),
  KEY `printsid` (`printsid`)
) ENGINE=InnoDB AUTO_INCREMENT=9113 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `prints_items`
-- ----------------------------
BEGIN;
INSERT INTO `prints_items` VALUES ('7383', 'Matte 8x10', '1', '4839', '3', '4125'), ('7384', 'Glossy 8x10', '2', '4839', '4', '4126'), ('7385', 'Matte 5x7', '3', '4839', '5', '4127'), ('7386', 'Glossy 5x7', '4', '4839', '6', '4128'), ('7389', 'Matte 8x10', '1', '7011', '3', '4125'), ('7390', 'Glossy 8x10', '2', '7011', '4', '4126'), ('7391', 'Matte 5x7', '3', '7011', '5', '4127'), ('7392', 'Glossy 5x7', '4', '7011', '6', '4128'), ('8079', 'Print 4x6', '1', '7451', '3', '4125'), ('8080', 'Print 5x7', '2', '7451', '4', '4126'), ('8081', 'Print 8x10', '3', '7451', '5', '4127'), ('8082', 'Print 11x14', '4', '7451', '6', '4128'), ('8085', 'Print 4x6', '1', '7452', '3', '4125'), ('8086', 'Print 5x7', '2', '7452', '4', '4126'), ('8087', 'Print 8x10', '3', '7452', '5', '4127'), ('8088', 'Print 11x14', '4', '7452', '6', '4128'), ('8091', 'Print 4x6', '1', '7453', '3', '4125'), ('8092', 'Print 5x7', '2', '7453', '4', '4126'), ('8093', 'Print 8x10', '3', '7453', '5', '4127'), ('8094', 'Print 11x14', '4', '7453', '6', '4128'), ('8097', 'Print 4x6', '1', '7454', '3', '4125'), ('8098', 'Print 5x7', '2', '7454', '4', '4126'), ('8099', 'Print 8x10', '3', '7454', '5', '4127'), ('8100', 'Print 11x14', '4', '7454', '6', '4128'), ('8103', 'Print 4x6', '1', '7455', '3', '4125'), ('8104', 'Print 5x7', '2', '7455', '4', '4126'), ('8105', 'Print 8x10', '3', '7455', '5', '4127'), ('8106', 'Print 11x14', '4', '7455', '6', '4128'), ('8109', 'Print 4x6', '1', '7456', '3', '4125'), ('8110', 'Print 5x7', '2', '7456', '4', '4126'), ('8111', 'Print 8x10', '3', '7456', '5', '4127'), ('8112', 'Print 11x14', '4', '7456', '6', '4128'), ('8115', 'Print 4x6', '1', '7457', '3', '4125'), ('8116', 'Print 5x7', '2', '7457', '4', '4126'), ('8117', 'Print 8x10', '3', '7457', '5', '4127'), ('8118', 'Print 11x14', '4', '7457', '6', '4128'), ('8121', 'Print 4x6', '1', '7458', '3', '4125'), ('8122', 'Print 5x7', '2', '7458', '4', '4126'), ('8123', 'Print 8x10', '3', '7458', '5', '4127'), ('8124', 'Print 11x14', '4', '7458', '6', '4128'), ('8127', 'Print 4x6', '1', '7459', '3', '4125'), ('8128', 'Print 5x7', '2', '7459', '4', '4126'), ('8129', 'Print 8x10', '3', '7459', '5', '4127'), ('8130', 'Print 11x14', '4', '7459', '6', '4128'), ('8133', 'Print 4x6', '1', '7461', '3', '4125'), ('8134', 'Print 5x7', '2', '7461', '4', '4126'), ('8135', 'Print 8x10', '3', '7461', '5', '4127'), ('8136', 'Print 11x14', '4', '7461', '6', '4128'), ('8139', 'Print 4x6', '1', '7462', '3', '4125'), ('8140', 'Print 5x7', '2', '7462', '4', '4126'), ('8141', 'Print 8x10', '3', '7462', '5', '4127'), ('8142', 'Print 11x14', '4', '7462', '6', '4128'), ('8145', 'Print 4x6', '1', '7463', '3', '4125'), ('8146', 'Print 5x7', '2', '7463', '4', '4126'), ('8147', 'Print 8x10', '3', '7463', '5', '4127'), ('8148', 'Print 11x14', '4', '7463', '6', '4128'), ('8151', 'Print 4x6', '1', '7464', '3', '4125'), ('8152', 'Print 5x7', '2', '7464', '4', '4126'), ('8153', 'Print 8x10', '3', '7464', '5', '4127'), ('8154', 'Print 11x14', '4', '7464', '6', '4128'), ('8157', 'Print 4x6', '1', '7465', '3', '4125'), ('8158', 'Print 5x7', '2', '7465', '4', '4126'), ('8159', 'Print 8x10', '3', '7465', '5', '4127'), ('8160', 'Print 11x14', '4', '7465', '6', '4128'), ('8163', 'Print 4x6', '1', '7466', '3', '4125'), ('8164', 'Print 5x7', '2', '7466', '4', '4126'), ('8165', 'Print 8x10', '3', '7466', '5', '4127'), ('8166', 'Print 11x14', '4', '7466', '6', '4128'), ('8169', 'Print 4x6', '1', '7467', '3', '4125'), ('8170', 'Print 5x7', '2', '7467', '4', '4126'), ('8171', 'Print 8x10', '3', '7467', '5', '4127'), ('8172', 'Print 11x14', '4', '7467', '6', '4128'), ('8175', 'Print 4x6', '1', '7468', '3', '4125'), ('8176', 'Print 5x7', '2', '7468', '4', '4126'), ('8177', 'Print 8x10', '3', '7468', '5', '4127'), ('8178', 'Print 11x14', '4', '7468', '6', '4128'), ('8181', 'Print 4x6', '1', '7469', '3', '4125'), ('8182', 'Print 5x7', '2', '7469', '4', '4126'), ('8183', 'Print 8x10', '3', '7469', '5', '4127'), ('8184', 'Print 11x14', '4', '7469', '6', '4128'), ('8187', 'Print 4x6', '1', '7470', '3', '4125'), ('8188', 'Print 5x7', '2', '7470', '4', '4126'), ('8189', 'Print 8x10', '3', '7470', '5', '4127'), ('8190', 'Print 11x14', '4', '7470', '6', '4128'), ('8193', 'Print 4x6', '1', '7471', '3', '4125'), ('8194', 'Print 5x7', '2', '7471', '4', '4126'), ('8195', 'Print 8x10', '3', '7471', '5', '4127'), ('8196', 'Print 11x14', '4', '7471', '6', '4128'), ('8619', 'Matte 8x10', '1', '7587', '3', '4125'), ('8620', 'Glossy 8x10', '2', '7587', '4', '4126'), ('8621', 'Matte 5x7', '3', '7587', '5', '4127'), ('8622', 'Glossy 5x7', '4', '7587', '6', '4128'), ('8625', 'Matte 8x10', '1', '7588', '3', '4125'), ('8626', 'Glossy 8x10', '2', '7588', '4', '4126'), ('8627', 'Matte 5x7', '3', '7588', '5', '4127'), ('8628', 'Glossy 5x7', '4', '7588', '6', '4128'), ('8631', 'Matte 8x10', '1', '7589', '3', '4125'), ('8632', 'Glossy 8x10', '2', '7589', '4', '4126'), ('8633', 'Matte 5x7', '3', '7589', '5', '4127'), ('8634', 'Glossy 5x7', '4', '7589', '6', '4128'), ('8733', 'Matte 8x10', '1', '7176', '3', '4125'), ('8734', 'Glossy 8x10', '2', '7176', '4', '4126'), ('8735', 'Matte 5x7', '3', '7176', '5', '4127'), ('8736', 'Glossy 5x7', '4', '7176', '6', '4128'), ('8931', 'Matte 8x10', '1', '7670', '3', '4125'), ('8932', 'Glossy 8x10', '2', '7670', '4', '4126'), ('8933', 'Matte 5x7', '3', '7670', '5', '4127'), ('8934', 'Glossy 5x7', '4', '7670', '6', '4128'), ('8937', 'Matte 8x10', '1', '7671', '3', '4125'), ('8938', 'Glossy 8x10', '2', '7671', '4', '4126'), ('8939', 'Matte 5x7', '3', '7671', '5', '4127'), ('8940', 'Glossy 5x7', '4', '7671', '6', '4128'), ('8943', 'Matte 8x10', '1', '7672', '3', '4125'), ('8944', 'Glossy 8x10', '2', '7672', '4', '4126'), ('8945', 'Matte 5x7', '3', '7672', '5', '4127'), ('8946', 'Glossy 5x7', '4', '7672', '6', '4128'), ('8949', 'Matte 8x10', '1', '7673', '3', '4125'), ('8950', 'Glossy 8x10', '2', '7673', '4', '4126'), ('8951', 'Matte 5x7', '3', '7673', '5', '4127'), ('8952', 'Glossy 5x7', '4', '7673', '6', '4128'), ('9073', 'Mug', '11', '7451', '7', '4133'), ('9074', 'T-shirt', '10', '7451', '8', '4134'), ('9075', 'Mug', '11', '7452', '7', '4133'), ('9076', 'T-shirt', '10', '7452', '8', '4134'), ('9077', 'Mug', '11', '7453', '7', '4133'), ('9078', 'T-shirt', '10', '7453', '8', '4134'), ('9079', 'Mug', '11', '7454', '7', '4133'), ('9080', 'T-shirt', '10', '7454', '8', '4134'), ('9081', 'Mug', '11', '7455', '7', '4133'), ('9082', 'T-shirt', '10', '7455', '8', '4134'), ('9083', 'Mug', '11', '7456', '7', '4133'), ('9084', 'T-shirt', '10', '7456', '8', '4134'), ('9085', 'Mug', '11', '7457', '7', '4133'), ('9086', 'T-shirt', '10', '7457', '8', '4134'), ('9087', 'Mug', '11', '7458', '7', '4133'), ('9088', 'T-shirt', '10', '7458', '8', '4134'), ('9089', 'Mug', '11', '7459', '7', '4133'), ('9090', 'T-shirt', '10', '7459', '8', '4134'), ('9091', 'Mug', '11', '7461', '7', '4133'), ('9092', 'T-shirt', '10', '7461', '8', '4134'), ('9093', 'Mug', '11', '7462', '7', '4133'), ('9094', 'T-shirt', '10', '7462', '8', '4134'), ('9095', 'Mug', '11', '7463', '7', '4133'), ('9096', 'T-shirt', '10', '7463', '8', '4134'), ('9097', 'Mug', '11', '7464', '7', '4133'), ('9098', 'T-shirt', '10', '7464', '8', '4134'), ('9099', 'Mug', '11', '7465', '7', '4133'), ('9100', 'T-shirt', '10', '7465', '8', '4134'), ('9101', 'Mug', '11', '7466', '7', '4133'), ('9102', 'T-shirt', '10', '7466', '8', '4134'), ('9103', 'Mug', '11', '7467', '7', '4133'), ('9104', 'T-shirt', '10', '7467', '8', '4134'), ('9105', 'Mug', '11', '7468', '7', '4133'), ('9106', 'T-shirt', '10', '7468', '8', '4134'), ('9107', 'Mug', '11', '7469', '7', '4133'), ('9108', 'T-shirt', '10', '7469', '8', '4134'), ('9109', 'Mug', '11', '7470', '7', '4133'), ('9110', 'T-shirt', '10', '7470', '8', '4134'), ('9111', 'Mug', '11', '7471', '7', '4133'), ('9112', 'T-shirt', '10', '7471', '8', '4134');
COMMIT;

-- ----------------------------
--  Table structure for `products_options`
-- ----------------------------
DROP TABLE IF EXISTS `products_options`;
CREATE TABLE `products_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL,
  `required` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activ` (`activ`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `products_options`
-- ----------------------------
BEGIN;
INSERT INTO `products_options` VALUES ('1', 'Paper type', '_select_form', '1', '1'), ('2', 'Size of t-shirts', '_select_form', '1', '1'), ('3', 'Color of t-shirts', '_select_form', '1', '1'), ('4', 'Color of mugs', '_select_form', '1', '1'), ('6', 'Framed', 'radio', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `products_options_items`
-- ----------------------------
DROP TABLE IF EXISTS `products_options_items`;
CREATE TABLE `products_options_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `adjust` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `products_options_items`
-- ----------------------------
BEGIN;
INSERT INTO `products_options_items` VALUES ('25', '2', 'XS', '0', '1'), ('26', '2', 'S', '0', '1'), ('27', '2', 'M', '0', '1'), ('28', '2', 'L', '0', '1'), ('29', '2', 'XL', '0', '1'), ('30', '2', 'XXL', '0', '1'), ('31', '2', 'XXXL', '0', '1'), ('32', '3', 'White', '0', '1'), ('33', '3', 'Black', '0', '1'), ('34', '3', 'Green', '0', '1'), ('35', '3', 'Red', '0', '1'), ('46', '4', 'Black', '0', '1'), ('47', '4', 'Red', '0', '1'), ('50', '6', 'Yes', '0', '1'), ('51', '6', 'No', '0', '1'), ('52', '1', 'Matte', '0', '1'), ('53', '1', 'Glossy', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `pwinty`
-- ----------------------------
DROP TABLE IF EXISTS `pwinty`;
CREATE TABLE `pwinty` (
  `account` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `testmode` tinyint(4) DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `pwinty`
-- ----------------------------
BEGIN;
INSERT INTO `pwinty` VALUES ('test', 'test', '1', '120');
COMMIT;

-- ----------------------------
--  Table structure for `pwinty_orders`
-- ----------------------------
DROP TABLE IF EXISTS `pwinty_orders`;
CREATE TABLE `pwinty_orders` (
  `order_id` int(11) DEFAULT NULL,
  `pwinty_id` int(11) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Table structure for `pwinty_prints`
-- ----------------------------
DROP TABLE IF EXISTS `pwinty_prints`;
CREATE TABLE `pwinty_prints` (
  `print_id` int(11) DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  KEY `print_id` (`print_id`),
  KEY `activ` (`activ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `pwinty_prints`
-- ----------------------------
BEGIN;
INSERT INTO `pwinty_prints` VALUES ('4125', '1', '4x6'), ('4126', '1', '5x7'), ('4127', '1', '8x10'), ('4128', '1', '11x14'), ('4133', '0', ''), ('4134', '0', '');
COMMIT;

-- ----------------------------
--  Table structure for `reviews`
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `fromuser` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `itemid` int(11) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `fromuser` (`fromuser`),
  KEY `itemid` (`itemid`),
  KEY `data` (`data`)
) ENGINE=InnoDB AUTO_INCREMENT=7523 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `reviews`
-- ----------------------------
BEGIN;
INSERT INTO `reviews` VALUES ('7521', 'demo', 0x436c6173732121, '7176', '1349688395'), ('7520', 'buyer', 0x457863656c6c656e7421, '4817', '1337256503'), ('7522', 'demo', 0x576f7721, '7471', '1349712692');
COMMIT;

-- ----------------------------
--  Table structure for `search_history`
-- ----------------------------
DROP TABLE IF EXISTS `search_history`;
CREATE TABLE `search_history` (
  `zapros` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  KEY `zapros` (`zapros`),
  KEY `data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `search_history`
-- ----------------------------
BEGIN;
INSERT INTO `search_history` VALUES ('cat', '1374222608'), ('cat', '1375947570'), ('nature', '1375954857'), ('cat', '1376035980'), ('bird', '1376035984'), ('cat', '1376644050'), ('arrogant', '1376644058');
COMMIT;

-- ----------------------------
--  Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `svalue` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `setting_key` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `settings`
-- ----------------------------
BEGIN;
INSERT INTO `settings` VALUES ('1', 'Site name', 'Photo Video Store', '0', 'site_name', '10'), ('2', 'Admin email', 'sales@cmsaccount.com', '0', 'admin_email', '20'), ('5', 'Small thumb width', '120', '0', 'thumb_width', '85'), ('6', 'Big thumb width', '400', '0', 'thumb_width2', '95'), ('7', 'Lightbox photo', '', '1', 'lightbox_photo', '130'), ('8', 'Lightbox video', '', '1', 'lightbox_video', '140'), ('11', 'Date format', 'm/d/Y', '0', 'date_format', '40'), ('12', 'Items on the page', '10', '0', 'k_str', '80'), ('15', 'Items in the row', '5', '0', 'k_row', '80'), ('16', 'Lightbox video width', '300', '0', 'video_width', '140'), ('17', 'Lightbox video height', '225', '0', 'video_height', '150'), ('18', 'Left column width', '190', '0', 'left_width', '160'), ('19', 'Right column width', '1', '0', 'right_width', '170'), ('20', 'Items in news box', '2', '0', 'qnews', '190'), ('21', 'Items in best seller box', '1', '0', 'qbest', '200'), ('22', 'From', 'noreply@cmsaccount.com', '0', 'from_email', '30'), ('23', 'photographers', '', '1', 'userupload', '73'), ('24', 'allow photo types', 'jpg,jpeg,gif,png', '0', 'uploadphoto', '220'), ('25', 'allow video types', 'wmv,flv,avi,mod', '0', 'uploadvideo', '230'), ('26', 'user status as default', 'Silver', '0', 'userstatus', '240'), ('28', 'Upload video limit (Mb)', '250', '0', 'videolimit', '260'), ('29', 'Upload preview video limit', '10', '0', 'previewvideolimit', '270'), ('30', 'Upload photo limit (Mb)', '3', '0', 'photolimit', '255'), ('31', 'Avatar width', '25', '0', 'avatarwidth', '290'), ('32', 'User photo width', '140', '0', 'userphotowidth', '300'), ('33', 'Photo preupload folder', '/content/photopreupload/', '0', 'photopreupload', '310'), ('34', 'Video preupload folder', '/content/videopreupload/', '0', 'videopreupload', '320'), ('35', 'Audio preupload folder', '/content/audiopreupload/', '0', 'audiopreupload', '330'), ('36', 'small thumb height', '120', '0', 'thumb_height', '90'), ('37', 'big thumb height', '400', '0', 'thumb_height2', '100'), ('38', 'Allow photo', '', '1', 'allow_photo', '50'), ('39', 'Allow video', '', '1', 'allow_video', '53'), ('40', 'Allow audio', '', '1', 'allow_audio', '55'), ('41', 'Blog', '', '1', 'blog', '340'), ('42', 'Messages', '', '1', 'messages', '350'), ('43', 'Testimonials', '', '1', 'testimonials', '360'), ('44', 'Reviews', '', '1', 'reviews', '370'), ('45', 'Friends', '', '1', 'friends', '380'), ('46', 'prints', '', '1', 'prints', '64'), ('47', 'Allow audio types', 'mp3,wav', '0', 'uploadaudio', '235'), ('48', 'Upload audio limit (Mb)', '20', '0', 'audiolimit', '275'), ('49', 'Upload preview audio limit', '5', '0', 'previewaudiolimit', '276'), ('53', 'sell prints only', '', '0', 'printsonly', '66'), ('54', 'show watermark info', '', '1', 'watermarkinfo', '410'), ('55', 'Allow vector types', 'cdr,ai,eps,zip', '0', 'uploadvector', '237'), ('56', 'allow vector', '', '1', 'allow_vector', '60'), ('58', 'vector preupload folder', '/content/vectorpreupload/', '0', 'vectorpreupload', '335'), ('59', 'credits', '', '1', 'credits', '78'), ('60', 'download limit', '5', '0', 'download_limit', '430'), ('61', 'days till download expiration', '15', '0', 'download_expiration', '440'), ('63', 'Upload vector limit (Mb)', '20', '0', 'vectorlimit', '277'), ('64', 'Download sample', '', '1', 'download_sample', '450'), ('65', 'Check or Money Order', '', '1', 'moneyorder', '460'), ('66', 'Subscription', '', '1', 'subscription', '79'), ('67', 'Related items', '', '1', 'related_items', '480'), ('68', 'Content type as default', 'Common', '0', 'content_type', '242'), ('69', 'Related items quantity', '10', '0', 'related_items_quantity', '485'), ('70', 'Zoomer', '', '1', 'zoomer', '490'), ('71', 'user uploads premoderation', '', '1', 'moderation', '77'), ('72', 'prints for users', '', '1', 'prints_users', '65'), ('73', 'model property release', '', '1', 'model', '510'), ('74', 'Flash', '', '1', 'flash', '520'), ('75', 'Flash width', '400', '0', 'flash_width', '530'), ('76', 'Flash height', '290', '0', 'flash_height', '540'), ('77', 'Seller examination', '', '1', 'examination', '77'), ('78', 'Items in Bulk Upload', '10', '0', 'bulk_upload', '580'), ('79', 'show model property release', '', '0', 'show_model', '515'), ('80', 'Google Coordinates', '', '1', 'google_coordinates', '585'), ('81', 'Google map <a href=http://code.google.com/intl/en/apis/maps/signup.html target=blank>API</a>', '', '0', 'google_api', '590'), ('82', 'Show EXIF info', '', '1', 'exif', '595'), ('83', 'affiliates', '', '1', 'affiliates', '74'), ('84', 'Subscription only', '', '0', 'subscription_only', '79'), ('85', 'Common account for buyers,sellers and affiliates', '', '1', 'common_account', '77'), ('86', 'Google <a href=https://www.google.com/recaptcha/admin/create target=blank>Captcha', '', '0', 'google_captcha', '600'), ('87', 'Google Captcha Public Key', '6LcGbMoSAAAAAIStXBGMnRYldIefVK54EiIrCubq', '0', 'google_captcha_public', '605'), ('88', 'Google Captcha Private Key', '6LcGbMoSAAAAAHTLpzNeDfOzmo-YKZeIAvJwwx1w', '0', 'google_captcha_private', '610'), ('89', 'Login as guest', '', '1', 'site_guest', '77'), ('90', 'Meta keywords', 'photo store, video stock, php script', '0', 'meta_keywords', '15'), ('91', 'Meta description', 'Photo Video Store script allows you to sell photos online', '0', 'meta_description', '16'), ('92', 'Java Photo Uploader', '', '1', 'java_uploader', '620'), ('93', 'Flash Photo Uploader', '', '0', 'flash_uploader', '630'), ('94', 'Address', 'Your company name\r\n15 Noname St., 250\r\nLos Angeles 90210, USA\r\nTelephone: 1-675-234-56-78', '0', 'company_address', '17'), ('95', 'Show users as', 'name', '0', 'show_users_type', '244'), ('96', 'Sorting of catalog', 'date', '0', 'sorting_catalog', '81'), ('97', 'Simple Photo Uploader', '', '1', 'jquery_uploader', '631'), ('98', 'Seller may set prices', '', '1', 'seller_prices', '640'), ('99', 'Automatic language detection', '', '1', 'language_detection', '650'), ('100', 'Photo resolution in DPI', '300', '0', 'resolution_dpi', '83'), ('101', 'Telephone', '1-234-765-4967', '0', 'telephone', '18'), ('102', 'Adult content', '', '1', 'adult_content', '82'), ('103', 'Category preview', '200', '0', 'category_preview', '110'), ('104', 'Weight', 'lbs', '0', 'weight', '660'), ('108', 'CD weight', '0.01', '0', 'cd_weight', '670'), ('111', 'flow', '', '1', 'flow', '175'), ('112', 'flow by default', '', '1', 'flow_default', '176'), ('113', 'auto paging', '', '1', 'auto_paging', '177'), ('114', 'auto paging by default', '', '1', 'auto_paging_default', '178');
COMMIT;

-- ----------------------------
--  Table structure for `shipping`
-- ----------------------------
DROP TABLE IF EXISTS `shipping`;
CREATE TABLE `shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `shipping_time` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `activ` tinyint(4) DEFAULT NULL,
  `methods` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `methods_calculation` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `taxes` tinyint(4) DEFAULT NULL,
  `regions` tinyint(4) DEFAULT NULL,
  `weight_min` int(11) DEFAULT NULL,
  `weight_max` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activ` (`activ`),
  KEY `weight_max` (`weight_max`),
  KEY `weight_min` (`weight_min`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `shipping`
-- ----------------------------
BEGIN;
INSERT INTO `shipping` VALUES ('6', 'UPS', '3-5 days', '1', 'weight', 'currency', '0', '0', '0', '100'), ('8', 'DHL/Airborne', '2-3 days', '1', 'weight', 'currency', '1', '0', '0', '1000');
COMMIT;

-- ----------------------------
--  Table structure for `shipping_ranges`
-- ----------------------------
DROP TABLE IF EXISTS `shipping_ranges`;
CREATE TABLE `shipping_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `from_param` float DEFAULT NULL,
  `to_param` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`),
  KEY `to_param` (`to_param`),
  KEY `from_param` (`from_param`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `shipping_ranges`
-- ----------------------------
BEGIN;
INSERT INTO `shipping_ranges` VALUES ('39', '6', '1', '0', '1'), ('40', '6', '2', '1', '2'), ('41', '6', '3', '2', '3'), ('81', '8', '50', '0', '100'), ('82', '8', '75', '100', '1000000');
COMMIT;

-- ----------------------------
--  Table structure for `shipping_regions`
-- ----------------------------
DROP TABLE IF EXISTS `shipping_regions`;
CREATE TABLE `shipping_regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `country` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`),
  KEY `country` (`country`),
  KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Table structure for `sizes`
-- ----------------------------
DROP TABLE IF EXISTS `sizes`;
CREATE TABLE `sizes` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `size` int(11) DEFAULT NULL,
  `description` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `license` int(10) NOT NULL,
  KEY `license` (`license`),
  KEY `id_parent` (`id_parent`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=6894 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `sizes`
-- ----------------------------
BEGIN;
INSERT INTO `sizes` VALUES ('6892', '0', null, '125', '4', 'Electronic Items for Resale (unlimited run)', '4584'), ('6891', '0', null, '125', '3', 'Items for Resale (limited run)', '4584'), ('6890', '0', null, '125', '2', 'Unlimited Reproduction / Print Runs', '4584'), ('6889', '0', null, '75', '1', 'Multi-Seat (unlimited)', '4584'), ('6888', '0', null, '4', '4', 'Original size', '4583'), ('6887', '900', null, '3', '3', 'Large', '4583'), ('6885', '500', null, '1', '1', 'Small', '4583'), ('6886', '800', null, '2', '2', 'Medium', '4583'), ('6893', '0', null, '100', '5', 'Extended Legal Guarantee covers up to $250,000', '4584');
COMMIT;

-- ----------------------------
--  Table structure for `structure`
-- ----------------------------
DROP TABLE IF EXISTS `structure`;
CREATE TABLE `structure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `module_table` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `potential` int(11) DEFAULT NULL,
  `prioritet` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=7736 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `structure`
-- ----------------------------
BEGIN;
INSERT INTO `structure` VALUES ('1', '0', '0', 'Support', '1', '0'), ('2', '0', '0', 'Customers', '1', '0'), ('4', '0', '0', 'News', '1', '0'), ('5', '0', '0', 'Catalog', '1', '0'), ('7', '0', '0', 'Reporting', '1', '0'), ('15', '3908', '25', 'Sign Up', null, '0'), ('32', '0', '0', 'Payments', '1', '0'), ('36', '3908', '25', 'Forgot password', null, '0'), ('95', '7', '4', 'Stats', '-1', '0'), ('96', '7', '6', 'Blocked IP', '-1', '0'), ('97', '7', '7', 'Blocked Users', '-1', '0'), ('108', '2', '28', 'amkarik@mail.ru', null, '0'), ('3908', '0', '0', 'Text pages', '1', '0'), ('3913', '5', '34', 'Cities', null, '0'), ('3914', '5', '34', 'Movies', null, '0'), ('3918', '7474', '34', 'Animals', null, '0'), ('3923', '4', '26', 'First news', null, '0'), ('3924', '1', '33', 'User', null, '0'), ('3931', '2', '28', 'demo', null, '0'), ('3992', '0', '0', 'Coupons', null, '0'), ('3993', '3992', '0', 'Types of Coupons', '1', '2'), ('3994', '3992', '0', 'List of Current Coupons', '1', '1'), ('4012', '0', '0', 'Top navigation', '1', '0'), ('4013', '0', '0', 'Bottom navigation', '1', '0'), ('4014', '0', '0', 'Side boxes', '1', '0'), ('4016', '4012', '38', 'About', null, '0'), ('4017', '4012', '38', 'News', null, '0'), ('4018', '4012', '38', 'Contacts', null, '0'), ('4020', '4013', '38', 'About', null, '0'), ('4021', '4013', '38', 'News', null, '0'), ('4022', '4013', '38', 'Contacts', null, '0'), ('4023', '4014', '39', 'Categories', null, '0'), ('4026', '4014', '39', 'Shopping Cart', null, '0'), ('4028', '4014', '39', 'News', null, '0'), ('4029', '4014', '39', 'Member Area', null, '0'), ('4031', '4014', '39', 'Stock', null, '0'), ('4032', '4014', '39', 'Photographers', null, '0'), ('4033', '4014', '39', 'Advertisement', null, '0'), ('4034', '0', '0', 'Site info', '1', '0'), ('4035', '4034', '25', 'About', null, '0'), ('4036', '4034', '25', 'Support', null, '0'), ('4037', '4034', '25', 'Privacy Policy', null, '0'), ('4038', '4034', '25', 'FAQ', null, '0'), ('4039', '4034', '25', 'Contacts', null, '0'), ('4040', '4014', '39', 'Stat', null, '0'), ('4051', '0', '0', 'Customers categories', '1', '0'), ('4052', '4051', '40', 'Gold', null, '0'), ('4053', '4051', '40', 'Silver', null, '0'), ('4054', '4051', '40', 'Bronze', null, '0'), ('4096', '0', '0', 'Photo sizes', '1', '0'), ('4113', '2', '28', 'demo2', null, '0'), ('4121', '4014', '39', 'Search', null, '0'), ('4122', '0', '0', 'Prints types', '1', '0'), ('4123', '4122', '42', 'Framed', null, '0'), ('4124', '4122', '42', 'XL T-Shirt', null, '0'), ('4125', '4122', '42', 'Matte 8x10', null, '0'), ('4126', '4122', '42', 'Glossy 8x10', null, '0'), ('4127', '4122', '42', 'Matte 5x7', null, '0'), ('4128', '4122', '42', 'Glossy 5x7', null, '0'), ('4131', '2', '28', 'siteowner', null, '0'), ('4132', '2', '28', 'john', null, '0'), ('4133', '0', '0', 'Messages', '1', '0'), ('4142', '4133', '43', 'boby', null, '0'), ('4143', '4133', '43', 'john', null, '0'), ('4146', '4133', '43', 'demo', null, '0'), ('4147', '4133', '43', 'demo', null, '0'), ('4148', '4133', '43', 'boby', null, '0'), ('4149', '0', '0', 'Reviews', '1', '0'), ('4150', '0', '0', 'Testimonials', '1', '0'), ('4154', '4149', '44', 'demo', null, '0'), ('4155', '4149', '44', 'demo', null, '0'), ('4156', '4149', '44', 'demo', null, '0'), ('4159', '0', '0', 'Blog categories', '1', '0'), ('4160', '4159', '48', 'Photo', null, '0'), ('4161', '4149', '44', 'demo', null, '0'), ('4162', '4149', '44', 'demo', null, '0'), ('4163', '4159', '48', 'Video', null, '0'), ('4164', '4159', '48', '1', null, '0'), ('4165', '4159', '48', '', null, '0'), ('4166', '0', '0', 'Blog', '1', '0'), ('4167', '4166', '47', 'Hello world!', null, '0'), ('4170', '4166', '47', 'Hello world!', null, '0'), ('4174', '4166', '47', 'Second post', null, '0'), ('4175', '0', '0', 'Blog comments', '1', '0'), ('4196', '4150', '45', 'demo', null, '0'), ('4197', '4150', '45', 'demo', null, '0'), ('4198', '4150', '45', 'demo', null, '0'), ('4202', '4150', '45', 'siteowner', null, '0'), ('4203', '4150', '45', 'siteowner', null, '0'), ('4204', '4150', '45', 'demo', null, '0'), ('4205', '4150', '45', 'demo', null, '0'), ('4206', '4175', '49', 'siteowner', null, '0'), ('4207', '4175', '49', 'siteowner', null, '0'), ('4208', '4175', '49', 'siteowner', null, '0'), ('4209', '4166', '47', 'My first post', null, '0'), ('4210', '4175', '49', 'siteowner', null, '0'), ('4212', '4175', '49', 'demo', null, '0'), ('4213', '4175', '49', 'demo', null, '0'), ('4215', '4159', '48', 'Funny', null, '0'), ('4216', '4159', '48', 'Jokes', null, '0'), ('4223', '0', '0', 'Prints', '1', '0'), ('4334', '3908', '25', 'Terms and Conditions', null, '0'), ('4375', '5', '34', 'Sounds', null, '0'), ('4377', '5', '34', 'Illustrations', null, '0'), ('4379', '0', '0', 'Download links', '1', '0'), ('4380', '0', '0', 'Credits', '1', '0'), ('4419', '0', '0', 'Credits list', '1', '0'), ('4433', '32', '29', 'demo', null, '0'), ('4455', '0', '0', 'Subscription', '1', '0'), ('4456', '0', '0', 'Subscription list', '1', '0'), ('4457', '0', '0', 'Content type', '1', '0'), ('4475', '4014', '39', 'Subscription', null, '0'), ('4476', '4014', '39', 'Tag clouds', null, '0'), ('4580', '0', '0', 'License', '1', '0'), ('4583', '4580', '61', 'Standart', null, '0'), ('4584', '4580', '61', 'Extended', null, '0'), ('4740', '0', '0', 'Ads', '1', '0'), ('4741', '4740', '62', 'Google 468x60', null, '0'), ('4745', '4740', '62', 'Google 728x90', null, '0'), ('4776', '4133', '43', 'demo', null, '0'), ('4777', '4133', '43', 'demo2', null, '0'), ('4778', '4133', '43', 'siteowner', null, '0'), ('4779', '4133', '43', 'john', null, '0'), ('4795', '0', '0', 'Video types', '1', '0'), ('4796', '0', '0', 'Audio types', '1', '0'), ('4797', '0', '0', 'Vector types', '1', '0'), ('4798', '4795', '63', 'QuickTime', null, '0'), ('4799', '4795', '63', 'AVI', null, '0'), ('4800', '4795', '63', 'WMV', null, '0'), ('4801', '4796', '64', 'MP3', null, '0'), ('4802', '4796', '64', 'WAV', null, '0'), ('4804', '4797', '65', 'ZIP', null, '0'), ('4805', '4795', '63', 'FLV', null, '0'), ('4839', '4377', '53', 'Vector Illustration', null, '0'), ('5175', '2', '28', '1111111', null, '0'), ('5892', '4795', '63', 'Shipped CD', null, '0'), ('5893', '4796', '64', 'Shipped CD', null, '0'), ('5894', '4797', '65', 'Shipped CD', null, '0'), ('5995', '0', '0', 'Model Property Release', '1', '0'), ('6205', '5995', '67', 'sdfsd', null, '0'), ('6251', '5', '34', 'Flash', null, '0'), ('6288', '3908', '25', 'Examination', null, '0'), ('6345', '3908', '25', 'Examination - Thank you', null, '0'), ('6353', '3908', '25', 'Buyer agreement', null, '0'), ('6354', '3908', '25', 'Seller agreement', null, '0'), ('6355', '2', '28', 'buyer', null, '0'), ('6359', '3908', '25', 'Seller lecture', null, '0'), ('6360', '2', '28', 'seller', null, '0'), ('7176', '6251', '53', 'Flash example', null, '0'), ('7217', '3918', '52', '???Â°', null, '0'), ('7278', '3908', '25', 'Affiliate agreement', null, '0'), ('7283', '4175', '49', 'affiliate', null, '0'), ('7292', '2', '28', 'affiliate', null, '0'), ('7381', '32', '29', 'Order _resh_85', null, '0'), ('7382', '32', '29', 'Order _resh_86', null, '0'), ('7403', '2', '28', 'common', null, '0'), ('7449', '3913', '34', 'Venezia', null, '0'), ('7450', '3913', '34', 'Firenze', null, '0'), ('7451', '7449', '30', 'Venezia 1', null, '0'), ('7452', '7449', '30', 'Venezia 2', null, '0'), ('7453', '7449', '30', 'Venezia 3', null, '0'), ('7454', '7449', '30', 'Venezia 4', null, '0'), ('7455', '7449', '30', 'Venezia 5', null, '0'), ('7456', '7449', '30', 'Venezia 6', null, '0'), ('7457', '7450', '30', 'Firenze 1', null, '0'), ('7458', '7450', '30', 'Firenze 2', null, '0'), ('7459', '7450', '30', 'Firenze 3', null, '0'), ('7460', '3918', '34', 'Cats', null, '0'), ('7461', '7460', '30', 'Small Cat', null, '0'), ('7462', '3918', '30', 'Thin Cat', null, '0'), ('7463', '3918', '30', 'Cat', null, '0'), ('7464', '3918', '30', 'Arrogant Cat', null, '0'), ('7465', '7474', '30', 'Snow Garden', null, '0'), ('7466', '7472', '30', 'Fortress', null, '0'), ('7467', '7472', '30', 'Mill', null, '0'), ('7468', '3918', '30', 'Mew', null, '0'), ('7469', '3918', '30', 'Mews', null, '0'), ('7470', '7472', '30', 'Monastery ', null, '0'), ('7471', '3913', '30', 'Sewer manhole', null, '0'), ('7472', '3913', '34', 'Architecture', null, '0'), ('7473', '3918', '34', 'Birds', null, '0'), ('7474', '5', '34', 'Nature', null, '0'), ('7498', '3908', '25', 'Customer agreement', null, '0'), ('7519', '4149', '44', 'buyer', null, '0'), ('7587', '7473', '30', 'Moscow Kremlin', null, '0'), ('7588', '7473', '30', 'IMG0802', null, '0'), ('7589', '7473', '30', 'IMG0811', null, '0'), ('7655', '7474', '31', 'Apple tree', null, '0'), ('7656', '7474', '31', 'Forest', null, '0'), ('7657', '7473', '52', 'Yellowhammer', null, '0'), ('7658', '7473', '52', 'Waxwing', null, '0'), ('7670', '7474', '30', 'IMG1608', null, '0'), ('7671', '7474', '30', 'Moscow Kremlin', null, '0'), ('7672', '7474', '30', 'IMG1608', null, '0'), ('7673', '7474', '30', 'Moscow Kremlin', null, '0'), ('7712', '5', '34', 'CD collections', null, '0'), ('7713', '7712', '53', 'Cats', null, '0'), ('7735', '5', '34', '???â€ ?????â€ ??', null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `subscription`
-- ----------------------------
DROP TABLE IF EXISTS `subscription`;
CREATE TABLE `subscription` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `content_type` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `bandwidth` int(20) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `content_type` (`content_type`),
  KEY `bandwidth` (`bandwidth`),
  KEY `days` (`days`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=4466 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `subscription`
-- ----------------------------
BEGIN;
INSERT INTO `subscription` VALUES ('4461', '1 day instant access', '10', '1', 'Common', '15', '1'), ('4462', '1 month instant access', '100', '30', 'Common', '200', '2'), ('4464', '1 day instant premium access', '50', '1', 'Common|Premium', '20', '3'), ('4465', '1 month instant premium access', '300', '30', 'Common|Premium', '250', '4');
COMMIT;

-- ----------------------------
--  Table structure for `subscription_limit`
-- ----------------------------
DROP TABLE IF EXISTS `subscription_limit`;
CREATE TABLE `subscription_limit` (
  `name` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `subscription_limit`
-- ----------------------------
BEGIN;
INSERT INTO `subscription_limit` VALUES ('Credits', '1'), ('Downloads', '0'), ('Bandwidth', '0');
COMMIT;

-- ----------------------------
--  Table structure for `subscription_list`
-- ----------------------------
DROP TABLE IF EXISTS `subscription_list`;
CREATE TABLE `subscription_list` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `user` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `data1` int(11) DEFAULT NULL,
  `data2` int(11) DEFAULT NULL,
  `bandwidth` double DEFAULT NULL,
  `bandwidth_limit` int(40) DEFAULT NULL,
  `subscription` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL,
  `subtotal` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `taxes` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `billing_firstname` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_lastname` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_address` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_city` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_zip` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `billing_country` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `user` (`user`),
  KEY `data1` (`data1`),
  KEY `data2` (`data2`),
  KEY `bandwidth` (`bandwidth`),
  KEY `bandwidth_limit` (`bandwidth_limit`),
  KEY `subscription` (`subscription`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `subscription_list`
-- ----------------------------
BEGIN;
INSERT INTO `subscription_list` VALUES ('21', '1 day instant access', 'buyer', '1321959094', '1322045494', '3', '15', '4461', '1', '10', '0', '0', '10', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('7', '1 day instant access', 'common', '1318181794', '1318268194', '0', '15', '4461', '0', '0', '0', '0', '0', null, null, null, null, null, null), ('26', '1 day instant access', 'buyer', '1355489232', '1355575632', '0', '15', '4461', '1', '10', '0', '0', '10', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('22', '1 day instant access', 'buyer', '1344715200', '1348603199', '0', '15', '4461', '1', '10', '0', '0', '10', 'John', 'Brown', '', 'Los Angeles', '', 'United States'), ('18', '1 month instant access', 'common', '1319832000', '1322341199', '3', '150', '4462', '0', '10', '0', '0', '10', 'tester', 'tester', '', 'wqewq', '', 'Albania');
COMMIT;

-- ----------------------------
--  Table structure for `support`
-- ----------------------------
DROP TABLE IF EXISTS `support`;
CREATE TABLE `support` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `telephone` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `question` text COLLATE utf8_bin,
  `username` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `method` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `data` (`data`)
) ENGINE=InnoDB AUTO_INCREMENT=3928 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `support`
-- ----------------------------
BEGIN;
INSERT INTO `support` VALUES ('3927', 'John', 'john@mail.com', '+12323423424', 0x5768793f, null, null, '1334318550', 'by e-mail');
COMMIT;

-- ----------------------------
--  Table structure for `tax`
-- ----------------------------
DROP TABLE IF EXISTS `tax`;
CREATE TABLE `tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `rates_depend` int(11) DEFAULT NULL,
  `price_include` int(11) DEFAULT NULL,
  `rate_all` float DEFAULT NULL,
  `rate_all_type` int(11) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `regions` int(11) DEFAULT NULL,
  `files` tinyint(4) DEFAULT NULL,
  `credits` tinyint(4) DEFAULT NULL,
  `subscription` tinyint(4) DEFAULT NULL,
  `customer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `tax`
-- ----------------------------
BEGIN;
INSERT INTO `tax` VALUES ('3', 'VAT', '2', '0', '10', '1', '1', '0', '1', '1', '1', '0');
COMMIT;

-- ----------------------------
--  Table structure for `tax_regions`
-- ----------------------------
DROP TABLE IF EXISTS `tax_regions`;
CREATE TABLE `tax_regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `country` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`),
  KEY `country` (`country`),
  KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Table structure for `templates`
-- ----------------------------
DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `shome` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `templates`
-- ----------------------------
BEGIN;
INSERT INTO `templates` VALUES ('39', 'Template 15', 'templates/template15/', '0', '3', '1'), ('40', 'Template 16', 'templates/template16/', '0', '3', '1'), ('41', 'Template 17', 'templates/template17/', '0', '3', '1'), ('42', 'Template 18', 'templates/template18/', '0', '3', '1'), ('43', 'Template 19', 'templates/template19/', '0', '3', '1'), ('46', 'Template 20', 'templates/template20/', '0', '3', '1'), ('47', 'Template 21', 'templates/template21/', '1', '3', '1'), ('48', 'Template 22', 'templates/template22/', '0', '3', '1');
COMMIT;

-- ----------------------------
--  Table structure for `testimonials`
-- ----------------------------
DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE `testimonials` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `touser` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `fromuser` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `data` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `touser` (`touser`),
  KEY `fromuser` (`fromuser`),
  KEY `data` (`data`)
) ENGINE=InnoDB AUTO_INCREMENT=4205 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `testimonials`
-- ----------------------------
BEGIN;
INSERT INTO `testimonials` VALUES ('4202', 'siteowner', 'demo', 0x49206c6f76652068696d2070686f746f732061206c6f74212121, '1195339422'), ('4204', 'demo', 'siteowner', 0x5665727920676f6f6420617274776f726b73212121, '1195339778');
COMMIT;

-- ----------------------------
--  Table structure for `user_category`
-- ----------------------------
DROP TABLE IF EXISTS `user_category`;
CREATE TABLE `user_category` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) DEFAULT NULL,
  `upload` int(11) DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `upload2` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `videolimit` int(11) DEFAULT NULL,
  `previewvideolimit` int(11) DEFAULT NULL,
  `photolimit` int(11) DEFAULT NULL,
  `blog` int(11) DEFAULT NULL,
  `menu` int(11) DEFAULT NULL,
  `upload3` int(11) DEFAULT NULL,
  `audiolimit` int(11) DEFAULT NULL,
  `previewaudiolimit` int(11) DEFAULT NULL,
  `upload4` int(11) DEFAULT NULL,
  `vectorlimit` int(11) DEFAULT NULL,
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=4055 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `user_category`
-- ----------------------------
BEGIN;
INSERT INTO `user_category` VALUES ('4052', '1', '1', '25', 'Gold', '1', '1', '250', '10', '5', '1', '1', '1', '50', '5', '1', '20'), ('4053', '1', '1', '25', 'Silver', '1', '2', '50', '10', '10', '1', '0', '1', '20', '2', '1', '15'), ('4054', '0', '1', '25', 'Bronze', '0', '3', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `telephone` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `address` text COLLATE utf8_bin,
  `data1` int(11) DEFAULT NULL,
  `ip` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `accessdenied` int(11) DEFAULT NULL,
  `country` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `category` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `lastname` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `zipcode` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `avatar` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `website` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `utype` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `company` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `newsletter` int(11) DEFAULT NULL,
  `paypal` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `moneybookers` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `examination` int(11) DEFAULT NULL,
  `passport` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `authorization` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `aff_commission_buyer` float DEFAULT NULL,
  `aff_commission_seller` float DEFAULT NULL,
  `aff_visits` int(11) DEFAULT NULL,
  `aff_signups` int(11) DEFAULT NULL,
  `aff_referal` int(11) DEFAULT NULL,
  `state` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `dwolla` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `webmoney` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `qiwi` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `business` tinyint(4) DEFAULT NULL,
  `bank_account` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `bank_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `login` (`login`),
  KEY `data1` (`data1`),
  KEY `authorization` (`authorization`),
  KEY `aff_visits` (`aff_visits`),
  KEY `aff_signups` (`aff_signups`),
  KEY `aff_referal` (`aff_referal`)
) ENGINE=InnoDB AUTO_INCREMENT=7436 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('3931', 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'Nick', 'demo@cmsaccount.com', '1(123)232-24-34', 0x53756e6e797761792052642c203231, '1169233200', '127.0.0.1', '0', 'United States', 'Gold', 'Brown', 'Los Angeles', '90210', '/content/avatars/avatar_demo.gif', '/content/users/photo_demo.jpg', 0x4920616d206120676f6f642070686f746f677261706865722e, 'www.google.com', 'seller', 'Firm Inc.', '1', 'sales@cmsaccount.com', 'sales@cmsaccount.com', '1', null, 'site', '0', '0', null, null, '7292', '', '210-268-9238', 'Z160139856701', '9062496570', '0', '123456789', 'Privatbank'), ('4113', 'demo2', '1066726e7160bd9c987c9968e0cc275a', 'Jeff', 'demo2@cmsaccount.com', '212321323', 0x3173742052642e, '1228760057', '127.0.0.1', '0', 'USA', 'Silver', 'Ri_char_ds', 'New York', '12345', '', '', '', 'www.domain.com', 'buyer', '', '1', '', '', '0', null, 'site', null, null, null, null, null, null, null, null, null, '0', null, null), ('4131', 'siteowner', 'f06b354fe25b904c78083d6708fb71ab', 'Boby', 'sales@cmsaccount.com', '', '', '1193140455', '', '0', '', 'Gold', 'Ri_char_ds', 'Los Angeles', '90210', '/content/avatars/avatar1240915546.gif', '/content/users/photo1240915546.jpg', 0x5665727920676f6f642070686f746f677261706865722e266c743b6272202f2667743b266c743b6272202f2667743b4c6f72656d20697073756d20646f6c6f722073697420616d65742c20636f6e7365637465747565722061646970697363696e6720656c69742c20736564206469616d206e6f6e756d6d79206e69626820657569736d6f642074696e636964756e74207574206c616f7265657420646f6c6f7265206d61676e6120616c697175616d206572617420766f6c75747061742e205574207769736920656e696d206164206d696e696d2076656e69616d2c2071756973206e6f73747275642065786572636920746174696f6e20756c6c616d636f72706572207375736369706974206c6f626f72746973206e69736c20757420616c697175697020657820656120636f6d6d6f646f20636f6e7365717561742e204475697320617574656d2076656c2065756d2069726975726520646f6c6f7220696e2068656e64726572697420696e2076756c7075746174652076656c69742065737365206d6f6c657374696520636f6e7365717561742c2076656c20696c6c756d20646f6c6f72652065752066657567696174206e756c6c6120666163696c69736973206174207665726f2065726f7320657420616363756d73616e20657420697573746f206f64696f206469676e697373696d2071756920626c616e646974207072616573656e74206c7570746174756d207a7a72696c2064656c656e6974206175677565206475697320646f6c6f72652074652066657567616974206e756c6c6120666163696c6973692e266c743b6272202f2667743b, 'www.yahoo.com', 'seller', 'Company Inc.', '1', 'payout@cmsaccount.com', 'sales@cmsaccount.com', '0', null, 'site', '0', '0', null, null, null, '', '', '', '', '0', null, null), ('4132', 'john', 'a66e44736e753d4533746ced572ca821', 'John', 'sales@cmsaccount.com', '', '', '1193140555', '', '0', 'Canada', 'Silver', 'Smith', 'Ottava', '', '/content/avatars/avatar1240915571.gif', '/content/users/photo1240915571.jpg', 0x457863656c6c656e742070686f746f677261706865722e, 'www.msn.com', 'seller', '', '1', '', '', null, null, 'site', null, null, null, null, null, null, null, null, null, '0', null, null), ('6355', 'buyer', '794aad24cbd58461011ed9094b7fa212', 'John', 'buyer@cmsaccount.com', '', '', '1244146298', '127.0.0.1', '0', 'United States', 'Silver', 'Brown', 'Los Angeles', '', null, null, '', 'http://www.google.com', 'buyer', '', '1', '', '', '0', null, 'site', '0', '0', null, null, '7292', '', null, null, null, '0', null, null), ('6360', 'seller', '64c9ac2bb5fe46c3ac32844bb97be6bc', 'Nick', 'seller@cmsaccount.com', '', '', '1244150390', '127.0.0.1', '0', 'United States', 'Silver', 'Nickson', 'New York', '', null, null, '', '', 'seller', '', '1', '', '', '1', '/content/users/passport_.jpg', 'site', null, null, null, null, null, null, null, null, null, '0', null, null), ('7292', 'affiliate', '6d0bd9c8d2eadeb088b34895fde10c55', 'Jacob', 'affiliate@cmsaccount.com', '', '', '1301571281', '127.0.0.1', '0', 'Austria', 'Silver', 'Johnson', 'Wien', '', null, null, '', '', 'affiliate', '', '1', 'paypal@cmsaccount.com', 'moneybookers@cmsaccount.com', '0', null, 'site', '15', '10', '4', '0', '0', '', '210-268-9238', 'Z160139856701', '10859', '0', '1234-5678-1234-56781', 'VTB24'), ('7403', 'common', '9efab2399c7c560b34de477b9aa0a465', 'Bob', 'common@cmsaccount.com', '', '', '1318176792', '127.0.0.1', '0', 'Austria', 'Silver', 'Smith', 'Wien', '', null, null, '', '', 'common', '', '1', 'sales@cmsaccount.com', 'sales@cmsaccount.com', '1', null, 'site', '15', '10', '0', '0', '0', '', '210-268-9238', 'Z160139856701', '10859', '0', '1234-5678-1234-5678', 'VTB24'), ('7435', 'seller2', 'c30248d146039dd086b12f18154863e1', 'seller2', 'seller2@cmsaccount.com', '', '', '1360235342', '127.0.0.1', '0', 'Austria', 'Silver', 'seller2', '', '', null, null, null, '', 'common', '', '1', null, null, '1', null, 'site', '15', '10', '0', '0', '0', '', null, null, null, '0', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `users_access`
-- ----------------------------
DROP TABLE IF EXISTS `users_access`;
CREATE TABLE `users_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `data` int(50) DEFAULT NULL,
  `ip` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `bandwidth` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `data` (`data`)
) ENGINE=InnoDB AUTO_INCREMENT=726 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `users_access`
-- ----------------------------
BEGIN;
INSERT INTO `users_access` VALUES ('722', '7435', '1376838774', '127.0.0.1', '0'), ('723', '7441', '1376838839', '127.0.0.1', '0'), ('724', '3931', '1376839079', '127.0.0.1', '0'), ('725', '6355', '1376841319', '127.0.0.1', '0');
COMMIT;

-- ----------------------------
--  Table structure for `users_ip_blocked`
-- ----------------------------
DROP TABLE IF EXISTS `users_ip_blocked`;
CREATE TABLE `users_ip_blocked` (
  `ip` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Table structure for `users_login_failed`
-- ----------------------------
DROP TABLE IF EXISTS `users_login_failed`;
CREATE TABLE `users_login_failed` (
  `login` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `ip` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  KEY `ip` (`ip`),
  KEY `login` (`login`),
  KEY `data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Table structure for `users_qauth`
-- ----------------------------
DROP TABLE IF EXISTS `users_qauth`;
CREATE TABLE `users_qauth` (
  `title` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `consumer_key` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `consumer_secret` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `request_token_url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `access_token_url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `authorize_url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `users_qauth`
-- ----------------------------
BEGIN;
INSERT INTO `users_qauth` VALUES ('Twitter', 'snlPXUlvzPHkzR1Kyv4MMA', '9feJCd6CnezgKwi17Onu3MUppCanxleS6kezR2nqAs', 'https://api.twitter.com/oauth/request_token', 'https://api.twitter.com/oauth/access_token', 'https://api.twitter.com/oauth/authorize', '1'), ('Facebook', '210749745608292', 'cf261435ee47d74e6cf4d5978b3c9ad0', '', '', '', '1'), ('Vkontakte', '3219044', 'st5FsKHfXXOcJSH4PE5V', '', '', '', '1'), ('Instagram', '3b3ace34d2a1471b882ea070bfa21af7', '523e22d8ba094d318d1c8b703fdea347', null, null, null, '1');
COMMIT;

-- ----------------------------
--  Table structure for `users_settings`
-- ----------------------------
DROP TABLE IF EXISTS `users_settings`;
CREATE TABLE `users_settings` (
  `title` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `svalue` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `activ` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `users_settings`
-- ----------------------------
BEGIN;
INSERT INTO `users_settings` VALUES ('Activation disabled', 'off', '0'), ('Activation enabled', 'on', '1'), ('Activation by user', 'user', '0'), ('Activation by admin', 'admin', '0');
COMMIT;

-- ----------------------------
--  Table structure for `users_signup`
-- ----------------------------
DROP TABLE IF EXISTS `users_signup`;
CREATE TABLE `users_signup` (
  `param` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `users_signup`
-- ----------------------------
BEGIN;
INSERT INTO `users_signup` VALUES ('1');
COMMIT;

-- ----------------------------
--  Table structure for `vector`
-- ----------------------------
DROP TABLE IF EXISTS `vector`;
CREATE TABLE `vector` (
  `id_parent` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `keywords` text COLLATE utf8_bin,
  `author` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `viewed` int(11) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `folder` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `content_type` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `free` tinyint(1) DEFAULT NULL,
  `downloaded` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `model` int(10) DEFAULT NULL,
  `flash_width` int(11) DEFAULT NULL,
  `flash_height` int(11) DEFAULT NULL,
  `server1` int(11) DEFAULT NULL,
  `server2` int(11) DEFAULT NULL,
  `server3` tinyint(1) DEFAULT NULL,
  `examination` tinyint(1) DEFAULT NULL,
  `flash_version` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `script_version` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `category2` int(11) DEFAULT NULL,
  `category3` int(11) DEFAULT NULL,
  `google_x` double DEFAULT NULL,
  `google_y` double DEFAULT NULL,
  `refuse_reason` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `adult` tinyint(4) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `title` (`title`),
  KEY `data` (`data`),
  KEY `published` (`published`),
  KEY `featured` (`featured`),
  KEY `viewed` (`viewed`),
  KEY `downloaded` (`downloaded`),
  KEY `free` (`free`),
  KEY `author` (`author`),
  KEY `userid` (`userid`),
  KEY `rating` (`rating`),
  KEY `model` (`model`),
  KEY `examination` (`examination`),
  KEY `category2` (`category2`),
  KEY `category3` (`category3`),
  KEY `google_x` (`google_x`),
  KEY `google_y` (`google_y`),
  KEY `server1` (`server1`),
  KEY `server2` (`server2`),
  KEY `adult` (`adult`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `vector`
-- ----------------------------
BEGIN;
INSERT INTO `vector` VALUES ('4839', 'Vector Illustration', 0x4162737472616374, 0x766563746f722c696c6c757374726174696f6e, 'siteowner', '1217486920', '110', '1', '0', null, '4839', 'Common', '0', '5', '2', '0', '0', '0', '1', '0', null, '0', '', '', '5', '5', '0', '0', null, '/stock-vector/vector-illustration-4839.html', '0'), ('7176', 'Flash example', '', 0x666c617368, 'demo', '1292931298', '45', '1', '0', '0', '7176', 'Common', '0', '1', null, '0', '0', '0', '2', '0', null, '0', '', '', '5', '5', '0', '0', null, '/stock-vector/flash-example-7176.html', '0'), ('7713', 'Cats', 0x4d616e792063617473, 0x63617473, 'common', '1365161510', '43', '1', '0', '0', '7713', 'Common', '0', '0', null, '0', '0', '0', '2', '0', null, '0', '', '', '5', '5', '0', '0', null, '/stock-vector/cats-7713.html', '0');
COMMIT;

-- ----------------------------
--  Table structure for `vector_types`
-- ----------------------------
DROP TABLE IF EXISTS `vector_types`;
CREATE TABLE `vector_types` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `types` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `shipped` int(11) DEFAULT NULL,
  `license` int(10) NOT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `priority` (`priority`),
  KEY `license` (`license`)
) ENGINE=InnoDB AUTO_INCREMENT=6788 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `vector_types`
-- ----------------------------
BEGIN;
INSERT INTO `vector_types` VALUES ('4804', 'ZIP', 'zip', '5', '2', '0', '4583'), ('5894', 'Shipped CD', 'shipped', '10', '5', '1', '4583'), ('6786', 'ZIP', 'zip', '50', '0', '0', '4584'), ('6787', 'Shipped CD', 'shipped', '100', '5', '1', '4584');
COMMIT;

-- ----------------------------
--  Table structure for `video_fields`
-- ----------------------------
DROP TABLE IF EXISTS `video_fields`;
CREATE TABLE `video_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `activ` int(11) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `always` int(11) DEFAULT NULL,
  `fname` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `video_fields`
-- ----------------------------
BEGIN;
INSERT INTO `video_fields` VALUES ('1', 'category', '1', '1', '1', '1', 'folder'), ('2', 'title', '2', '1', '1', '1', 'title'), ('3', 'description', '3', '1', '1', '0', 'description'), ('4', 'keywords', '4', '1', '1', '0', 'keywords'), ('5', 'file for sale', '5', '1', '1', '1', 'sale'), ('6', 'preview video', '6', '1', '1', '1', 'previewvideo'), ('7', 'preview picture', '7', '1', '1', '1', 'previewpicture'), ('9', 'U.S. 2257 Information', '15', '0', '0', '0', 'usa'), ('10', 'duration', '9', '1', '1', '0', 'duration'), ('11', 'clip format', '10', '1', '0', '0', 'format'), ('12', 'aspect ratio', '11', '1', '0', '0', 'ratio'), ('13', 'field rendering', '12', '1', '0', '0', 'rendering'), ('14', 'frames per second', '13', '1', '0', '0', 'frames'), ('15', 'copyright holder', '14', '1', '0', '0', 'holder'), ('16', 'terms and conditions', '16', '1', '1', '0', 'terms');
COMMIT;

-- ----------------------------
--  Table structure for `video_format`
-- ----------------------------
DROP TABLE IF EXISTS `video_format`;
CREATE TABLE `video_format` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `video_format`
-- ----------------------------
BEGIN;
INSERT INTO `video_format` VALUES ('3', 'DV / MiniDV / DVCam'), ('4', 'Beta sp'), ('5', 'Digibeta'), ('6', 'HDV'), ('7', 'Other Hi-Def'), ('8', '35mm film'), ('9', '16mm film'), ('10', '8mm film'), ('11', 'Computer generated'), ('12', 'Flash File'), ('13', 'Other');
COMMIT;

-- ----------------------------
--  Table structure for `video_frames`
-- ----------------------------
DROP TABLE IF EXISTS `video_frames`;
CREATE TABLE `video_frames` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `video_frames`
-- ----------------------------
BEGIN;
INSERT INTO `video_frames` VALUES ('2', 'PAL'), ('3', 'NTSC'), ('4', 'HD1080'), ('5', 'HD720'), ('6', 'Other');
COMMIT;

-- ----------------------------
--  Table structure for `video_ratio`
-- ----------------------------
DROP TABLE IF EXISTS `video_ratio`;
CREATE TABLE `video_ratio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `video_ratio`
-- ----------------------------
BEGIN;
INSERT INTO `video_ratio` VALUES ('2', '4:3', '4', '3'), ('3', '16:9 native', '16', '9'), ('4', '16:9 anamorphic', '16', '9'), ('5', '16:9 letterboxed', '16', '9'), ('8', 'Other', '4', '3');
COMMIT;

-- ----------------------------
--  Table structure for `video_rendering`
-- ----------------------------
DROP TABLE IF EXISTS `video_rendering`;
CREATE TABLE `video_rendering` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `video_rendering`
-- ----------------------------
BEGIN;
INSERT INTO `video_rendering` VALUES ('2', 'Interlaced'), ('3', 'Progressive scan'), ('4', 'Other');
COMMIT;

-- ----------------------------
--  Table structure for `video_types`
-- ----------------------------
DROP TABLE IF EXISTS `video_types`;
CREATE TABLE `video_types` (
  `id_parent` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `types` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `shipped` int(11) DEFAULT NULL,
  `license` int(10) NOT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `priority` (`priority`),
  KEY `shipped` (`shipped`),
  KEY `license` (`license`)
) ENGINE=InnoDB AUTO_INCREMENT=6787 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `video_types`
-- ----------------------------
BEGIN;
INSERT INTO `video_types` VALUES ('4798', 'QuickTime', '1', 'mov', '5', '0', '4583'), ('4799', 'AVI', '2', 'avi', '6', '0', '4583'), ('4800', 'WMV', '3', 'wmv', '3', '0', '4583'), ('4805', 'FLV', '4', 'flv', '5', '0', '4583'), ('5892', 'Shipped CD', '5', 'shipped', '10', '1', '4583'), ('6775', 'AVI', '2', 'avi', '60', '0', '4584'), ('6774', 'QuickTime', '1', 'mov', '50', '0', '4584'), ('6776', 'WMV', '3', 'wmv', '30', '0', '4584'), ('6777', 'FLV', '4', 'flv', '50', '0', '4584'), ('6781', 'Shipped CD', '5', 'shipped', '100', '1', '4584'), ('6784', 'MP4', '0', 'mp4', '5', '0', '4583'), ('6786', 'MP4', '0', 'mp4', '40', '0', '4584');
COMMIT;

-- ----------------------------
--  Table structure for `videos`
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id_parent` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `folder` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `viewed` int(11) DEFAULT NULL,
  `author` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `keywords` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `usa` text COLLATE utf8_bin,
  `duration` int(10) DEFAULT NULL,
  `format` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ratio` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `rendering` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `holder` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `frames` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `content_type` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `free` tinyint(1) DEFAULT NULL,
  `downloaded` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `model` int(10) DEFAULT NULL,
  `server1` int(11) DEFAULT NULL,
  `server2` int(11) DEFAULT NULL,
  `server3` tinyint(1) DEFAULT NULL,
  `examination` tinyint(1) DEFAULT NULL,
  `category2` int(11) DEFAULT NULL,
  `category3` int(11) DEFAULT NULL,
  `google_x` double DEFAULT NULL,
  `google_y` double DEFAULT NULL,
  `refuse_reason` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `adult` tinyint(4) DEFAULT NULL,
  KEY `id_parent` (`id_parent`),
  KEY `data` (`data`),
  KEY `title` (`title`),
  KEY `published` (`published`),
  KEY `featured` (`featured`),
  KEY `viewed` (`viewed`),
  KEY `downloaded` (`downloaded`),
  KEY `free` (`free`),
  KEY `userid` (`userid`),
  KEY `author` (`author`),
  KEY `rating` (`rating`),
  KEY `examination` (`examination`),
  KEY `category2` (`category2`),
  KEY `category3` (`category3`),
  KEY `google_x` (`google_x`),
  KEY `google_y` (`google_y`),
  KEY `server1` (`server1`),
  KEY `server2` (`server2`),
  KEY `duration` (`duration`),
  KEY `adult` (`adult`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `videos`
-- ----------------------------
BEGIN;
INSERT INTO `videos` VALUES ('7656', 'Forest', '1361095640', '1', '', '7656', '0', '16', 'common', 'nature,forest', '0', '', '11', 'DV / MiniDV / DVCam', '16:9 native', 'Interlaced', 'John Smith', 'PAL', 'Common', '0', '0', null, '0', '2', '0', null, '0', '5', '5', '0', '0', null, '/stock-video/forest-7656.html', '0'), ('7655', 'Apple tree', '1361095355', '1', 0x546865206170706c6520626c6f73736f6d732e, '7655', '0', '6', 'common', 'apple,nature,blossom,flower', '0', '', '17', 'DV / MiniDV / DVCam', '16:9 native', 'Interlaced', 'John Smith', 'PAL', 'Common', '0', '0', null, '0', '2', '0', null, '0', '5', '5', '0', '0', null, '/stock-video/apple-tree-7655.html', '0');
COMMIT;

-- ----------------------------
--  Table structure for `voteitems`
-- ----------------------------
DROP TABLE IF EXISTS `voteitems`;
CREATE TABLE `voteitems` (
  `id` int(11) DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `vote` int(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `voteitems`
-- ----------------------------
BEGIN;
INSERT INTO `voteitems` VALUES ('4817', '127.0.0.1', '3'), ('5041', '127.0.0.1', '4'), ('3988', '127.0.0.1', '4'), ('4839', '127.0.0.1', '2'), ('4730', '127.0.0.1', '5'), ('3984', '127.0.0.1', '3'), ('4736', '127.0.0.1', '3'), ('6439', '127.0.0.1', '2'), ('6422', '127.0.0.1', '4'), ('6429', '127.0.0.1', '1'), ('4815', '127.0.0.1', '4'), ('6401', '127.0.0.1', '4'), ('6259', '127.0.0.1', '4'), ('4734', '127.0.0.1', '3'), ('6470', '127.0.0.1', '4'), ('3976', '127.0.0.1', '3'), ('3973', '127.0.0.1', '3'), ('3989', '127.0.0.1', '3'), ('3975', '127.0.0.1', '3'), ('3974', '127.0.0.1', '4'), ('6453', '127.0.0.1', '4'), ('7583', '127.0.0.1', '5'), ('7725', '127.0.0.1', '3');
COMMIT;

-- ----------------------------
--  Table structure for `watermark`
-- ----------------------------
DROP TABLE IF EXISTS `watermark`;
CREATE TABLE `watermark` (
  `photo` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `watermark`
-- ----------------------------
BEGIN;
INSERT INTO `watermark` VALUES ('/content/watermark.png', '5');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
