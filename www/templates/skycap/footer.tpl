</div>
<div class="fixed-social">
    <ul class="subs-list">
        <li><a href="#" class="yot"></a></li>
        <li><a href="#" class="inst"></a></li>
        <li><a href="#" class="fb"></a></li>
        <li><a href="#" class="vk"></a></li>
    </ul>
</div>
<a id="scroller" href="#" class="to-top"></a>
<footer class="container-fluid">
    <div class="container">
        <div class="row">
            <a href="/" class="col-md-3 logo">
                <img src="{TEMPLATE_ROOT}img/logo.png">
            </a>
            <div class="col-lg-9 pt15">
                <p class="ml20">
                    SkyCap-stock Видео предлагает растущую библиотеку стоковых видео и клипов без лицензионных
                    платежей для использования в кино, на телевидении, в рекламе, интерактивных веб-сайтах и других
                    мультимедийных проектах.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 line-divider"></div>
        </div>
        <div class="row pt20">
            <div class="col-md-3">
                <ul class="ft-list">
                    <li>
                        SkyCap-stock Видео
                    </li>
                    <li>
                        <a href="{SITE_ROOT}">{lang.Home}</a>
                    </li>
                    <li>
                        <a href="{SITE_ROOT}pages/about.html">{lang.About}</a>
                    </li>
                    <li>
                        <a href="#">Блог</a>
                    </li>
                    <li>
                        <a href="#">Архив информационных бюллетеней</a>
                    </li>
                    <li>
                        <a href="#">Вакансии</a>
                    </li>
                    <li>
                        <a href="#">Связи с инвесторами</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="ft-list">
                    <li>
                        Справка
                    </li>
                    <li>
                        <a href="#">Центр поддержки</a>
                    </li>
                    <li>
                        <a href="{SITE_ROOT}contacts/">{lang.Contacts}</a>
                    </li>
                </ul>
                <ul class="ft-list">
                    <li>
                        Язык
                    </li>
                    <li>
                        <a href="#">Русский</a>
                    </li>
                    <li>
                        <a href="#">Английский</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="ft-list">
                    <li>
                        Юридические положения
                    </li>
                    <li>
                        <a href="#"> Условия использования веб-сайта</a>
                    </li>
                    <li>
                        <a href="#"> Условия лицензирования сток видео</a>
                    </li>
                    <li>
                        <a href="#">Политика конфиденциальности</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 ft-subscribe">
                <h6>Подпишитесь на нас</h6>
                <ul class="subs-list">
                    <li><a href="#" class="yot"></a></li>
                    <li><a href="#" class="inst"></a></li>
                    <li><a href="#" class="fb"></a></li>
                    <li><a href="#" class="vk"></a></li>
                </ul>
                <p class="phones">
                    {TELEPHONE}
                </p>
                <a href="mailto:{EMAIL}" class="email"><i class="i-email"></i> {EMAIL} </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 line-divider"></div>
        </div>
        <div class="row">
            <div class="col-md-8 center-block text-center copyR">
                <img src="{TEMPLATE_ROOT}img/st-logo.png"> © 2015  DaVinci-design studio
            </div>
        </div>
    </div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{TEMPLATE_ROOT}js/jquery-1.11.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{TEMPLATE_ROOT}js/bootstrap.min.js"></script>

<!-- Aditional plugins -->
<script src="{TEMPLATE_ROOT}js/imagesloaded.pkgd.min.js"></script>
<script src="{TEMPLATE_ROOT}js/jquery-imagefill.js"></script>
<script src="{TEMPLATE_ROOT}js/fontsmoothie.min.js"></script>
<script src="{TEMPLATE_ROOT}js/jquery.formstyler.min.js"></script>
<script src="{TEMPLATE_ROOT}js/jquery.mCustomScrollbar.min.js"></script>
<script src="{TEMPLATE_ROOT}js/video.js"></script>
<script src="{TEMPLATE_ROOT}js/jquery.vide.js"></script>
<script src="{TEMPLATE_ROOT}js/owl.carousel.min.js"></script>
<script>
    videojs.options.flash.swf = "video-js.swf"
</script>

<!-- jQuery UI - Custom - ########### Only effects ########## -->
<script src="{TEMPLATE_ROOT}js/jquery-ui.min.js"></script>

<!-- Initial script -->
<script src="{TEMPLATE_ROOT}js/script.js"></script>


</body>
</html>