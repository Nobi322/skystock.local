<!DOCTYPE html>
<html lang="{LANG_SYMBOL}">
<head>
    <meta charset="{MTG}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1000">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{SITE_NAME}</title>

    <!-- Bootstrap -->
    <link href="{TEMPLATE_ROOT}css/jquery.formstyler.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/video-js.min.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/owl.carousel.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/owl.theme.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/styles.css" rel="stylesheet">

    <meta name="description" content="{DESCRIPTION}">
    <meta name="keywords" content="{KEYWORDS}">
    {META_SOCIAL}
    <link href="{SITE_ROOT}images/favicon.gif" type="image/gif" rel="icon">
    <link href="{SITE_ROOT}images/favicon.gif" type="image/gif" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="container-fluid">
    <div class="row top-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-8 pull-right top-menu">
                    <ul class="lang-menu">
                        <li>
                            <a href="#">En</a>
                        </li>
                        <li class="active">
                            <a href="#">Ru</a>
                        </li>
                    </ul>
                    <div class="profile login">
                        <a href="#login-pp" class="open-log">
                            Войти
                        </a>
                        <div id="login-pp" class="login-pp">
                            <form method='post' action='{SITE_ROOT}members/check.php'>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class='form-control' type='text' required name='l' placeholder="{lang.Login}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class='form-control' type='password' name='p' required placeholder="{lang.Password}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-default">{lang.Enter}</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{SITE_ROOT}members/forgot.php" class="grey-link">
                                            {lang.Forgot password}
                                        </a>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-12 line-divider"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-cell">
                                    Еще не зарегистрированы?<br>
                                    <a href="{SITE_ROOT}members/signup.php?utype=affiliate&step=1">
                                        Создать бесплатный аккаунт
                                        для просмотра
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile signed-in hidden">
                        <a href="#sigin-pp" class="open-sig">
                            Здравствуйте, iren <i class="fa fa-angle-down"></i>
                        </a>
                        <div id="sigin-pp" class="sigin-pp">
                            <ul class="sig-list">
                                <li><a href="#" class="si-favorite"><i class="icon"></i> Избранное</a></li>
                                <li><a href="#" class="si-profile"><i class="icon"></i> Сведения об аккаунте</a></li>
                                <li><a href="#" class="si-downloads"><i class="icon"></i> История загрузок</a></li>
                                <li><a href="#" class="si-logout"><i class="icon"></i> Выйти</a></li>
                            </ul>
                        </div>
                    </div>
                    <a href="#" class="cart-link">В корзине 2 товара</a>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row h-second">
            <a href="/" class="col-md-3 logo">
                <img src="{TEMPLATE_ROOT}img/logo.png" alt=""/>
            </a>
            <div class="col-md-3 pull-right info-h">
                <p class="phones">
                    {TELEPHONE}
                </p>
                <a href="mailto:{EMAIL}" class="email"><i class="i-email"></i> {EMAIL}</a>
            </div>
        </div>
    </div>
</header>
<div class="wrapper">


    