<!DOCTYPE html>
<html lang="{LANG_SYMBOL}">
<head>
    <meta charset="{MTG}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1000">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{SITE_NAME}</title>

    <!-- Bootstrap -->
    <link href="{TEMPLATE_ROOT}css/jquery.formstyler.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/video-js.min.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/owl.carousel.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/owl.theme.css" rel="stylesheet">
    <link href="{TEMPLATE_ROOT}css/styles.css" rel="stylesheet">

    <meta name="description" content="{DESCRIPTION}">
    <meta name="keywords" content="{KEYWORDS}">
    {META_SOCIAL}
    <link href="{SITE_ROOT}images/favicon.gif" type="image/gif" rel="icon">
    <link href="{SITE_ROOT}images/favicon.gif" type="image/gif" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="container-fluid">
    <div class="row top-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-8 pull-right top-menu">
                    <ul class="lang-menu">
                        <li>
                            <a href="#">En</a>
                        </li>
                        <li class="active">
                            <a href="#">Ru</a>
                        </li>
                    </ul>
                    <div class="profile login">
                        <a href="#login-pp" class="open-log">
                            Войти
                        </a>
                        <div id="login-pp" class="login-pp">
                            <form method='post' action='{SITE_ROOT}members/check.php'>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class='form-control' type='text' required name='l' placeholder="{lang.Login}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class='form-control' type='password' name='p' required placeholder="{lang.Password}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-default">{lang.Enter}</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{SITE_ROOT}members/forgot.php" class="grey-link">
                                            {lang.Forgot password}
                                        </a>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-12 line-divider"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-cell">
                                    Еще не зарегистрированы?<br>
                                    <a href="{SITE_ROOT}members/signup.php?utype=affiliate&step=1">
                                        Создать бесплатный аккаунт
                                        для просмотра
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile signed-in hidden">
                        <a href="#sigin-pp" class="open-sig">
                            Здравствуйте, iren <i class="fa fa-angle-down"></i>
                        </a>
                        <div id="sigin-pp" class="sigin-pp">
                            <ul class="sig-list">
                                <li><a href="#" class="si-favorite"><i class="icon"></i> Избранное</a></li>
                                <li><a href="#" class="si-profile"><i class="icon"></i> Сведения об аккаунте</a></li>
                                <li><a href="#" class="si-downloads"><i class="icon"></i> История загрузок</a></li>
                                <li><a href="#" class="si-logout"><i class="icon"></i> Выйти</a></li>
                            </ul>
                        </div>
                    </div>
                    <a href="#" class="cart-link">В корзине 2 товара</a>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row h-second">
            <a href="/" class="col-md-3 logo">
                <img src="{TEMPLATE_ROOT}img/logo.png" alt=""/>
            </a>
            <div class="col-md-3 pull-right info-h">
                <p class="phones">
                    {TELEPHONE}
                </p>
                <a href="mailto:{EMAIL}" class="email"><i class="i-email"></i> {EMAIL}</a>
            </div>
        </div>
    </div>
</header>
<div class="wrapper">

<section class="search-block container-fluid" style="background: url('{TEMPLATE_ROOT}img/search-bg.png')">
    <video id="mp-video-bg" data-setup='{ "controls": false, "autoplay": true, "muted":true,  "preload": "auto" }'  class="mp-video-bg video-js vjs-default-skin"
           controls preload="auto" width="100%" height="530"
           poster="http://video-js.zencoder.com/oceans-clip.png" width="100%" height="100%" autoplay loop>
        <source src="http://ak.picdn.net/footage/assets/montage/HomepageReel_062515.orig.mp4" type='video/mp4' />
        <source src="src="http://ak.picdn.net/footage/assets/montage/HomepageReel_062515.orig.webm type='video/webm' />
        <p class="vjs-no-js">Для просмотра этого видео пожалуйста включите JavaScript, или узнайте какой браузер поддерживает  <a href="http://videojs.com/html5-video-support/" target="_blank">воспроизведение HTML5 видео</a></p>
    </video>
    <div class="container">
        <div class="row">
            <div class="col-md-8 center-block search-big">
                <form method='get' action='{SITE_ROOT}index.php'>
                    <input name='search' type="text" placeholder="Поиск видео">
                    <button type="submit" class="btn btn-search"><i class="i-magni"></i> {lang.Search}</button>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 text-center pt40">
            <h1>{lang.Categories}</h1>
            <div class="categories-block">
                <div class="row">
                    <?
                    $i=0;
                    $t = array(
                        "Технологии",
                        "Люди",
                        "Фоны/текстуры",
                        "Природа",
                        "Спорт/отдых",
                        "Искуство"
                    );
                    while($i++<6):
                    ?>
                        <div class="col-md-4 ">
                            <a href="#" class="cat-item">
                                <div class="img-cnt">
                                    <img src="{TEMPLATE_ROOT}img/content/cat-<? echo $i; ?>.jpg">
                                </div>
                                <div class="cat-text">
                                    <? echo $t[$i-1]; ?>
                                </div>
                            </a>
                        </div>
                    <? endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {BOX_CATEGORIES}
        </div>
    </div>
</section>
<section class="tarif">
    <div class="container">
        <div class="row">
            <div class="col-md-12 line-divider"></div>
            <div class="col-md-12 tarif-single">
                Скачайте один видеоклип: &nbsp;&nbsp;
                <span class="price">19 грн </span><span class="type">Web</span><span class="divider"></span>
                <span class="price">49 грн </span><span class="type">HD</span><span class="divider"></span>
                <span class="price">79 грн </span><span class="type">SD</span><span class="divider"></span>
                <span class="price">199 грн </span><span class="type">4K</span>
            </div>
            <div class="col-md-12 line-divider"></div>
            <div class="col-md-4 tarif-single">
                Экономьте на видео:
            </div>
            <div class="col-md-7">

                <a href="#" class="chose-packet">
                    Выберите подходящий пакет подписки
                    <i class="i-arrow-round"></i>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="register-slide container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-7 center-block">
                <form class="register-form">
                    <h5>Создать бесплатный аккаунт</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="email" class="form-control" placeholder="Ваш e-mail"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Ваше имя"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="password" class="form-control" placeholder="Введите пароль"/>
                        </div>
                    </div>
                    <div class="row mb15 mt20">
                        <div class="col-md-10 col-md-offset-1">
                            <input id="subscribe" type="checkbox">
                            <label for="subscribe" class="text">Да, присылайте мне обновления и специальные предложения от SkyCap-stock по электронной почте.</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <input id="accept" type="checkbox" required>
                            <label for="accept" class="text">Я принимаю Условия использования сайта и Условия лицензирования SkyCap-stock.</label>
                        </div>
                    </div>
                    <div class="row mt40">
                        <div class="col-md-10 col-md-offset-1">
                            <button class="btn btn-default">Зарегистрироваться</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

</div>
<div class="fixed-social">
    <ul class="subs-list">
        <li><a href="#" class="yot"></a></li>
        <li><a href="#" class="inst"></a></li>
        <li><a href="#" class="fb"></a></li>
        <li><a href="#" class="vk"></a></li>
    </ul>
</div>
<a id="scroller" href="#" class="to-top"></a>
<footer class="container-fluid">
    <div class="container">
        <div class="row">
            <a href="/" class="col-md-3 logo">
                <img src="{TEMPLATE_ROOT}img/logo.png">
            </a>
            <div class="col-lg-9 pt15">
                <p class="ml20">
                    SkyCap-stock Видео предлагает растущую библиотеку стоковых видео и клипов без лицензионных
                    платежей для использования в кино, на телевидении, в рекламе, интерактивных веб-сайтах и других
                    мультимедийных проектах.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 line-divider"></div>
        </div>
        <div class="row pt20">
            <div class="col-md-3">
                <ul class="ft-list">
                    <li>
                        SkyCap-stock Видео
                    </li>
                    <li>
                        <a href="{SITE_ROOT}">{lang.Home}</a>
                    </li>
                    <li>
                        <a href="{SITE_ROOT}pages/about.html">{lang.About}</a>
                    </li>
                    <li>
                        <a href="#">Блог</a>
                    </li>
                    <li>
                        <a href="#">Архив информационных бюллетеней</a>
                    </li>
                    <li>
                        <a href="#">Вакансии</a>
                    </li>
                    <li>
                        <a href="#">Связи с инвесторами</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="ft-list">
                    <li>
                        Справка
                    </li>
                    <li>
                        <a href="#">Центр поддержки</a>
                    </li>
                    <li>
                        <a href="{SITE_ROOT}contacts/">{lang.Contacts}</a>
                    </li>
                </ul>
                <ul class="ft-list">
                    <li>
                        Язык
                    </li>
                    <li>
                        <a href="#">Русский</a>
                    </li>
                    <li>
                        <a href="#">Английский</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="ft-list">
                    <li>
                        Юридические положения
                    </li>
                    <li>
                        <a href="#"> Условия использования веб-сайта</a>
                    </li>
                    <li>
                        <a href="#"> Условия лицензирования сток видео</a>
                    </li>
                    <li>
                        <a href="#">Политика конфиденциальности</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 ft-subscribe">
                <h6>Подпишитесь на нас</h6>
                <ul class="subs-list">
                    <li><a href="#" class="yot"></a></li>
                    <li><a href="#" class="inst"></a></li>
                    <li><a href="#" class="fb"></a></li>
                    <li><a href="#" class="vk"></a></li>
                </ul>
                <p class="phones">
                    {TELEPHONE}
                </p>
                <a href="mailto:{EMAIL}" class="email"><i class="i-email"></i> {EMAIL} </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 line-divider"></div>
        </div>
        <div class="row">
            <div class="col-md-8 center-block text-center copyR">
                <img src="{TEMPLATE_ROOT}img/st-logo.png"> © 2015  DaVinci-design studio
            </div>
        </div>
    </div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{TEMPLATE_ROOT}js/jquery-1.11.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{TEMPLATE_ROOT}js/bootstrap.min.js"></script>

<!-- Aditional plugins -->
<script src="{TEMPLATE_ROOT}js/imagesloaded.pkgd.min.js"></script>
<script src="{TEMPLATE_ROOT}js/jquery-imagefill.js"></script>
<script src="{TEMPLATE_ROOT}js/fontsmoothie.min.js"></script>
<script src="{TEMPLATE_ROOT}js/jquery.formstyler.min.js"></script>
<script src="{TEMPLATE_ROOT}js/jquery.mCustomScrollbar.min.js"></script>
<script src="{TEMPLATE_ROOT}js/video.js"></script>
<script src="{TEMPLATE_ROOT}js/owl.carousel.min.js"></script>
<script>
    videojs.options.flash.swf = "{TEMPLATE_ROOT}js/video-js.swf"
</script>

<!-- jQuery UI - Custom - ########### Only effects ########## -->
<script src="{TEMPLATE_ROOT}js/jquery-ui.min.js"></script>

<!-- Initial script -->
<script src="{TEMPLATE_ROOT}js/script.js"></script>


</body>
</html>
