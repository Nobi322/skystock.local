<div class="col-md-3 item-pill">
	<div class="item-cnt" data-video-src="{PREVIEW_VIDEO}" data-video-title="{ITEM_TITLE_FULL}" data-item-no="54fdfsfb">
		<a href="{ITEM_URL}" class="img-cnt">
			<img src="{ITEM_IMG2}" alt="{ITEM_TITLE_FULL}">
		</a>
		<div class="ip-title">
			<span class="quality">HD</span> {DURATION}
		</div>
		<div class="ip-buttons">
			<a href="#" class="ip-favorite <? echo (rand(0,1)) ?  'active': ''; ?>" title="{lang.add to favorite list}" data-item-no="54fdfsfb"></a>
			<span class="divider"></span>
			{if cartflow}<a href="#" class="ip-cart"></a>{/if}
		</div>
	</div>
</div>