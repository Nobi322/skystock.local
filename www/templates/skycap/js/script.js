/**
 * Created by Denis on 07.08.2015.
 */


$(document).ready(function () {

	if($('#mp-video-bg').length){
		var myPlayer = videojs('mp-video-bg');
		myPlayer.play().muted(true);;
	}


	$('.img-cnt').imagefill({throttle:1000/60});
	setTimeout(function() {
		$('input, select').not('.dnt-style').styler();
	}, 100);


	$(window).scroll(function () {
		if ($(this).scrollTop() > 0) {
			$('#scroller').fadeIn();
		} else {
			$('#scroller').fadeOut();
		}
	});
	$('#scroller').click(function (e) {
		e.preventDefault();
		$('body,html').animate({
			scrollTop: 0
		}, 400);
		return false;
	});

	$('.item-cnt').popover({
		'trigger':'hover',
		'html':true,
		placement: 'auto right',
		'content':function(){
			 var html =  '<video id="testV" autoplay  loop width="280" height="180">';
			html+= '<source src="'+$(this).data('video-src')+'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\' />';
			html+=	'</video>';

			return html;
		},
		title: function () {
			return $(this).data('video-title');
		},
		template: '<div class="popover video-popover" role="tooltip"><div class="popover-content"></div><h3 class="popover-title"></h3></div>'
	});
	$('.mh-item').popover({
		'trigger':'hover',
		'html':true,
		placement: 'auto right',
		'content':function(){
			 var html =  '<video id="testV" autoplay  loop width="280" height="180">';
			html+= '<source src="'+$(this).data('video-src')+'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\' />';
			html+=	'</video>';

			return html;
		},
		title: function () {
			return $(this).data('video-title');
		},
		template: '<div class="popover video-popover" role="tooltip"><div class="popover-content"></div><h3 class="popover-title"></h3></div>',
		container: 'body'
	});

	$('.mh-tab-list a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
		var $this = $(this);
		setTimeout(function () {
			$($this.attr('href')).find('.img-cnt').imagefill();
		},100);
	});



	var owl = $(".mh-owl-carousel");
	if(owl.length){

		owl.each(function () {
			$(this).owlCarousel({
				items: 5,
				navigation: true,
				pagination: false,
				itemsCustom : [
					[0, 2],
					[450, 5],
					[600, 5],
					[700, 5],
					[1000, 5],
					[1200, 5],
					[1400, 5],
					[1600, 5]
				],
				navigation : true

			});
		});


	}


	$('.ip-favorite').click(function (e) {
		e.preventDefault();
		$('.ip-favorite').popover('hide');
		$('.ip-favorite').attr('title', 'Добавить в избранное')
	}).popover({
		'trigger':'click',
		'html':true,
		placement: 'auto bottom',
		'content':function(){
			var html = '<div class="fav-top"><i class="i-fav"></i>Добавить в избранное:<a href="#" class="fav-close"></a></div>';
			html+= '<form id="favorite-add-form" data-item-no="'+$(this).data('item-no')+'"><div class="fav-content"><div class="fav-items"></div></div>';
			html+= '<div class="fav-buttons"><input type="text" name="new-lib" placeholder="Новая библиотека"><button type="submit" class="btn btn-info">Сохранить</button> </div></form>';

			addFavoriteList(this);
			return html;
		},
		template: '<div class="popover favorite-popover" role="tooltip"><div class="popover-content"></div></div>',
		container: 'body'
	});
	$('.pp-unlogined').click(function (e) {
		e.preventDefault();
		$('.pp-unlogined').popover('hide');
		$('.pp-unlogined').attr('title', 'Скачать образец');
	}).popover({
		'trigger':'click',
		'html':true,
		placement: 'auto bottom',
		'content':function(){
			var html = '<div class="def-top"><i class="fa fa-download"></i>Добавить в избранное:<a href="#" class="def-close"></a></div>';
			html+= '<div class="def-success"><a href="/login">Войдите</a> , чтобы скачать бесплатную копию клипа с водяными знаками.</div>';

			addFavoriteList(this);
			return html;
		},
		template: '<div class="popover default-popover" role="tooltip"><div class="popover-content"></div></div>',
		container: 'body'
	});


	$('.quality-table input').on('change', function () {
		console.log('chane');
		var $tr = $(this).parents('tr');
		$tr.find('input[type="radio"]').filter(':checked').parents('tr').addClass('active-ch').siblings().removeClass('active-ch');
	});

	if($('.cart-table').length){
		initCart();
	}

	/*End or ready*/
});

$(document).on('click', '.ip-cart', function (e) {
	e.preventDefault();
	console.log('add');

	var cpHtml = '',
		$btn = $(this),
		$item = $btn.parents('.item-cnt'),
		id = $item.data('item-no'),
		imgSrc = $item.find('.img-cnt img').attr('src');

	cpHtml+= '<div id="cart-add" class="cart-popup">';
	cpHtml+= '  <a href="#" class="pp-close"></a>';
	cpHtml+= '  <div class="row">';
	cpHtml+= '      <div class="col-md-4 img">';
	cpHtml+= '          <img src="'+imgSrc+'">';
	cpHtml+= '      </div>';
	cpHtml+= '      <div class="col-md-8">';

	cpHtml+= '          <h5>';
	cpHtml+= '              <i class="fa fa-check"></i>';
	cpHtml+= '              Добавлено в вашу корзину';
	cpHtml+= '          </h5>';
	cpHtml+= '          <p class="id">';
	cpHtml+= '               <b>id:</b> '+id;
	cpHtml+= '          </p>';
	cpHtml+= '          <div class="row">';
	cpHtml+= '              <div class="col-md-6">';
	cpHtml+= '                  <a href="/cart" class="btn btn-info">Показать корзину</a>';
	cpHtml+= '              </div>';
	cpHtml+= '          </div>';
	cpHtml+= '      </div>';
	cpHtml+= '  </div>';
	cpHtml+= '</div>';


	$('#cart-add').remove();
	$('body').append(cpHtml);

	$('#cart-add').hover(function () {
		//Hover on
		$('#cart-add').addClass('hover');
	}, function () {
		//Hover out
		$('#cart-add').removeClass('hover');
		hideCartPP(1000);

	});

	hideCartPP(5000);
	$('body,html').animate({
		scrollTop: 0
	}, 400);

});

$(document).on('click', '.pp-close', function (e) {
	e.preventDefault();
	$('#cart-add').fadeOut(500);
});

function hideCartPP(timer){

	setTimeout(function () {
		if(!$('#cart-add').hasClass('hover')){
			$('#cart-add').fadeOut(500);
		}
	}, timer);
}

function initCart(){
	calculateCart();
	$(document).on('click', '.ct-delete', function (e) {
		e.preventDefault();
		$(this).parents('tr').hide(300, function () {
			$(this).remove();
			calculateCart();
		});
	});
	$(document).on('click', '.ct-delete-total', function (e) {
		e.preventDefault();
		$('.cart-table tr').not('.ct-total-row').hide(300, function () {
			$(this).remove();
			calculateCart();
		});
	});

}
function calculateCart(){
	var $alltr = $('.cart-table tr').not('.ct-total-row'),
		priceSum = 0,
		qty = $alltr.length;

	$alltr.each(function () {
		priceSum+= parseInt($(this).find('td').eq(2).text().match(/(\d+)/ig));

	});

	if(qty>0){
		$('.ct-total .total').text(priceSum);
		$('.ct-total .each').text((priceSum/qty).toFixed(0));
		$('.ct-qty-total .qty-total').text(qty);
	}else{
		$('.ct-total .total').text(0);
		$('.ct-total .each').text(0);
		$('.ct-qty-total .qty-total').text(0);
	}


	//console.log(priceSum)
}

function addFavoriteList(link){

	setTimeout(function () {
		var $popup = $('#'+$(link).attr('aria-describedby')),
			id = $(link).attr('href'),
			$itemCnt = $popup.find('.fav-items');
		console.log($popup);

		$.ajax({
			type: "POST",
			url: "/favorites.json",
			data: '{"Query":"favorite","id":"'+id+'"}',
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			beforeSend: function () {
				$itemCnt.html('<i class="fa fa-circle-o-notch fa-spin"></i>')
			},
			success: function (data) {
				libs = data.error ? [] : $.map(data.libraries, function(m) {
					return {
						lib: m.lib
					};
				});
				$itemCnt.html(buildLibs(libs));
			},
			error: function (msg) {

			}
		});
		$popup.find('.fav-content').mCustomScrollbar({
			scrollButtons:{
				enable: true
			},
			advanced:{
				updateOnContentResize: true
			}
		})


	},10)
}
function buildLibs(data){

	//console.log(data);
	var html = '';
	data.forEach(function(item, i, arr) {
		html += ' <label>';
		html +='<input type="radio" name="jqsl-lib" value="'+item.lib+'">';
		html +='<span>'+item.lib+'</span>';
		html +='</label>';

	});
	return html;
}

$(document).on('click', '.favorite-popover button[type="submit"]', function (e) {
	e.preventDefault();
	console.log('subm');
	var $this = $(this),
		$form =  $this.parents('form'),
		$radio = $form.find('input[name="jqsl-lib"]'),
		radioVal = $radio.filter(':checked').val(),
		newVal = $form.find('input[name="new-lib"]').val();
	console.log(radioVal);

	if(newVal!=''){
		confirmFavorite($form,newVal, true);
	}else if(radioVal!=undefined){
		confirmFavorite($form,radioVal, false);
	}else{
		$form.parents('.popover').effect( "shake" );
	}
});


function confirmFavorite(form, name, newItem){
	var iFav = form.parents('.i-fav');
	console.log('Send to srv about my select: '+name);
	$.ajax({
		type: "POST",
		url: "/favorites-recieve.php",
		data: '{"itemId":"'+form.data('item-no')+'","newLib":"'+newItem+'", "libName": "'+name+'"}',
		contentType: "application/json; charset=utf-8",
		dataType: "text",
		beforeSend: function () {
			iFav.hide().after('<i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		success: function (data) {
			iFav.show().next().hide();
			console.log('success');
			form.html('<div class="fav-success">Сохранено в <a href="http://skystock.local/colaection/'+name+'">'+name+'</a></div>');
		},
		error: function (msg) {
			console.log(msg);
		},
		always: function () {
			iFav.show().next().hide();
		}
	});
}

//Close baner btn
$(document).on('click', '.block-vigoda .close-btn', function (e) {
	e.preventDefault();
	$(this).parents('.block-vigoda').fadeOut(300);
});

$(document).on('click', '.popover .fav-close, .popover .def-close', function (e) {
	e.preventDefault();
	$(this).parents('.popover').popover('hide');
});

$(document).on('click', '.profile .open-log, .profile .open-sig', function (e) {
	e.preventDefault();
	var $link = $(this),
		$tgt = $link.attr('href')!=undefined ?  $($link.attr('href')) : $link.next('div');


	if(!$tgt.hasClass('fade-open')){
		$tgt.fadeIn({
			always: function () {
				$tgt.addClass('fade-open');
			},
			duration: 300
		});
	}else{
		$tgt.fadeOut(300, function () {
			$tgt.removeClass('fade-open')
		});
	}
});
$(document).on('click', function(e){
	if($(e.target).closest('.fade-open').length==0 && !$(e.target).hasClass('fade-open')){
		$('.fade-open').fadeOut(300, function () {
			$('.fade-open').removeClass('fade-open').parent('.parent-open').removeClass('parent-open');

		});
	}
});

$(document).on('click', '.number-settings .nbs-controll, .number-settings .i-arr-down', function (e) {
	e.preventDefault();
	var $link = $(this),
		$parent = $link.parents('.number-settings'),
		$tgt = $parent.find('.nbs-popup');


	if(!$tgt.hasClass('fade-open')){
		$tgt.fadeIn({
			always: function () {
				$tgt.addClass('fade-open');
				$parent.addClass('parent-open');
			},
			duration: 300
		});
	}else{
		$tgt.fadeOut(300, function () {
			$tgt.removeClass('fade-open');
			$parent.removeClass('parent-open');
		});
	}
});