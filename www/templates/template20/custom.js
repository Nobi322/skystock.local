/**
 * @author Alexander Farkas
 * v. 1.02
 */
(function($) {
	$.extend($.fx.step,{
	    backgroundPosition: function(fx) {
            if (fx.state === 0 && typeof fx.end == 'string') {
                var start = $.curCSS(fx.elem,'backgroundPosition');
                start = toArray(start);
                fx.start = [start[0],start[2]];
                var end = toArray(fx.end);
                fx.end = [end[0],end[2]];
                fx.unit = [end[1],end[3]];
			}
            var nowPosX = [];
            nowPosX[0] = ((fx.end[0] - fx.start[0]) * fx.pos) + fx.start[0] + fx.unit[0];
            nowPosX[1] = ((fx.end[1] - fx.start[1]) * fx.pos) + fx.start[1] + fx.unit[1];           
            fx.elem.style.backgroundPosition = nowPosX[0]+' '+nowPosX[1];
            
           function toArray(strg){
               strg = strg.replace(/left|top/g,'0px');
               strg = strg.replace(/right|bottom/g,'100%');
               strg = strg.replace(/([0-9\.]+)(\s|\)|$)/g,"$1px$2");
               var res = strg.match(/(-?[0-9\.]+)(px|\%|em|pt)\s(-?[0-9\.]+)(px|\%|em|pt)/);
               return [parseFloat(res[1],10),res[2],parseFloat(res[3],10),res[4]];
           }
        }
	});
})(jQuery);



$(document).ready(function(){

	if ($(window).scrollTop() > 94) 
    {
    	$('#wrapper3').css({'position':'fixed','top':'0'});
   	}

    $(window).scroll(function(){
    	if ($(window).scrollTop() > 94) 
    	{
    		$('#wrapper3').css({'position':'fixed','top':'0'});
   	 	}
    	else 
    	{
    		$('#wrapper3').css({'position':'absolute','top':'94px'});
    	};
    	
    	
    	
    	if ($(window).scrollTop() > 500) 
    	{
    		$('#scroll_box').slideDown("slow");
   	 	}
    	else 
    	{
    		$('#scroll_box').slideUp("slow");
    	};
    });
    
    
        $("#scroll_box").click
   		(
			function () 
			{
				$(window).scrollTo(0, 1000, {axis:'y'} );
			}
		);

	 

        
         $(".blogin").hover(function() {
              $(this).stop().animate({ backgroundColor: "#860e49"}, 600);
          },function() {
              $(this).stop().animate({ backgroundColor: "#007fcc" }, 600);
          });   
          
          
          $(".bsignup").hover(function() {
              $(this).stop().animate({ backgroundColor: "#ff5b02"}, 600);
          },function() {
              $(this).stop().animate({ backgroundColor: "#599c3e" }, 600);
          });   

		
		 $('.ibox_search_submit').mouseover(function()
        {
            $(this).stop(); $(this).animate({backgroundPosition: '(0px -40px)'},200);
        }).mouseout(function()
        {
            $(this).stop(); $(this).animate({backgroundPosition: '(0px 0px)'},200);
        });
   
        
        

        
        $(".lanbox").colorbox({width:"730",height:"420", inline:true, href:"#languages_lite2"});
        
        $(".lbox").colorbox({width:"",height:"", inline:true, href:"#login_box"});
        
        $('#search').keyup(function() 
		{
 		 	show_search();
		});
	
	
		$("#instant_search").hover
		(
			function () 
			{
				
			},
			function () 
			{
				$('#instant_search').slideUp("fast");
				document.getElementById('instant_search').innerHTML ="";
			}
		);

    });
    
    
    
    
    
